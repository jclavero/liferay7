package PORLET.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import LIBRO.service.EscritorLocalServiceUtil;
import PORLET.constants.PORLETPortletKeys;

@Component(immediate = true, property = { 
		"javax.portlet.name=" + PORLETPortletKeys.PORLET,
		 "mvc.command.name=addEscritor"

}, service = MVCActionCommand.class)
public class AddEscritorMvcActionCommand extends BaseMVCActionCommand {

	private static final Log _log = LogFactoryUtil.getLog(AddEscritorMvcActionCommand.class);

	   @Override
	    protected void doProcessAction(ActionRequest request, ActionResponse response) throws Exception {
	        final ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	        final String nombre = ParamUtil.getString(request, "nombreEscritor");


	        if (_log.isInfoEnabled() && !nombre.isEmpty()) {
				_log.info("process info ::" + nombre);

				EscritorLocalServiceUtil.addEscritor(td.getSiteGroupId(), td.getCompanyId(), td.getUser().getUserId(),
						td.getUser().getFullName(), nombre);
				
			}
	       
	    }
}

package PORLET.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import LIBRO.service.EscritorLocalServiceUtil;
import PORLET.constants.PORLETPortletKeys;

@Component(
        immediate = true,
        property = {
        		"javax.portlet.name=" + PORLETPortletKeys.PORLET,
                "mvc.command.name=deleteEscritor"
        },
        service = MVCActionCommand.class
)
public class DeleteEscritorMvcActionCommand extends BaseMVCActionCommand{

	private static final Log _log = LogFactoryUtil.getLog(DeleteEscritorMvcActionCommand.class);
	
    @Override
    protected void doProcessAction(ActionRequest request, ActionResponse response) throws Exception {
      
    	final String id = ParamUtil.getString(request, "idEscritor");
		// final String id = actionRequest.getParameter("idEscritor");
		try {

			if (_log.isInfoEnabled() && !id.isEmpty()) {
				_log.info("process info ::" + id);

				EscritorLocalServiceUtil.deleteEscritor(Long.valueOf(id));
			}

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
		
    }

}

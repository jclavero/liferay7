package PORLET.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import LIBRO.service.EscritorLocalServiceUtil;
import PORLET.constants.PORLETPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PORLETPortletKeys.PORLET,
		"mvc.command.name=editEscritor" }, service = MVCActionCommand.class)
public class EditEscritorMvcActionCommand extends BaseMVCActionCommand {

	private static final Log _log = LogFactoryUtil.getLog(EditEscritorMvcActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest request, ActionResponse response) throws Exception {
		try {
			final String escritores = EscritorLocalServiceUtil.getEscritors(0, Integer.MAX_VALUE).toString();
			if (_log.isInfoEnabled() && !escritores.isEmpty()) {
				_log.info("process info ::" + escritores);

				final String id = ParamUtil.getString(request, "idEscritor");
				final String nombre = ParamUtil.getString(request, "nombreEscritor");
				EscritorLocalServiceUtil.updateEscritor(Long.valueOf(id), nombre);
				response.setRenderParameter("mvcPath", "/view.jsp");

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

package PORLET.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ContentTypes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import LIBRO.service.EscritorLocalServiceUtil;
import PORLET.constants.PORLETPortletKeys;

@Component(immediate = true, property = { 
		"javax.portlet.name=" + PORLETPortletKeys.PORLET,
		 "mvc.command.name=downloadEscritores"

}, service = MVCResourceCommand.class)
public class DownloadEscritoresMvcResourceCommand extends BaseMVCResourceCommand {
	
	private static final Log _log = LogFactoryUtil.getLog(DownloadEscritoresMvcResourceCommand.class);
    @Override
    protected void doServeResource(ResourceRequest request, ResourceResponse response) throws Exception {
    	try {
			final String escritores = EscritorLocalServiceUtil.getEscritors(0, Integer.MAX_VALUE).toString();
			if (_log.isInfoEnabled() && !escritores.isEmpty()) {
				_log.info("process info ::" + escritores);
				final InputStream stream = new ByteArrayInputStream(escritores.getBytes(StandardCharsets.UTF_8));
				PortletResponseUtil.sendFile(request, response, "escritores.txt", stream,
						ContentTypes.APPLICATION_TEXT);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }


}

package PORLET.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import LIBRO.model.Escritor;
import LIBRO.service.EscritorLocalServiceUtil;
import PORLET.constants.PORLETPortletKeys;

@Component(
        immediate = true,
        property = {
        		"javax.portlet.name=" + PORLETPortletKeys.PORLET,
                "mvc.command.name=displayEscritorEdition"
        },
        service = MVCRenderCommand.class
)
public class EditEscritorMvcRenderCommand implements MVCRenderCommand {
	
	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
       
		
        final String id = ParamUtil.getString(renderRequest, "idEscritor");
        try {
        	 final Escritor escritor = EscritorLocalServiceUtil.getEscritor(Long.valueOf(id));
             
				  renderRequest.setAttribute("escritor", escritor);
			
           
        } catch (PortalException e) {
            throw new RuntimeException(e);
        }

        return "/escritorEdit.jsp";
	}

}

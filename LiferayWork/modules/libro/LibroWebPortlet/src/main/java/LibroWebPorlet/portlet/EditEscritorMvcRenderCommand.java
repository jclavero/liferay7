package LibroWebPorlet.portlet;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.servicebuilder.libro.model.Escritor;
import com.liferay.servicebuilder.libro.service.EscritorLocalServiceUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import LibroWebPorlet.constants.LibroWebPortletKeys;

@Component(
        immediate = true,
        property = {
        		"javax.portlet.name=" + LibroWebPortletKeys.LibroWeb,
                "mvc.command.name=displayEscritorEdition"
        },
        service = MVCRenderCommand.class
)
public class EditEscritorMvcRenderCommand implements MVCRenderCommand {

    @Override
    public String render(RenderRequest request, RenderResponse response) throws PortletException {
        final String id = request.getParameter("idEscritor");

        try {
            final Escritor escritor = EscritorLocalServiceUtil.getEscritor(Long.valueOf(id));
            request.setAttribute("escritor", escritor);

        } catch (PortalException e) {
            throw new RuntimeException(e);
        }

        return "/escritorEdit.jsp";
    }

}
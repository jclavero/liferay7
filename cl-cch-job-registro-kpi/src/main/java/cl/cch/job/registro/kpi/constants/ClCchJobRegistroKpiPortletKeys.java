package cl.cch.job.registro.kpi.constants;

/**
 * @author alexa
 */
public class ClCchJobRegistroKpiPortletKeys {

	public static final String ClCchJobRegistroKpi = "ClCchJobRegistroKpi";

	//Constantes para crear archivos de textos
	public static final String FILE_USERS = "/Usuarios_";
	public static final String FILE_TRANSACTIONS = "/Transacciones_";
	public static final String FILE_TRANSACTIONS_SAC = "/TransaccionesSac_";
	public static final String FILE_TRANSACTIONS_TB = "/TransaccionesTransbank_";
	public static final String FILE_DIRECCIONES_PERSONAL= "/DireccionPersonal_";
	public static final String FILE_DIRECCIONES_DESTINATARIOS= "/DireccionDestinatario_";
	public static final String EXTENTION_FILE = ".txt";
	public static final String CREATE_FILE_USUARIOS = "file-usuarios";
	public static final String CREATE_FILE_TRANSACCIONES = "file-transacciones";
	public static final String CREATE_FILE_TRANSACCIONES_SAC = "file-transacciones-sac";
	public static final String CREATE_FILE_TRANSACCIONES_TB = "file-transacciones-transbank";
	public static final String CREATE_FILE_DIRECCIONES_PERSONAL = "file-direcciones-personales";
	public static final String CREATE_FILE_DIRECCIONES_DESTINATARIO = "file-direcciones-destinatarios";
	public static final String DATA_SEPARATION = ";";
	
	//Configuraciones Jobs
	public static final String JOBS_CCH_CONFIGURATION_NAME = "Configuracion Job Regitro KPI";
	public static final String JOBS_CCH_CONFIGURATION_PID = "registro-kpi-configuration-pid";
	
	//Campos personalizados usuarios
	public static final String NUMERO_DOCUMENTO = "Numero_Documento";
	
	public static final String USUARIOS_URL = "usuariosURL";
	public static final String TRANSACCIONES_URL = "transaccionesURL";
	public static final String TRANSABANK_URL = "transabankURL";
	public static final String DPERSONAL_URL = "direccionesPersonalesURL";
	public static final String DDESTINATARIO_URL = "direccionesDestinatarioURL";
	public static final String SAC_URL = "transaccionesSacURL";
	
}
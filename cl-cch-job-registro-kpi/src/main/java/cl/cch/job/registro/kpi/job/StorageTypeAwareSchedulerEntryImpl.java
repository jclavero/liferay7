package cl.cch.job.registro.kpi.job;

import com.liferay.portal.kernel.scheduler.SchedulerEntry;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.StorageTypeAware;
import com.liferay.portal.kernel.scheduler.Trigger;

public class StorageTypeAwareSchedulerEntryImpl extends SchedulerEntryImpl implements SchedulerEntry, StorageTypeAware {

	private static final long serialVersionUID = 1102724886977154489L;
	/**
	   * StorageTypeAwareSchedulerEntryImpl: Constructor for the class.
	   * @param schedulerEntry
	   */
	  public StorageTypeAwareSchedulerEntryImpl(final SchedulerEntryImpl schedulerEntry) {

	    _schedulerEntry = schedulerEntry;

	    // use the same default that Liferay uses.
	    _storageType = StorageType.MEMORY_CLUSTERED;
	  }

	  /**
	   * StorageTypeAwareSchedulerEntryImpl: Constructor for the class.
	   * @param schedulerEntry
	   * @param storageType
	   */
	  public StorageTypeAwareSchedulerEntryImpl(final SchedulerEntryImpl schedulerEntry, final StorageType storageType) {
	    super();

	    _schedulerEntry = schedulerEntry;
	    _storageType = storageType;
	  }

	  @Override
	  public String getDescription() {
	    return _schedulerEntry.getDescription();
	  }

	  @Override
	  public String getEventListenerClass() {
	    return _schedulerEntry.getEventListenerClass();
	  }

	  @Override
	  public StorageType getStorageType() {
	    return _storageType;
	  }

	  @Override
	  public Trigger getTrigger() {
	    return _schedulerEntry.getTrigger();
	  }

	  private SchedulerEntryImpl _schedulerEntry;
	  private StorageType _storageType;
}

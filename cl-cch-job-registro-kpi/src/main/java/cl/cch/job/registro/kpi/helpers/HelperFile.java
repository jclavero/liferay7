package cl.cch.job.registro.kpi.helpers;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.model.ExpandoValue;
import com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.PhoneLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.osgi.service.component.annotations.Reference;

import cl.cch.job.registro.kpi.config.JobKpiConfig;
import cl.cch.job.registro.kpi.constants.ClCchJobRegistroKpiPortletKeys;
import cl.cch.mis.direcciones.sb.model.Direccion_destinatario;
import cl.cch.mis.direcciones.sb.model.Direccion_personal;
import cl.cch.mis.direcciones.sb.service.Direccion_destinatarioLocalServiceUtil;
import cl.cch.mis.direcciones.sb.service.Direccion_personalLocalServiceUtil;
import cl.cch.pre.ingreso.sb.model.PreAdmision;
import cl.cch.pre.ingreso.sb.service.PreAdmisionLocalServiceUtil;
import cl.cch.pre.ingreso.transaction.sb.model.PreAdmisionTransaction;
import cl.cch.pre.ingreso.transaction.sb.service.PreAdmisionTransactionLocalServiceUtil;

public class HelperFile {

	@Reference
	private DLAppService _dlAppService;

	@Reference
	private ServiceContextThreadLocal serviceContext;

	private static final Log _log = LogFactoryUtil.getLog(HelperFile.class);

	// M�todo principal para obtener datos desde sb, vaciarlos en un txt y
	// enviarlo a un SFTP (recibe el archivo que se creara 'usuarios'
	// o 'transaciones')
	public void registrarKpi(String fileKpi, JobKpiConfig _jobKipConfig) throws ParseException {
		_log.info("[registrarKpi] Se inicia registro KPI: " + fileKpi);
		String fileName = null;
		File file = null;
		Boolean subido = false;
		fileName = createFileName(fileKpi);
		file = createFile(fileName);
		if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_USUARIOS.equalsIgnoreCase(fileKpi)) {
			insertDataUsers(file);
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES.equalsIgnoreCase(fileKpi)) {
			insertDataTransactions(file);
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_SAC.equalsIgnoreCase(fileKpi)) {
			insertDataTransactionsSac(file);
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_TB.equalsIgnoreCase(fileKpi)) {
			insertDataTransactionsTransbank(file);
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_PERSONAL.equalsIgnoreCase(fileKpi)) {
			insertDataDireccionPersonal(file);
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_DESTINATARIO.equalsIgnoreCase(fileKpi)) {
			insertDataDireccionDestinatarios(file);
		}
		subido = sendFileSFTP(fileName, _jobKipConfig);
		for (int i = 1; i < 3; i++) {
			if (subido) {
				_log.info("[registrarKpi] Archivo subido en intento numero " + i);
				break;
			} else {
				_log.info("[registrarKpi] Intenta nmero " + i + " subir archivo");
				subido = sendFileSFTP(fileName, _jobKipConfig);
			}
		}
		_log.info("[registrarKpi] Fin registro KPI");
	}

	// Mtodo para crear nombre del archivo.
	private String createFileName(String fileKpi) {

		_log.info("[createFileName] Creacin de nombre para archivo");
		String fileNameTxt = "";

		Date someDate = new Date();
		Date newDate = new Date(someDate.getTime() + TimeUnit.DAYS.toMillis(-1));
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String fecha = dateFormat.format(newDate);

		_log.info("[createFileName] Fecha de archivo: " + fecha);

		String property = "java.io.tmpdir";
		String tempDir = System.getProperty(property);
		System.out.println("resp : " + tempDir);
		String filepath = tempDir;
		String fileName = "";

		if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_USUARIOS.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_USERS + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_TRANSACTIONS + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		}
		else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_SAC.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_TRANSACTIONS_SAC + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		}
		else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_TB.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_TRANSACTIONS_TB + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		} else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_PERSONAL.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_DIRECCIONES_PERSONAL + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		}

		else if (ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_DESTINATARIO.equalsIgnoreCase(fileKpi)) {
			fileName = ClCchJobRegistroKpiPortletKeys.FILE_DIRECCIONES_DESTINATARIOS + fecha
					+ ClCchJobRegistroKpiPortletKeys.EXTENTION_FILE;
		}
		fileNameTxt = filepath + fileName;
		_log.info("[createFileName] Nombre de archivo: " + fileName);
		return fileNameTxt;
	}

	// Mtodo para crear el archivo de texto.
	private static File createFile(String fileName) {
		_log.info("[createFile] Iniciando creacin de archivo ...");
		File file = new File(fileName);
		_log.info("[createFile] Archivo creado en " + fileName);
		return file;
	}

	// M�todo para ingresar datos de usuarios en texto.
	private void insertDataUsers(File file) {

		_log.info("[insertDataUsers] Iniciando vaciado de datos usuarios en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;
		try {
			_log.info("[insertDataUsers] Se agregaran datos en archivo");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);

			List<User> usuarios = UserLocalServiceUtil.getCompanyUsers(20101L, -1, -1);
			for (User user : usuarios) {
				if (user.getFirstName() != "admin" && user.getFirstName() != "test" && user.getFirstName() != "Admin") {
					_log.info("[usuario] :" + user.toString());
					String userId = "";
					String nombres = "";
					String apellidos = "";
					String rut = "";
					String fechaNac = "";
					String calle = "";
					String numeroCalle = "";
					String dpto = "";
					String comuna = "";
					String codPostal = "";
					String correo = "";
					String telefono = "";
					String uuid_ = "";
					String externalReferenceCode = "";
					String companyId = "";
					String createDate = "";
					String modifiedDate = "";
					String screenName = "";
					String googleUserId = "";
					String openId = "";
					String comments = "";
					String firstName = "";
					String middleName = "";
					String lastName = "";
					String jobTitle = "";
					String loginDate = "";
					String loginIP = "";

					ExpandoValue valorExpando = ExpandoValueLocalServiceUtil.getValue(20101, User.class.getName(),
							ExpandoTableConstants.DEFAULT_TABLE_NAME, ClCchJobRegistroKpiPortletKeys.NUMERO_DOCUMENTO,
							user.getUserId());

					try {
						if (valorExpando.getData() != null) {
							
							rut = normalizarData(valorExpando.getData());
						}
					} catch (Exception e) {
						_log.error("[error no contiene rut el usuario]");

					}

					try {
						List<Direccion_personal> dir_personal = Direccion_personalLocalServiceUtil
								.getFindByNumero_Documento(rut);

						if (dir_personal != null) {

							if (dir_personal.get(0).getCalle() != null) {
								calle =normalizarData(dir_personal.get(0).getCalle());
							}
							if (dir_personal.get(0).getNumero() != null) {
								numeroCalle = normalizarData(dir_personal.get(0).getNumero());
							}
							if (dir_personal.get(0).getDepto() != null) {
								dpto = normalizarData(dir_personal.get(0).getDepto());
							}
							if (dir_personal.get(0).getComuna() != null) {
								comuna = normalizarData(dir_personal.get(0).getComuna());
							}
							if (dir_personal.get(0).getCodigo_Postal() != null) {
								codPostal = normalizarData(dir_personal.get(0).getCodigo_Postal());
							}
						}
					} catch (Exception e) {
						_log.error("[error no contiene direccion el usuario]");
					}

					try {
						List<Phone> phones = PhoneLocalServiceUtil.getPhones(-1, -1);
						for (Phone phone : phones) {
							if (phone.getUserId() == user.getUserId()) {
								telefono = normalizarData(phone.getNumber());
							}
						}
					} catch (Exception e) {
						_log.error("[error no contiene telefono el usuario]");
					}

					String id = String.valueOf(user.getUserId());
					if (id != null) {
						userId = normalizarData(id);
					}

					if (user.getFirstName() != null) {
						nombres = normalizarData(user.getFirstName());
					}
					if (user.getLastName() != null) {
						apellidos = normalizarData(user.getLastName());
					}

					if (user.getUuid() != null) {
						uuid_ = normalizarData(user.getUuid());
					}
					if (user.getExternalReferenceCode() != null) {
						externalReferenceCode = normalizarData(user.getExternalReferenceCode());
					}
					if (String.valueOf(user.getCompanyId()) != null) {
						companyId = normalizarData(String.valueOf(user.getCompanyId()));
					}
					if (user.getCreateDate() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(user.getCreateDate());
						createDate = normalizarData(fecha1);
					}
					if (user.getModifiedDate() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(user.getModifiedDate());
						modifiedDate = normalizarData(fecha1);
					}
					if (user.getScreenName() != null) {
						screenName = normalizarData(user.getScreenName());
					}
					if (user.getGoogleUserId() != null) {
						googleUserId = normalizarData(user.getGoogleUserId());
					}
					if (user.getOpenId() != null) {
						openId = normalizarData(user.getOpenId());
					}
					if (user.getComments() != null) {
						comments = normalizarDataReferencia(user.getComments());
					}
					if (user.getFirstName() != null) {
						firstName = normalizarData(user.getFirstName());
					}
					if (user.getMiddleName() != null) {
						middleName = normalizarData(user.getMiddleName());
					}
					if (user.getLastName() != null) {
						lastName = normalizarData(user.getLastName());
					}
					if (user.getJobTitle() != null) {
						jobTitle = normalizarData(user.getJobTitle());
					}
					if (user.getLoginDate() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(user.getLoginDate());
						loginDate = normalizarData(fecha1);
					}
					if (user.getLoginIP() != null) {
						loginIP = normalizarData(user.getLoginIP());
					}
					if (user.getBirthday() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(user.getBirthday());
						fechaNac = normalizarData(fecha1);
					}
					if (user.getEmailAddress() != null) {
						correo = normalizarData(user.getEmailAddress());
					}

					bw.write(userId.replace(";", "") + separation + nombres.replace(";", "") + separation + apellidos.replace(";", "") + separation + rut.replace(";", "") + separation
							+ fechaNac.replace(";", "") + separation + calle.replace(";", "") + separation + numeroCalle.replace(";", "") + separation + dpto.replace(";", "") + separation
							+ comuna.replace(";", "") + separation + codPostal.replace(";", "").replace("-", "") + separation + correo.replace(";", "") + separation + telefono.replace(";", "") + separation
							+ uuid_.replace(";", "") + separation + externalReferenceCode.replace(";", "") + separation + companyId.replace(";", "") + separation
							+ createDate.replace(";", "")+ separation + modifiedDate.replace(";", "") + separation + screenName.replace(";", "") + separation
							+ googleUserId.replace(";", "") + separation + openId.replace(";", "") + separation + comments.replace(";", "") + separation + firstName.replace(";", "")
							+ separation + middleName.replace(";", "") + separation + lastName.replace(";", "") + separation + jobTitle.replace(";", "") + separation
							+ loginDate.replace(";", "") + separation + loginIP.replace(";", ""));
					bw.newLine();
				}
			}
			bw.close();
			_log.info("[insertDataUsers] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataUsers] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para ingresar datos de usuarios en texto.
	private void insertDataTransactions(File file) throws ParseException {
		_log.info("[insertDataTransactions] Iniciando vaciado de datos de transacciones en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;

		Date someDate = new Date();
		Date newDate = new Date(someDate.getTime()+ TimeUnit.DAYS.toMillis(-1));

		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = sdf.format(newDate);
		String dateStringUser = "";
		Date dateFormat = null;
		Date dateFormatUser = null;
		try {
			List<PreAdmision> transacciones_lst = PreAdmisionLocalServiceUtil.getPreAdmisions(-1, -1);
			_log.info("[insertDataTransactions] Se agregaran datos en archivo");
			dateFormat = sdf.parse(dateString);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			for (PreAdmision transaccion : transacciones_lst) {
				_log.info("[transaccion] :" + transaccion.toString());

				ExpandoValue valorExpando = ExpandoValueLocalServiceUtil.getValue(20101, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME, ClCchJobRegistroKpiPortletKeys.NUMERO_DOCUMENTO,
						transaccion.getIdUsuario());

				dateStringUser = sdf.format(transaccion.getFechaCreacion());
				String id = "";
				String uuid = "";
				String idToken = "";
				String idUsuario = "";
				String fechaCreacion = "";
				String fechaActualizacion = "";
				String totalArticulos = "";
				String ctdaPaquetes = "";
				String ctdaDocumentos = "";
				String pesoEnvio = "";
				String tipoDestino = "";
				String iataOrigen = "";
				String iataDestino = "";
				String iataOficinaDestino = "";
				String mercado = "";
				String largoEnvio = "";
				String altoEnvio = "";
				String anchoEnvio = "";
				String pesoVolumen = "";
				String linea = "";
				String nroVentas = "";
				String idServicioTarifa = "";

				String descripcionTarifa = "";
				String tarifaBase = "";
				String tieneDescuento = "";
				String montoDescuento = "";
				String porcentajeDescuento = "";
				String idCampanaTarifa = "";
				String totalPagar = "";
				String nroEnvio = "";
				String tipoContenidoEnvio = "";
				String descripcionContenidoEnvio = "";
				String valorContenido = "";
				String numBoletaEnvio = "";
				String mercanciasPeligrosas = "";
				String primerNombreRemitente = "";
				String apellidoPaternoRemitente = "";
				String apellidoMaternoRemitente = "";
				String emailRemitente = "";
				String telefonoRemitente = "";
				String numeroDocumentoRemitente = "";
				String tipoDocumentoRemitente = "";
				String recepcionRemitente = "";
				String calleRemitente = "";
				String numeroRemitente = "";
				String comunaRemitente = "";
				String codPostalRemitente = "";
				String nombreImpositor = "";
				String apellidoImpositor = "";
				String tipoImpositor = "";
				String documentoImpositor = "";
				String primerNombreDestinatario = "";
				String apellidoPaternoDestinatario = "";
				String apellidoMaternoDestinatario = "";
				String emailDestinatario = "";
				String telefonoDestinatario = "";
				String numeroDocumentoDestinatario = "";
				String tipoDocumentoDestinatario = "";
				String tipoRecepcionDestino = "";
				String nombreSucursalDestino = "";
				String calleDestinatario = "";
				String numeroDestinatario = "";
				String comunaDestinatario = "";
				String casaDeptoDestinatario = "";
				String codSucursalDestinatario = "";
				String codPostalDestinatario = "";
				String refeDireccionDestinatario = "";
				String latDireccionDestinatario = "";
				String lonDireccionDestinatario = "";
				String documentoRespuesta = "";
				String numSeguimiento = "";
				String numTransaccion = "";
				String rut = "";

				dateFormatUser = sdf.parse(dateStringUser);
				if (dateFormatUser.compareTo(dateFormat) == 0) {

					if (valorExpando.getData() != null) {
						rut = normalizarData(valorExpando.getData());
					}
					if (transaccion.getIdCampanaTarifa() != null) {
						idCampanaTarifa = normalizarData(transaccion.getIdCampanaTarifa());
					}
					if (transaccion.getUuid() != null) {
						uuid = normalizarData(transaccion.getUuid());
					}
					if (transaccion.getNumBoletaEnvio() != null) {
						numBoletaEnvio = normalizarData(transaccion.getNumBoletaEnvio());
					}
					if (transaccion.getCasaDeptoDestinatario() != null) {
						casaDeptoDestinatario = normalizarData(transaccion.getCasaDeptoDestinatario());
					}
					if (transaccion.getRefeDireccionDestinatario() != null) {
						refeDireccionDestinatario = normalizarData(transaccion.getRefeDireccionDestinatario());
					}
					if (transaccion.getLatDireccionDestinatario() != null) {
						latDireccionDestinatario = normalizarData(transaccion.getLatDireccionDestinatario());
					}
					if (transaccion.getLonDireccionDestinatario() != null) {
						lonDireccionDestinatario = normalizarData(transaccion.getLonDireccionDestinatario());
					}
					if (transaccion.getDocumentoRespuesta() != null) {
						documentoRespuesta = normalizarData(transaccion.getDocumentoRespuesta());
					}
					if (String.valueOf(transaccion.getId()) != null) {
						id = normalizarData(String.valueOf(transaccion.getId()));
					}
					if (String.valueOf(transaccion.getIdServicioTarifa()) != null) {
						idServicioTarifa = normalizarData(String.valueOf(transaccion.getIdServicioTarifa()));
					}
					if (transaccion.getIdToken() != null) {
						idToken = normalizarData(transaccion.getIdToken());
					}
					if (transaccion.getIdUsuario() != null) {
						idUsuario = normalizarData(transaccion.getIdUsuario().toString());
					}
					if (transaccion.getFechaCreacion() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(transaccion.getFechaCreacion());
						fechaCreacion = normalizarData(fecha1);
					}
					if (transaccion.getTotalArticulos() != null) {
						totalArticulos = normalizarData(transaccion.getTotalArticulos().toString());
					}
					if (transaccion.getCtdaPaquetes() != null) {
						ctdaPaquetes = normalizarData(transaccion.getCtdaPaquetes());
					}
					if (transaccion.getTotalPagar() != null) {
						totalPagar = normalizarData(transaccion.getTotalPagar().replace(".", ""));
					}
					if (transaccion.getCtdaDocumentos() != null) {
						ctdaDocumentos = normalizarData(transaccion.getCtdaDocumentos());
					}
					if (transaccion.getPesoEnvio() != null) {
						pesoEnvio = normalizarData(transaccion.getPesoEnvio().replace(".", ""));
					}
					if (transaccion.getTipoDestino() != null) {
						tipoDestino = normalizarData(transaccion.getTipoDestino());
					}
					if (transaccion.getIataOrigen() != null) {
						iataOrigen = normalizarData(transaccion.getIataOrigen());
					}
					if (transaccion.getIataDestino() != null) {
						iataDestino = normalizarData(transaccion.getIataDestino());
					}
					if (transaccion.getIataOficinaDestino() != null) {
						iataOficinaDestino = normalizarData(transaccion.getIataOficinaDestino());
					}
					if (transaccion.getMercado() != null) {
						mercado = normalizarData(transaccion.getMercado());
					}
					if (transaccion.getLargoEnvio() != null) {
						largoEnvio = normalizarData(transaccion.getLargoEnvio());
					}
					if (transaccion.getAltoEnvio() != null) {
						altoEnvio = normalizarData(transaccion.getAltoEnvio());
					}
					if (transaccion.getAnchoEnvio() != null) {
						anchoEnvio = normalizarData(transaccion.getAnchoEnvio());
					}
					if (transaccion.getPesoVolumen() != null) {
						pesoVolumen = normalizarData(transaccion.getPesoVolumen().replace(".", ""));
					}
					if (transaccion.getLinea() != null) {
						linea = normalizarData(transaccion.getLinea());
					}
					if (transaccion.getNroVentas() != null) {
						nroVentas = normalizarData(transaccion.getNroVentas());
					}
					if (transaccion.getFechaActualizacion() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(transaccion.getFechaActualizacion());
						fechaActualizacion = normalizarData(fecha1);
					}
					if (transaccion.getNumSeguimiento() != null) {
						numSeguimiento = normalizarData(transaccion.getNumSeguimiento());
					}
					if (transaccion.getNumTransaccion() != null) {
						numTransaccion =normalizarData(transaccion.getNumTransaccion());
					}
					if (transaccion.getIdToken() != null) {
						idToken = normalizarData(transaccion.getIdToken());
					}
					bw.write(id.replace(";", "") + separation + idToken.replace(";", "") + separation + idUsuario.replace(";", "") + separation + fechaCreacion.replace(";", "")
							+ separation + fechaActualizacion.replace(";", "")+ separation + totalArticulos.replace(";", "") + separation + ctdaPaquetes.replace(";", "")
							+ separation + ctdaDocumentos.replace(";", "") + separation + pesoEnvio.replace(";", "") + separation + tipoDestino.replace(";", "")
							+ separation + iataOrigen.replace(";", "") + separation + iataDestino.replace(";", "") + separation + iataOficinaDestino.replace(";", "")
							+ separation + mercado.replace(";", "") + separation + largoEnvio.replace(";", "") + separation + altoEnvio.replace(";", "") + separation
							+ anchoEnvio.replace(";", "") + separation + pesoVolumen.replace(";", "") + separation + linea.replace(";", "") + separation + nroVentas.replace(";", "")
							+ separation + idServicioTarifa.replace(";", "") + separation + descripcionTarifa.replace(";", "") + separation + tarifaBase.replace(";", "")
							+ separation + tieneDescuento.replace(";", "") + separation + montoDescuento.replace(";", "") + separation
							+ porcentajeDescuento.replace(";", "") + separation + idCampanaTarifa.replace(";", "") + separation + totalPagar.replace(";", "") + separation
							+ nroEnvio.replace(";", "") + separation + tipoContenidoEnvio.replace(";", "") + separation + descripcionContenidoEnvio.replace(";", "")
							+ separation + valorContenido.replace(";", "") + separation + numBoletaEnvio.replace(";", "") + separation
							+ mercanciasPeligrosas.replace(";", "") + separation + primerNombreRemitente.replace(";", "") + separation
							+ apellidoPaternoRemitente.replace(";", "") + separation + apellidoMaternoRemitente.replace(";", "") + separation
							+ emailRemitente.replace(";", "") + separation + telefonoRemitente.replace(";", "") + separation + numeroDocumentoRemitente.replace(";", "")
							+ separation + tipoDocumentoRemitente.replace(";", "") + separation + recepcionRemitente.replace(";", "") + separation
							+ calleRemitente.replace(";", "") + separation + numeroRemitente.replace(";", "") + separation + comunaRemitente.replace(";", "") + separation
							+ codPostalRemitente.replace(";", "").replace("-", "") + separation + nombreImpositor.replace(";", "") + separation + apellidoImpositor.replace(";", "")
							+ separation + tipoImpositor.replace(";", "") + separation + documentoImpositor.replace(";", "") + separation
							+ primerNombreDestinatario.replace(";", "") + separation + apellidoPaternoDestinatario.replace(";", "") + separation
							+ apellidoMaternoDestinatario.replace(";", "") + separation + emailDestinatario.replace(";", "") + separation
							+ telefonoDestinatario.replace(";", "") + separation + numeroDocumentoDestinatario.replace(";", "") + separation
							+ tipoDocumentoDestinatario.replace(";", "") + separation + tipoRecepcionDestino.replace(";", "") + separation
							+ nombreSucursalDestino.replace(";", "") + separation + calleDestinatario.replace(";", "") + separation + numeroDestinatario.replace(";", "")
							+ separation + comunaDestinatario.replace(";", "") + separation + casaDeptoDestinatario.replace(";", "") + separation
							+ codSucursalDestinatario.replace(";", "") + separation + codPostalDestinatario.replace(";", "").replace("-", "") + separation
							+ refeDireccionDestinatario.replace(";", "") + separation + latDireccionDestinatario.replace(";", "") + separation
							+ lonDireccionDestinatario.replace(";", "") + separation + documentoRespuesta.replace(";", "") + separation + numSeguimiento.replace(";", "")
							+ separation + numTransaccion.replace(";", "") + separation + rut.replace(";", "") + separation + uuid.replace(";", ""));
					bw.newLine();
				}
			}
			bw.close();
			_log.info("[insertDataTransactions] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataTransactions] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para ingresar datos de usuarios en texto.
	private void insertDataTransactionsSac(File file) throws ParseException {
		_log.info("[insertDataTransactions Sac] Iniciando vaciado de datos de transacciones en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;

		Date someDate = new Date();
		Date newDate = new Date(someDate.getTime()+ TimeUnit.DAYS.toMillis(-1));

		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = sdf.format(newDate);
		String dateStringUser = "";
		Date dateFormat = null;
		Date dateFormatUser = null;
		try {
			List<PreAdmision> transacciones_lst = PreAdmisionLocalServiceUtil.getPreAdmisions(-1, -1);
			_log.info("[insertDataTransactions Sac ] Se agregaran datos en archivo");
			dateFormat = sdf.parse(dateString);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			for (PreAdmision transaccion : transacciones_lst) {
	
				_log.info("[transaccion Sac] :" + transaccion.toString());

				ExpandoValue valorExpando = ExpandoValueLocalServiceUtil.getValue(20101, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME, ClCchJobRegistroKpiPortletKeys.NUMERO_DOCUMENTO,
						transaccion.getIdUsuario());

				dateStringUser = sdf.format(transaccion.getFechaCreacion());
				String id = "";
				String uuid = "";
				String idToken = "";
				String idUsuario = "";
				String fechaCreacion = "";
				String fechaActualizacion = "";
				String totalArticulos = "";
				String ctdaPaquetes = "";
				String ctdaDocumentos = "";
				String pesoEnvio = "";
				String tipoDestino = "";
				String iataOrigen = "";
				String iataDestino = "";
				String iataOficinaDestino = "";
				String mercado = "";
				String largoEnvio = "";
				String altoEnvio = "";
				String anchoEnvio = "";
				String pesoVolumen = "";
				String linea = "";
				String nroVentas = "";
				String idServicioTarifa = "";

				String descripcionTarifa = "";
				String tarifaBase = "";
				String tieneDescuento = "";
				String montoDescuento = "";
				String porcentajeDescuento = "";
				String idCampanaTarifa = "";
				String totalPagar = "";
				String nroEnvio = "";
				String tipoContenidoEnvio = "";
				String descripcionContenidoEnvio = "";
				String valorContenido = "";
				String numBoletaEnvio = "";
				String mercanciasPeligrosas = "";
				String primerNombreRemitente = "";
				String apellidoPaternoRemitente = "";
				String apellidoMaternoRemitente = "";
				String emailRemitente = "";
				String telefonoRemitente = "";
				String numeroDocumentoRemitente = "";
				String tipoDocumentoRemitente = "";
				String recepcionRemitente = "";
				String calleRemitente = "";
				String numeroRemitente = "";
				String comunaRemitente = "";
				String codPostalRemitente = "";
				String nombreImpositor = "";
				String apellidoImpositor = "";
				String tipoImpositor = "";
				String documentoImpositor = "";
				String primerNombreDestinatario = "";
				String apellidoPaternoDestinatario = "";
				String apellidoMaternoDestinatario = "";
				String emailDestinatario = "";
				String telefonoDestinatario = "";
				String numeroDocumentoDestinatario = "";
				String tipoDocumentoDestinatario = "";
				String tipoRecepcionDestino = "";
				String nombreSucursalDestino = "";
				String calleDestinatario = "";
				String numeroDestinatario = "";
				String comunaDestinatario = "";
				String casaDeptoDestinatario = "";
				String codSucursalDestinatario = "";
				String codPostalDestinatario = "";
				String refeDireccionDestinatario = "";
				String latDireccionDestinatario = "";
				String lonDireccionDestinatario = "";
				String documentoRespuesta = "";
				String numSeguimiento = "";
				String numTransaccion = "";
				String rut = "";
				String autorizacion = "";
				String vci = "";
				String numeroFolio = "";
				String urlBoleta = "";


				try {
					PreAdmisionTransaction lst_transbank = PreAdmisionTransactionLocalServiceUtil.getFindBy_Referencia(transaccion.getIdToken());
					
					if (lst_transbank != null) {
						autorizacion=normalizarData(lst_transbank.getAutorizacion());
						vci=normalizarData(lst_transbank.getVci());
						numeroFolio=normalizarData(lst_transbank.getFolioBoleta());
						urlBoleta=normalizarDataReferencia(lst_transbank.getUrlBoleta());
					
					}
				}catch (Exception e) {
			    _log.error("error autorizacion :"+e.getMessage());
				}
				
				
				dateFormatUser = sdf.parse(dateStringUser);
				if (dateFormatUser.compareTo(dateFormat) == 0) {

					if (valorExpando.getData() != null) {
						rut = normalizarData(valorExpando.getData());
					}
					if (transaccion.getIdCampanaTarifa() != null) {
						idCampanaTarifa = normalizarData(transaccion.getIdCampanaTarifa());
					}
					if (transaccion.getUuid() != null) {
						uuid = normalizarData(transaccion.getUuid());
					}
					if (transaccion.getNumBoletaEnvio() != null) {
						numBoletaEnvio = normalizarData(transaccion.getNumBoletaEnvio());
					}
					if (transaccion.getCasaDeptoDestinatario() != null) {
						casaDeptoDestinatario = normalizarData(transaccion.getCasaDeptoDestinatario());
					}
					if (transaccion.getRefeDireccionDestinatario() != null) {
						refeDireccionDestinatario = normalizarData(transaccion.getRefeDireccionDestinatario());
					}
					if (transaccion.getLatDireccionDestinatario() != null) {
						latDireccionDestinatario = normalizarData(transaccion.getLatDireccionDestinatario());
					}
					if (transaccion.getLonDireccionDestinatario() != null) {
						lonDireccionDestinatario = normalizarData(transaccion.getLonDireccionDestinatario());
					}
					if (transaccion.getDocumentoRespuesta() != null) {
						documentoRespuesta = normalizarData(transaccion.getDocumentoRespuesta());
					}
					if (String.valueOf(transaccion.getId()) != null) {
						id = normalizarData(String.valueOf(transaccion.getId()));
					}
					if (String.valueOf(transaccion.getIdServicioTarifa()) != null) {
						idServicioTarifa = normalizarData(String.valueOf(transaccion.getIdServicioTarifa()));
					}
					if (transaccion.getIdToken() != null) {
						idToken = normalizarData(transaccion.getIdToken());
					}
					if (transaccion.getIdUsuario() != null) {
						idUsuario = normalizarData(transaccion.getIdUsuario().toString());
					}
					if (transaccion.getFechaCreacion() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(transaccion.getFechaCreacion());
						fechaCreacion = normalizarData(fecha1);
					}
					if (transaccion.getTotalArticulos() != null) {
						totalArticulos = normalizarData(transaccion.getTotalArticulos().toString());
					}
					if (transaccion.getCtdaPaquetes() != null) {
						ctdaPaquetes = normalizarData(transaccion.getCtdaPaquetes());
					}
					if (transaccion.getTotalPagar() != null) {
						totalPagar = normalizarData(transaccion.getTotalPagar().replace(".", ""));
					}
					if (transaccion.getCtdaDocumentos() != null) {
						ctdaDocumentos = normalizarData(transaccion.getCtdaDocumentos());
					}
					if (transaccion.getPesoEnvio() != null) {
						pesoEnvio = normalizarData(transaccion.getPesoEnvio().replace(".", ""));
					}
					if (transaccion.getTipoDestino() != null) {
						tipoDestino = normalizarData(transaccion.getTipoDestino());
					}
					if (transaccion.getIataOrigen() != null) {
						iataOrigen = normalizarData(transaccion.getIataOrigen());
					}
					if (transaccion.getIataDestino() != null) {
						iataDestino = normalizarData(transaccion.getIataDestino());
					}
					if (transaccion.getIataOficinaDestino() != null) {
						iataOficinaDestino = normalizarData(transaccion.getIataOficinaDestino());
					}
					if (transaccion.getMercado() != null) {
						mercado = normalizarData(transaccion.getMercado());
					}
					if (transaccion.getLargoEnvio() != null) {
						largoEnvio = normalizarData(transaccion.getLargoEnvio());
					}
					if (transaccion.getAltoEnvio() != null) {
						altoEnvio = normalizarData(transaccion.getAltoEnvio());
					}
					if (transaccion.getAnchoEnvio() != null) {
						anchoEnvio = normalizarData(transaccion.getAnchoEnvio());
					}
					if (transaccion.getPesoVolumen() != null) {
						pesoVolumen = normalizarData(transaccion.getPesoVolumen().replace(".", ""));
					}
					if (transaccion.getLinea() != null) {
						linea = normalizarData(transaccion.getLinea());
					}
					if (transaccion.getNroVentas() != null) {
						nroVentas = normalizarData(transaccion.getNroVentas());
					}
					if (transaccion.getFechaActualizacion() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(transaccion.getFechaActualizacion());
						fechaActualizacion = normalizarData(fecha1);
					}
					if (transaccion.getNumSeguimiento() != null) {
						numSeguimiento = normalizarData(transaccion.getNumSeguimiento());
					}
					if (transaccion.getNumTransaccion() != null) {
						numTransaccion =normalizarData(transaccion.getNumTransaccion());
					}
					if (transaccion.getIdToken() != null) {
						idToken = normalizarData(transaccion.getIdToken());
					}

					bw.write(id.replace(";", "") + separation + idToken.replace(";", "") + separation
							+ idUsuario.replace(";", "") + separation + fechaCreacion.replace(";", "") + separation
							+ fechaActualizacion.replace(";", "") + separation + totalArticulos.replace(";", "")
							+ separation + ctdaPaquetes.replace(";", "") + separation + ctdaDocumentos.replace(";", "")
							+ separation + pesoEnvio.replace(";", "") + separation + tipoDestino.replace(";", "")
							+ separation + iataOrigen.replace(";", "") + separation + iataDestino.replace(";", "")
							+ separation + iataOficinaDestino.replace(";", "") + separation + mercado.replace(";", "")
							+ separation + largoEnvio.replace(";", "") + separation + altoEnvio.replace(";", "")
							+ separation + anchoEnvio.replace(";", "") + separation + pesoVolumen.replace(";", "")
							+ separation + linea.replace(";", "") + separation + nroVentas.replace(";", "") + separation
							+ idServicioTarifa.replace(";", "") + separation + descripcionTarifa.replace(";", "")
							+ separation + tarifaBase.replace(";", "") + separation + tieneDescuento.replace(";", "")
							+ separation + montoDescuento.replace(";", "") + separation
							+ porcentajeDescuento.replace(";", "") + separation + idCampanaTarifa.replace(";", "")
							+ separation + totalPagar.replace(";", "") + separation + nroEnvio.replace(";", "")
							+ separation + tipoContenidoEnvio.replace(";", "") + separation
							+ descripcionContenidoEnvio.replace(";", "") + separation + valorContenido.replace(";", "")
							+ separation + numBoletaEnvio.replace(";", "") + separation
							+ mercanciasPeligrosas.replace(";", "") + separation
							+ primerNombreRemitente.replace(";", "") + separation
							+ apellidoPaternoRemitente.replace(";", "") + separation
							+ apellidoMaternoRemitente.replace(";", "") + separation + emailRemitente.replace(";", "")
							+ separation + telefonoRemitente.replace(";", "") + separation
							+ numeroDocumentoRemitente.replace(";", "") + separation
							+ tipoDocumentoRemitente.replace(";", "") + separation + recepcionRemitente.replace(";", "")
							+ separation + calleRemitente.replace(";", "") + separation
							+ numeroRemitente.replace(";", "") + separation + comunaRemitente.replace(";", "")
							+ separation + codPostalRemitente.replace(";", "").replace("-", "") + separation
							+ nombreImpositor.replace(";", "") + separation + apellidoImpositor.replace(";", "")
							+ separation + tipoImpositor.replace(";", "") + separation
							+ documentoImpositor.replace(";", "") + separation
							+ primerNombreDestinatario.replace(";", "") + separation
							+ apellidoPaternoDestinatario.replace(";", "") + separation
							+ apellidoMaternoDestinatario.replace(";", "") + separation
							+ emailDestinatario.replace(";", "") + separation + telefonoDestinatario.replace(";", "")
							+ separation + numeroDocumentoDestinatario.replace(";", "") + separation
							+ tipoDocumentoDestinatario.replace(";", "") + separation
							+ tipoRecepcionDestino.replace(";", "") + separation
							+ nombreSucursalDestino.replace(";", "") + separation + calleDestinatario.replace(";", "")
							+ separation + numeroDestinatario.replace(";", "") + separation
							+ comunaDestinatario.replace(";", "") + separation + casaDeptoDestinatario.replace(";", "")
							+ separation + codSucursalDestinatario.replace(";", "") + separation
							+ codPostalDestinatario.replace(";", "").replace("-", "") + separation
							+ refeDireccionDestinatario.replace(";", "") + separation
							+ latDireccionDestinatario.replace(";", "") + separation
							+ lonDireccionDestinatario.replace(";", "") + separation
							+ documentoRespuesta.replace(";", "") + separation + numSeguimiento.replace(";", "")
							+ separation + numTransaccion.replace(";", "") + separation + rut.replace(";", "")
							+ separation + uuid.replace(";", "") + separation + autorizacion.replace(";", "")
							+ separation + vci.replace(";", "") + separation + numeroFolio.replace(";", "") + separation
							+ urlBoleta.replace(";", ""));
					bw.newLine();
				}
			}
			bw.close();
			_log.info("[insertDataTransactions Sac] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataTransactions Sac ] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para ingresar datos de TransactionsTransbank.
	private void insertDataTransactionsTransbank(File file) throws ParseException {

		_log.info("[TransactionsTransbank] Iniciando vaciado de datos de transacciones en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;
		Date someDate = new Date();
		Date newDate = new Date(someDate.getTime()+ TimeUnit.DAYS.toMillis(-1));
		System.out.println("Ayer fue: " + newDate);
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = sdf.format(newDate);
		String dateStringUser = "";
		Date dateFormat = null;
		Date dateFormatUser = null;

		try {
			List<PreAdmisionTransaction> lst_transbank = PreAdmisionTransactionLocalServiceUtil
					.getPreAdmisionTransactions(-1, -1);
			_log.info("[TransactionsTransbank] Se agregaran datos en archivo");
			dateFormat = sdf.parse(dateString);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			for (PreAdmisionTransaction _transbank : lst_transbank) {
				_log.info("[transaccion] :" + _transbank.toString());
				if (_transbank.getFecha() == null) {
					_transbank.setFecha(new Date());

				}
				dateStringUser = sdf.format(_transbank.getFecha());
				String id = "";
				String uuid = "";
				String autorizacion = "";
				String comercio = "";
				String cuotas = "";
				String fecha = "";
				String message = "";
				String monto = "";
				String numeroTarjeta = "";
				String referencia = "";
				String resultado = "";
				String status = "";
				String token = "";
				String vci = "";
				String folioBoleta = "";
				String urlBoleta = "";

				dateFormatUser = sdf.parse(dateStringUser);
				if (dateFormatUser.compareTo(dateFormat) == 0) {

					if (String.valueOf(_transbank.getId()) != null) {
						id = normalizarData(String.valueOf(_transbank.getId()));
					}
					if (_transbank.getFolioBoleta() != null) {
						folioBoleta = normalizarData(_transbank.getFolioBoleta());
					}
					if (_transbank.getUrlBoleta() != null) {
						urlBoleta = normalizarDataReferencia(_transbank.getUrlBoleta());
					}
					if (_transbank.getUuid() != null) {
						uuid = normalizarData(_transbank.getUuid());
					}
					if (_transbank.getAutorizacion() != null) {
						autorizacion = normalizarData(_transbank.getAutorizacion());
					}
					if (_transbank.getFecha() != null) {
						DateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						String fecha1 = sd.format(_transbank.getFecha());
						fecha = normalizarData(fecha1);
					}
					if (_transbank.getComercio() != null) {
						comercio = normalizarData(_transbank.getComercio());
					}
					if (_transbank.getCuotas() != null) {
						cuotas = normalizarData(_transbank.getCuotas());
					}
					if (_transbank.getMessage() != null) {
						message = normalizarData(_transbank.getMessage());
					}
					if (_transbank.getMonto() != null) {
						monto = normalizarData(_transbank.getMonto());
					}
					if (_transbank.getNumeroTarjeta() != null) {
						numeroTarjeta = normalizarData(_transbank.getNumeroTarjeta());
					}
					if (_transbank.getReferencia() != null) {
						referencia = normalizarDataReferencia(_transbank.getReferencia());
					}
					if (_transbank.getResultado() != null) {
						resultado = normalizarData(_transbank.getResultado());
					}
					if (_transbank.getStatus() != null) {
						status = normalizarData(_transbank.getStatus());
					}
					if (_transbank.getToken() != null) {
						token = normalizarData(_transbank.getToken());
					}
					if (_transbank.getVci() != null) {
						vci = normalizarData(_transbank.getVci());
					}

					bw.write(id.replace(";", "") + separation + uuid.replace(";", "") + separation + autorizacion.replace(";", "") + separation + comercio.replace(";", "") + separation
							+ cuotas.replace(";", "") + separation + fecha.replace(";", "") + separation + message.replace(";", "") + separation + monto.replace(";", "") + separation
							+ numeroTarjeta.replace(";", "") + separation + referencia.replace(";", "") + separation + resultado.replace(";", "") + separation + status.replace(";", "")
							+ separation + token.replace(";", "") + separation + vci.replace(";", "") + separation + folioBoleta.replace(";", "") + separation
							+ urlBoleta.replace(";", ""));
					bw.newLine();
				}
			}
			bw.close();
			_log.info("[insertDataTransactionsTransbank] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataTransactionsTransbank] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para ingresar datos de direccion personal en texto.
	private void insertDataDireccionPersonal(File file) {
		_log.info("[insertDataDireccionPersonal] Iniciando vaciado de datos usuarios en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;
		try {
			_log.info("[insertDataDireccionPersonal] Se agregaran datos en archivo");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);

			List<Direccion_personal> direcciones = Direccion_personalLocalServiceUtil.getDireccion_personals(-1, -1);

			for (Direccion_personal direccion : direcciones) {
				_log.info("[direccion Personal] :" + direccion.toString());

				String calle = "";
				String codigo_Postal = "";
				String comuna = "";
				String depto = "";
				String emailPersonal = "";
				String id_Direccion = "";
				String latitud = "";
				String longitud = "";
				String nombre_direccion = "";
				String numero = "";
				String numero_documento = "";
				String preferencial = "";
				String referencia = "";
				String region = "";
				String telefono = "";
				String tipo_direccion = "";
				String uuid_ = "";

				if (direccion.getNumero_documento() != null) {
					numero_documento = normalizarData(direccion.getNumero_documento());
				}

				if (direccion.getCalle() != null) {
					calle = normalizarData(direccion.getCalle());
				}

				if (direccion.getNumero() != null) {
					numero = normalizarData(direccion.getNumero());
				}

				if (direccion.getDepto() != null) {
					depto = normalizarData(direccion.getDepto());
				}
				if (direccion.getComuna() != null) {
					comuna = normalizarData(direccion.getComuna());
				}
				if (direccion.getCodigo_Postal() != null) {
					codigo_Postal = normalizarData(direccion.getCodigo_Postal());
				}
				if (direccion.getEmailPersonal() != null) {
					emailPersonal = normalizarData(direccion.getEmailPersonal());
				}
				if (String.valueOf(direccion.getId_Direccion()) != null) {
					id_Direccion = normalizarData(String.valueOf(direccion.getId_Direccion()));
				}
				if (direccion.getLatitud() != null) {
					latitud = normalizarData(direccion.getLatitud());
				}
				if (direccion.getLongitud() != null) {
					longitud = normalizarData(direccion.getLongitud());
				}
				if (direccion.getNombre_direccion() != null) {
					nombre_direccion = normalizarData(direccion.getNombre_direccion());
				}
				if (String.valueOf(direccion.getPreferencial()) != null) {
					preferencial = normalizarData(direccion.getCodigo_Postal());
				}
				if (direccion.getReferencia() != null) {
					referencia = normalizarDataReferencia(direccion.getReferencia());
				}
				if (direccion.getRegion() != null) {
					region = normalizarData(direccion.getRegion());
				}
				if (direccion.getTelefono() != null) {
					telefono = normalizarData(direccion.getTelefono());
				}
				if (direccion.getTipo_direccion() != null) {
					tipo_direccion = normalizarData(direccion.getTipo_direccion());
				}
				if (direccion.getUuid() != null) {
					uuid_ = normalizarData(direccion.getUuid());
				}

				bw.write(uuid_ .replace(";", "")+ separation + id_Direccion.replace(";", "") + separation + numero_documento.replace(";", "")  + separation
						+ nombre_direccion.replace(";", "") + separation + region.replace(";", "") + separation + comuna.replace(";", "") + separation + calle.replace(";", "") + separation
						+ numero.replace(";", "") + separation + depto.replace(";", "") + separation + codigo_Postal.replace(";", "").replace("-", "") + separation + latitud.replace(";", "") + separation
						+ longitud.replace(";", "") + separation + telefono.replace(";", "") + separation + tipo_direccion.replace(";", "") + separation + preferencial.replace(";", "")
						+ separation + referencia.replace(";", "") + separation + emailPersonal.replace(";", ""));
				bw.newLine();
			}
			bw.close();
			_log.info("[insertDataDireccionPersonal] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataDireccionPersonal] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para ingresar datos de direccion destinatarios en texto.
	private void insertDataDireccionDestinatarios(File file) {
		_log.info("[insertDataDireccionDestinatarios] Iniciando vaciado de datos usuarios en archivo");
		String separation = ClCchJobRegistroKpiPortletKeys.DATA_SEPARATION;
		try {
			_log.info("[insertDataDireccionDestinatarios] Se agregaran datos en archivo");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);

			List<Direccion_destinatario> direcciones = Direccion_destinatarioLocalServiceUtil
					.getDireccion_destinatarios(-1, -1);

			for (Direccion_destinatario direccion : direcciones) {


				String uuid_ = "";
				String id_Direccion = "";
				String nombre_direccion = "";
				String region = "";
				String comuna = "";
				String calle = "";
				String numero = "";
				String depto = "";
				String codigo_Postal = "";
				String latitud = "";
				String longitud = "";
				String nombre_destinatario = "";
				String apellido_destinatario = "";
				String email_destinatario = "";
				String rut_destinatario = "";
				String telefono_destinatario = "";
				String tipo_direccion = "";
				String numero_documento = "";
				String referencia = "";
				String emailPersonal = "";

				if (direccion.getApellido_destinatario() != null) {
					apellido_destinatario = normalizarData(direccion.getApellido_destinatario());
				}
				if (direccion.getEmailPersonal() != null) {
					email_destinatario = normalizarData(direccion.getEmailPersonal());
				}
				if (direccion.getRut_destinatario() != null) {
					rut_destinatario = normalizarData(direccion.getRut_destinatario());
				}
				if (direccion.getEmailPersonal() != null) {
					emailPersonal = normalizarData(direccion.getEmailPersonal());
				}
				if (direccion.getNumero_documento() != null) {
					numero_documento = normalizarData(direccion.getNumero_documento());
				}
				if (direccion.getCalle() != null) {
					calle = normalizarData(direccion.getCalle());
				}
				if (direccion.getNumero() != null) {
					numero = normalizarData(direccion.getNumero());
				}
				if (direccion.getDepto() != null) {
					depto = normalizarData(direccion.getDepto());
				}
				if (direccion.getComuna() != null) {
					comuna = normalizarData(direccion.getComuna());
				}
				if (direccion.getCodigo_Postal() != null) {
					codigo_Postal = normalizarData(direccion.getCodigo_Postal());
				}
				if (direccion.getEmailPersonal() != null) {
					emailPersonal = normalizarData(direccion.getEmailPersonal());
				}
				if (String.valueOf(direccion.getId_Direccion()) != null) {
					id_Direccion = normalizarData(String.valueOf(direccion.getId_Direccion()));
				}
				if (direccion.getLatitud() != null) {
					latitud = normalizarData(direccion.getLatitud());
				}
				if (direccion.getLongitud() != null) {
					longitud = normalizarData(direccion.getLongitud());
				}
				if (direccion.getNombre_direccion() != null) {
					nombre_direccion = normalizarData(direccion.getNombre_direccion());
				}
				if (direccion.getNombre_destinatario() != null) {
					nombre_destinatario = normalizarData(direccion.getNombre_destinatario());
				}
				if (direccion.getReferencia() != null) {
					referencia = normalizarDataReferencia(direccion.getReferencia());
				}
				if (direccion.getRegion() != null) {
					region = normalizarData(direccion.getRegion());
				}
				if (direccion.getTelefono_destinatario() != null) {
					telefono_destinatario = normalizarData(direccion.getTelefono_destinatario());
				}
				if (direccion.getTipo_direccion() != null) {
					tipo_direccion = normalizarData(direccion.getTipo_direccion());
				}
				if (direccion.getUuid() != null) {
					uuid_ = normalizarData(direccion.getUuid());
				}

				bw.write(uuid_.replace(";", "") + separation + id_Direccion.replace(";", "") + separation + nombre_direccion.replace(";", "") + separation + region.replace(";", "")
						+ separation + comuna.replace(";", "") + separation + calle.replace(";", "") + separation + numero.replace(";", "") + separation + depto.replace(";", "")
						+ separation + codigo_Postal.replace(";", "").replace("-", "") + separation + latitud.replace(";", "") + separation + longitud.replace(";", "") + separation
						+ nombre_destinatario.replace(";", "") + separation + apellido_destinatario.replace(";", "") + separation + email_destinatario.replace(";", "")
						+ separation + rut_destinatario.replace(";", "") + separation + telefono_destinatario.replace(";", "") + separation
						+ tipo_direccion.replace(";", "") + separation + numero_documento.replace(";", "") + separation + referencia.replace(";", "") + separation
						+ emailPersonal.replace(";", ""));
				bw.newLine();
			}
			bw.close();
			_log.info("[insertDataDireccionDestinatarios] Vaciado de datos realizada con exito");
		} catch (Exception e) {
			_log.error("[insertDataDireccionDestinatarios] Error al llenar datos en archivo: ", e);
		}
	}

	// M�todo para setear datos de servidor SFTP.
	private static boolean sendFileSFTP(String filePath, JobKpiConfig _jobKipConfig) {
		_log.info("[sendFileSFTP] Iniciando env�o de archivo...");
		String host = _jobKipConfig.getHostConfig();
		int port = Integer.parseInt(_jobKipConfig.getPortConfig());
		String username = _jobKipConfig.getUserConfig();
		String password = _jobKipConfig.getPasswordConfig();
		String sftpPath = _jobKipConfig.getSftpPathConfig();

		if (filePath.contains(ClCchJobRegistroKpiPortletKeys.FILE_TRANSACTIONS_SAC)) {
			String sftpPathSac = _jobKipConfig.getSftpPathConfigSac();
			return loadFileSFTP(host, port, username, password, sftpPathSac, filePath);
		}else {
			_log.info("[sendFileSFTP] Archivo se subira a host: " + host);
			return loadFileSFTP(host, port, username, password, sftpPath, filePath);
		}
	}

	// M�todo para subir el archivo de texto creado al servidor SFTP.
	private static boolean loadFileSFTP(String host, int port, String username, String password, String sftpPath,
			String filePath) {
		JSch sftp = new JSch();
		Session session = null;
		ChannelSftp channelSftp = null;
		boolean subido = false;
		try {
			session = sftp.getSession(username, host, port);
			session.setPassword(password);
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			session.setConfig(prop);
			session.connect();
			channelSftp = (ChannelSftp) session.openChannel("sftp");
			channelSftp.connect();
			channelSftp.put(filePath, sftpPath);
			_log.info("[loadFileSFTP] Archivo Subido Exitosamente: " + filePath);
			subido = true;
		} catch (SftpException ex) {
			_log.info("[loadFileSFTP]  SftpException");
			_log.info(
					"[loadFileSFTP] Error al subir o buscar archivo: ----" + filePath + "--- No se ha dejado en sftp");
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		} catch (Exception e) {
			_log.info("[loadFileSFTP]  Exception", e);
			_log.info(
					"[loadFileSFTP] Error al subir o buscar archivo: ----" + filePath + "--- No se ha dejado en sftp");
			e.printStackTrace();
			System.out.println(e.getMessage());
		} finally {
			_log.info("[loadFileSFTP]  Finally");
			if (channelSftp.isConnected()) {
				channelSftp.exit();
				channelSftp.disconnect();
			}
			if (session.isConnected())
				session.disconnect();
		}
		_log.info("[loadFileSFTP] Fin loadFileSFTP, valor de funcion subido: " + subido);
		return subido;
	}

	public String normalizarData(String data) throws Exception {
		if(data.length()<=75) {
			return data;
		}else {
			throw new Exception("campo no normalizado :"+data);
		}
			
	}
	public String normalizarDataReferencia(String data) throws Exception {
		if(data.length()<=200) {
			return data;
		}
		else {
			throw new Exception("campo no normalizado :"+data);
		}	
	}
}

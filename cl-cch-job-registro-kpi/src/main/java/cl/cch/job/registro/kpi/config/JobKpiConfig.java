package cl.cch.job.registro.kpi.config;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;
import cl.cch.job.registro.kpi.constants.ClCchJobRegistroKpiPortletKeys;

@Meta.OCD(
	id = ClCchJobRegistroKpiPortletKeys.JOBS_CCH_CONFIGURATION_PID,
	localization = "content/Language", 
	name = ClCchJobRegistroKpiPortletKeys.JOBS_CCH_CONFIGURATION_NAME
)
public interface JobKpiConfig {

	public static final String CRON_EXPRESSION_CONFIG = "cron-expression-config";
	public static final String HOST = "host-config";
	public static final String PUERTO = "puerto-config";
	public static final String USUARIO = "usuario-config";
	public static final String PASSWORD = "password-config";
	public static final String SFTP_PATH = "sftp-path-config";
	public static final String SFTP_PATH_SAC = "sftp-path-sac-config";
	public static final String FILE_PATH = "file-path-config";
	
	@Meta.AD(name=CRON_EXPRESSION_CONFIG, description = "Configuracion CRON de Job env�o archivos KPI", deflt = "0 0 7 1/1 * ? *", required = false, type = Type.String)
	public String getCronExpressionConfig();
	
	@Meta.AD(name=HOST, description = "Host SFTP para env�o de archivos", deflt = "201.238.220.53", required = false, type = Type.String)
	public String getHostConfig();
	
	@Meta.AD(name=PUERTO, description = "Puerto de servidor para env�o de archivos", deflt = "22", required = false, type = Type.String)
	public String getPortConfig();
	
	@Meta.AD(name=USUARIO, description = "Usuario de servidor para env�o de archivos", deflt = "ext_liferay", required = false, type = Type.String)
	public String getUserConfig();
	
	@Meta.AD(name=PASSWORD, description = "Password de usuario para env�o de archivos", deflt = "&k3uCeULwQsC=4vm", required = false, type = Type.String)
	public String getPasswordConfig();
	
	@Meta.AD(name=SFTP_PATH, description = "Ruta de archivos en donde se almacenar�n los registros", deflt = "/entrada/", required = false, type = Type.String)
	public String getSftpPathConfig();
	
	@Meta.AD(name=FILE_PATH, description = "Ruta donde se guardar� el archivo de forma local", deflt = "", required = false, type = Type.String)
	public String getFilePathConfig();
	
	@Meta.AD(name=SFTP_PATH_SAC, description = "Ruta de archivos en donde se almacenar�n los registros sac ", deflt = "/entrada/SAC/", required = false, type = Type.String)
	public String getSftpPathConfigSac();
	
}


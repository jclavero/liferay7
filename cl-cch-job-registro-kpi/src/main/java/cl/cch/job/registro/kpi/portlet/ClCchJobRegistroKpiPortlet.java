package cl.cch.job.registro.kpi.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;

import cl.cch.job.registro.kpi.constants.ClCchJobRegistroKpiPortletKeys;
import cl.cch.job.registro.kpi.helpers.HelperMantentedor;
import cl.cch.pre.ingreso.sb.model.PreAdmision;
import cl.cch.pre.ingreso.sb.service.PreAdmisionLocalServiceUtil;
import cl.cch.pre.ingreso.transaction.sb.model.PreAdmisionTransaction;
import cl.cch.pre.ingreso.transaction.sb.service.PreAdmisionTransactionLocalServiceUtil;
/**
 * @author jclavero
 */
@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50", "com.liferay.portlet.use-default-template=true",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=cl-cch-job-registro-kpi Portlet",
		"javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.portlet-mode=text/html;view",
		"javax.portlet.name=" + ClCchJobRegistroKpiPortletKeys.ClCchJobRegistroKpi,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=administrator",
		"javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class ClCchJobRegistroKpiPortlet extends MVCPortlet {

	private static final Log _log = LogFactoryUtil.getLog(ClCchJobRegistroKpiPortlet.class);

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		_log.info("[doView] inicio de doView");

		super.doView(renderRequest, renderResponse);
	}

	@Override
	public final void serveResource(final ResourceRequest request, final ResourceResponse response)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String resourceID = request.getResourceID();
		String year = ParamUtil.getString(request, "anno").trim();
		String month = ParamUtil.getString(request, "mes").trim();
		String day = ParamUtil.getString(request, "dia").trim();
		String rut = ParamUtil.getString(request, "rut").trim();
		String email = ParamUtil.getString(request, "email").trim();

		GregorianCalendar fecha = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month)-1,
				Integer.parseInt(day));
		
		HelperMantentedor helperMantenedor = new HelperMantentedor();
		try {
			
			_log.info("[MANTENEDOR]  inicio Registros KPI");
			if (resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.USUARIOS_URL)) {	
				helperMantenedor.registrarKpiMantenedor(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_USUARIOS, response,
						fecha.getTime());
			}else if(resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.TRANSACCIONES_URL)) {
				helperMantenedor.registrarKpiMantenedor(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES,
						response, fecha.getTime());
			}else if(resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.TRANSABANK_URL)) {
				helperMantenedor.registrarKpiMantenedor(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_TB,
						response, fecha.getTime());
			}else if(resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.DPERSONAL_URL)) {
				helperMantenedor.registrarKpiMantenedor(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_PERSONAL,
						response, fecha.getTime());
			}else if(resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.DDESTINATARIO_URL)) {
				helperMantenedor.registrarKpiMantenedor(
						ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_DESTINATARIO, response, fecha.getTime());
			}
			else if(resourceID.equalsIgnoreCase(ClCchJobRegistroKpiPortletKeys.SAC_URL)) {
				
				JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
				String autorizacion ="";
				String vci ="";
				String numeroFolio ="";
				String urlBoleta ="";
				
				if(rut!=null) {
					User user = null;
					try {			
						 user=helperMantenedor.getUserNdoc(themeDisplay, rut);
							List<PreAdmision> transacciones_lst = PreAdmisionLocalServiceUtil.getFindBy_IdUsuario(user.getUserId());			 
							for(PreAdmision transaccion:transacciones_lst) {
								try {
									PreAdmisionTransaction lst_transbank = PreAdmisionTransactionLocalServiceUtil
											.getFindBy_Referencia(transaccion.getIdToken());

									if (lst_transbank != null) {
										autorizacion = helperMantenedor.normalizarData(lst_transbank.getAutorizacion());
										vci = helperMantenedor.normalizarData(lst_transbank.getVci());
										numeroFolio = helperMantenedor.normalizarData(lst_transbank.getFolioBoleta());
										urlBoleta = helperMantenedor.normalizarDataReferencia(lst_transbank.getUrlBoleta());
									}
								} catch (Exception e) {
									_log.error("error autorizacion :" + e.getMessage());
								}	
								 //Creating the ObjectMapper object
							      ObjectMapper mapper = new ObjectMapper();
							      //Converting the Object to JSONString
							      String jsonString = mapper.writeValueAsString(transacciones_lst);
							      System.out.println(jsonString);
		
								JSONObject jsonObject = new JSONFactoryUtil().createJSONObject(jsonString);
								jsonObject.put("autorizacion", autorizacion);
								jsonObject.put("vci", vci);
								jsonObject.put("numeroFolio", numeroFolio);
								jsonObject.put("urlBoleta", urlBoleta);
								
								jsonArray.put(jsonObject);
							}

							PrintWriter out = response.getWriter();
							  out.write(jsonArray.toString());
						        out.flush();
						        out.close();
						        
					} catch (PortalException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(email!=null){
					
				}else {
					helperMantenedor.registrarKpiMantenedor(
							ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_SAC, response, fecha.getTime());
				}

			}
			_log.info("[MANTENEDOR] fin Registros KPI");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
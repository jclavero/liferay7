package cl.cch.job.registro.kpi.job;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelper;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.StorageTypeAware;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactory;
import com.liferay.portal.kernel.util.GetterUtil;

import java.util.Date;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import cl.cch.job.registro.kpi.config.JobKpiConfig;
import cl.cch.job.registro.kpi.constants.ClCchJobRegistroKpiPortletKeys;
import cl.cch.job.registro.kpi.helpers.HelperFile;

@Component(immediate = true, 
			property =  { "cron.expression=0 0 7 1/1 * ? *"}, 
			service = SchedulerJobDiario.class, 
			configurationPid = ClCchJobRegistroKpiPortletKeys.JOBS_CCH_CONFIGURATION_PID, 
			configurationPolicy = ConfigurationPolicy.OPTIONAL)

public class SchedulerJobDiario extends BaseMessageListener{
	private static final Log _log = LogFactoryUtil.getLog(SchedulerJobDiario.class);
	private TriggerFactory _triggerFactory;
	private SchedulerEngineHelper _schedulerEngineHelper;
	private SchedulerEntryImpl _schedulerEntryImpl = null;
	private volatile JobKpiConfig _jobKipConfig;
	private volatile boolean initialized;

	/**
    *
    * With this reference, this component activation waits until portal initialization has completed.
    * @param moduleServiceLifecycle
    */
   @Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
   protected void setModuleServiceLifecycle(
       ModuleServiceLifecycle moduleServiceLifecycle) {
   }

   @Reference(unbind = "-")
   protected void setTriggerFactory(TriggerFactory triggerFactory) {
       _triggerFactory = triggerFactory;
   }

   @Reference(unbind = "-")
   protected void setSchedulerEngineHelper(
       SchedulerEngineHelper schedulerEngineHelper) {
       _schedulerEngineHelper = schedulerEngineHelper;
   }

   /**
      * doReceive: This is where the magic happens, this is where you want to do the work for
      * the scheduled job.
      * @param message This is the message object tied to the job.  If you stored data with the
      *                job, the message will contain that data.
      * @throws Exception In case there is some sort of error processing the task.
      */
   @Override
   protected void doReceive(Message message) throws Exception {
       _log.info("[doReceive()] Inicio job diario Registros KPI");
       HelperFile helperFile = new HelperFile();
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_USUARIOS, _jobKipConfig);
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES, _jobKipConfig);
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_SAC, _jobKipConfig);
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_TRANSACCIONES_TB, _jobKipConfig);
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_PERSONAL, _jobKipConfig);
       helperFile.registrarKpi(ClCchJobRegistroKpiPortletKeys.CREATE_FILE_DIRECCIONES_DESTINATARIO, _jobKipConfig);
       _log.info("[doReceive()] fin job diario Registros KPI");
   }

   
   /**
    * activate: Called whenever the properties for the component change (ala Config Admin)
    * or OSGi is activating the component.
    * @param properties The properties map from Config Admin.
    * @throws SchedulerException in case of error.
    */
   @Activate
   @Modified
   protected void activate(Map<String, Object> properties)
       throws SchedulerException {
       //String cronExpresionConfig = GetterUtil.getString(properties.get("cron.expression"), _DEFAULT_CRON_EXPRESSION);
	   _jobKipConfig = ConfigurableUtil.createConfigurable(JobKpiConfig.class,
               properties);

       String cronExpresionConfig = _jobKipConfig.getCronExpressionConfig();
       String cronExpression = GetterUtil.getString(cronExpresionConfig,
               GetterUtil.getString(properties.get("cron.expression")));
       
       _log.info("[cron_expresion :]"+cronExpression);

       String listenerClass = getClass().getName();
       Trigger jobTrigger = _triggerFactory.createTrigger(listenerClass,
               listenerClass, new Date(), null, cronExpression); // create a new trigger definition for the job

       _schedulerEntryImpl = new SchedulerEntryImpl();
       _schedulerEntryImpl.setEventListenerClass(listenerClass);
       _schedulerEntryImpl.setTrigger(jobTrigger);
       _schedulerEntryImpl = new StorageTypeAwareSchedulerEntryImpl(_schedulerEntryImpl,
               StorageType.PERSISTED);

       if (initialized) {
           deactivate();
       }

       _schedulerEngineHelper.register(this, _schedulerEntryImpl,
           DestinationNames.SCHEDULER_DISPATCH); // register the scheduled task

       initialized = true;
   }

   @Deactivate
   protected void deactivate() {
       if (initialized) {
           try {
               _schedulerEngineHelper.unschedule(_schedulerEntryImpl,
                   getStorageType()); // unschedule the job so it is cleaned up
           } catch (SchedulerException se) {
               if (_log.isWarnEnabled()) {
                   _log.warn("[deactivate()] Unable to unschedule trigger", se);
               }
           }

           _schedulerEngineHelper.unregister(this);
       }

       initialized = false;
   }

   /**
    * getStorageType: Utility method to get the storage type from the scheduler entry wrapper.
    * @return StorageType The storage type to use.
    */
   protected StorageType getStorageType() {
       if (_schedulerEntryImpl instanceof StorageTypeAware) {
           return ((StorageTypeAware) _schedulerEntryImpl).getStorageType();
       }

       return StorageType.MEMORY_CLUSTERED;
   }

}

package cl.cch.job.registro.kpi.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import cl.cch.job.registro.kpi.constants.ClCchJobRegistroKpiPortletKeys;

/**
 * @author Juan Clavero
 */

@Component(immediate = true, property = { "panel.app.order:Integer=500",
		"panel.category.key=" + ClCchJobRegistroKpiPortletKeys.ClCchJobRegistroKpi }, service = PanelApp.class)
public class TarificadorAdminPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ClCchJobRegistroKpiPortletKeys.ClCchJobRegistroKpi;
	}

	@Override
	@Reference(target = "(javax.portlet.name=" + ClCchJobRegistroKpiPortletKeys.ClCchJobRegistroKpi + ")", unbind = "-")
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}
}

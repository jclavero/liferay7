<%@ include file="init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>


<portlet:resourceURL var="usuariosURL" id="usuariosURL"></portlet:resourceURL>
<portlet:resourceURL var="transaccionesURL" id="transaccionesURL"></portlet:resourceURL>
<portlet:resourceURL var="transabankURL" id="transabankURL"></portlet:resourceURL>
<portlet:resourceURL var="direccionesPersonalesURL" id="direccionesPersonalesURL"></portlet:resourceURL>
<portlet:resourceURL var="direccionesDestinatarioURL" id="direccionesDestinatarioURL"></portlet:resourceURL>
<portlet:resourceURL var="transaccionesSacURL" id="transaccionesSacURL"></portlet:resourceURL>

<form id="<portlet:namespace/>formFormulario" method="post">


<div class="content" style="margin:100px">

			<div class="row">
							<div class="col-md-4">
								<div class="form-group mb-4">
									<label class="f-common-bold small-text"> 
									<span class="resumen-icon d-inline-block" ></span>
									Tipo de Consulta
									</label> 
									<select class="form-select small-text" id="<portlet:namespace/>selectTipoRegistro" onchange="registro(this.value);">
										<option value="">Seleccione un Tipo de Registro</option>
										<option value="usuarios">usuarios</option>
										<option value="transacciones">transacciones</option>
										<option value="transabank">transabank</option>
										<option value="direccionPersonal">direccion Personal</option>
										<option value="direccionDestinatario">direccion Destinatario</option>
										<option value="sac">sac</option>
									</select>
								</div>
								<input class="form-field" id="<portlet:namespace/>tipoRegistro" name="<portlet:namespace/>tipoRegistro" type="hidden"  value="" required="" readonly="readonly">
								
						</div>
						
							<div id="usuario_input" style="display: none">	
						</div>

                         <div id="transacciones_input" style="display: none">      
						</div>


						<div id="transbank_input" style="display: none">		
						</div>


						<div id="dpersonales_input" style="display: none">		
						</div>


						<div id="ddestinatarios_input" style="display: none">				
						</div>


						<div id="sac_input" style="display: none">
			
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mb-4">
										<label class="form-label fs-1" for="<portlet:namespace/>email" >Ingrese Correo Electronico :</label>
										<input class="form-field" id="<portlet:namespace/>email" name="<portlet:namespace/>email" type="text" maxlength="50" value="" required="">
									</div>
								</div>
								
							</div>
						
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mb-4">
										<label class="form-label fs-1" for="<portlet:namespace/>rut" >Ingrese rut a consultar :</label>
										<input class="form-field" id="<portlet:namespace/>rut" name="<portlet:namespace/>rut" type="text" maxlength="50" value="" required="">
									</div>
								</div>
							</div>

						</div>
						
						<div class="row">
	

<div id="fecha" style="display: none">
	
<div class="form-group">
				<select class="form-select small-text" id="select_day"
					name="<portlet:namespace/>dia">
					<option value="">D&iacute;a</option>
				</select>
			</div>

		
			<div class="form-group">
				<select class="form-select small-text" id="select_month"
					name="<portlet:namespace/>mes">
					<option value="0">Mes</option>
					<option value="01">Enero</option>
					<option value="02">Febrero</option>
					<option value="03">Marzo</option>
					<option value="04">Abril</option>
					<option value="05">Mayo</option>
					<option value="06">Junio</option>
					<option value="07">Julio</option>
					<option value="08">Agosto</option>
					<option value="09">Septiembre</option>
					<option value="10">Octubre</option>
					<option value="11">Noviembre</option>
					<option value="12">Diciembre</option>
				</select>
			</div>
		
			<div class="form-group">
				<select class="form-select small-text" id="select_year"
					name="<portlet:namespace/>anno">
					<option value="">A&ntilde;o</option>
				</select>
			</div>

</div>


<div id="usuarios" style="display: none">
<button a href="<%=usuariosURL%>" type="button" onclick="enviarFormulario('usuarios');" class="btn btn-corp btn-normal">Usuarios</button>
</div>
<div id="transacciones" style="display: none">
<button a href="<%=transaccionesURL%>" type="button" onclick="enviarFormulario('transacciones')" class="btn btn-corp btn-normal">Transacciones</button>
</div>
<div id="transbank" style="display: none"> 
<button a href="<%=transabankURL%>" type="button" onclick="enviarFormulario('transbank')" class="btn btn-corp btn-normal">Transabank</button>
</div>
<div id="sac" style="display: none">
<button a href="<%=transaccionesSacURL%>" type="button" onclick="enviarFormulario('sac')" class="btn btn-corp btn-normal">Sac</button>
</div>	
<div id="dPeronales" style="display: none">
<button a href="<%=direccionesPersonalesURL%>" type="button" onclick="enviarFormulario('dPeronales')" class="btn btn-corp btn-normal">Direcciones Personales</button>
</div>
<div id="dDestinatarios" style="display: none">
<button a href="<%=direccionesDestinatarioURL%>" type="button" onclick="enviarFormulario('dDestinatarios')" class="btn btn-corp btn-normal">Direcciones Destinatarios</button>
</div>
			
		
</div>
		
</div>
	
	<div id="visor"></div>
		
</form>

<script type="text/javascript">

var formFormulario = $('#<portlet:namespace/>formFormulario');

var usuariosURL = '${usuariosURL}';
var transaccionesURL = '${transaccionesURL}';
var transabankURL = '${transabankURL}';
var direccionesPersonalesURL = '${direccionesPersonalesURL}';
var direccionesDestinatarioURL = '${direccionesDestinatarioURL}';
var transaccionesSacURL = '${transaccionesSacURL}';
var downloadURL = null;
var fileName = null;
var usuarios_ = "usuarios_";
var transacciones_ = "transacciones_";
var transbank_ = "transbank_";
var dPeronales_ = "dPeronales_";
var dDestinatarios_ = "dDestinatarios_";
var sac_ = "sac_";
var extencion = ".txt";
var years = $("#select_year");
var month = $("#select_month");
var dia = $("#select_day");
var rut = $("#rut");
var email = $("#email");

				var $select_day = $("#select_day");
				for (var i = 1; i < 32; i++) {
					var day_number = i;
					$('<option>').val(('0' + day_number).slice(-2)) // set the value
					.text(i) // set the text in in the <option>
					.appendTo($select_day);
				}
				// :: YEAR
				var $select_year = $("#select_year");

				// Get the current year
				var year = new Date().getFullYear();
				var $select_year = $('#select_year');

				for (var i = 0; i < 99; i++) {
					$('<option>').val(year - i) // set the value
					.text(year - i) // set the text in in the <option>
					.appendTo($select_year);
				}
				
				function enviarFormulario(tipoRegitro) {
					
					var today = new Date();
					var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
					console.log(date);
					
					if(tipoRegitro=="usuarios"){
						console.log("usuarios");
					  downloadURL = usuariosURL;
					  fileName=usuarios_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					if(tipoRegitro=="transacciones"){
						console.log("transacciones");
					  downloadURL = transaccionesURL;
					  fileName=transacciones_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					if(tipoRegitro=="transbank"){
						console.log("transbank");
					  downloadURL = transabankURL;
					  fileName=transbank_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					if(tipoRegitro=="dPeronales"){
						console.log("dPeronales");
					  downloadURL = direccionesPersonalesURL;
					  fileName=dPeronales_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					if(tipoRegitro=="dDestinatarios"){
						console.log("dDestinatarios");
					  downloadURL = direccionesDestinatarioURL;
					  fileName=dDestinatarios_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					if(tipoRegitro=="sac"){
						console.log("sac");
					  downloadURL = transaccionesSacURL;
					  fileName=sac_+date+extencion;
					  console.log(fileName);
						formFormulario.submit();
					}
					
				}
				formFormulario.submit(function(event) {
					event.preventDefault();
					if (validarFormulario()) {
					
						$.ajax({
							type : 'post',
							url : downloadURL,
							data : formFormulario.serialize(),
							datatype : 'json',
							cache : false,
							error : function(xhr, status, error) {
								$('html, body').animate({scrollTop:200}, 'slow');
							},
							success : function(data) {
								console.log(data);
								$("#visor").html(data);
									/*  saveDoc(fileName, data);
									alert("Archivo descargado") */
									$('html, body').animate({scrollTop:200}, 'slow');
											
							}
						});
					} else {
					
						$('html, body').animate({
							scrollTop : 200
						}, 'slow');
					}
				});
				function validarFormulario() {

					if (years.val() == "") {
						console.log("yearsinput");
						return false;
					}

					if (month.val() == "") {
						console.log("monthinput");
						return false;
					}
					if (dia.val() == "") {
						console.log("diainput");
						return false;
					}

					return true;
				}
				
				saveDoc = function(filename, data) {
					  var blob = new Blob([data], {
					    type: 'text/csv'
					  });
					  if (window.navigator.msSaveOrOpenBlob) {
					    window.navigator.msSaveBlob(blob, filename);
					  } else {
					    var elem = window.document.createElement('a');
					    elem.href = window.URL.createObjectURL(blob);
					    elem.download = filename;
					    document.body.appendChild(elem);
					    elem.click();
					    document.body.removeChild(elem);
					  }
					}
				
				function registro(reg){
					console.log(reg);
					if(reg == "usuarios"){
				
						$("#usuarios").show();
						$("#transacciones").hide();
						$("#transbank").hide();
						$("#sac").hide();
						$("#dPeronales").hide();
						$("#dDestinatarios").hide();
						$("#sac_input").hide();
						$("#fecha").show();
					}else if(reg == "transacciones"){	
					
							$("#usuarios").hide();
						$("#transacciones").show();
						$("#transbank").hide();
						$("#sac").hide();
						$("#dPeronales").hide();
						$("#dDestinatarios").hide();
						$("#sac_input").hide();
						$("#fecha").show();
					}else if(reg == "transabank"){	
					
								$("#usuarios").hide();
						$("#transacciones").hide();
						$("#transbank").show();
						$("#sac").hide();
						$("#dPeronales").hide();
						$("#dDestinatarios").hide();
						$("#sac_input").hide();
						$("#fecha").show();
					}else if(reg == "direccionPersonal"){	
					
						$("#usuarios").hide();
						$("#transacciones").hide();
						$("#transbank").hide();
						$("#sac").hide();
						$("#dPeronales").show();
						$("#dDestinatarios").hide();
						$("#sac_input").hide();
						$("#fecha").hide();
					}else if(reg == "direccionDestinatario"){	
					
						$("#usuarios").hide();
						$("#transacciones").hide();
						$("#transbank").hide();
						$("#sac").hide();
						$("#dPeronales").hide();
						$("#dDestinatarios").show();
						$("#sac_input").hide();
						$("#fecha").hide();
					}else if(reg == "sac"){	
						$("#sac_input").show();
						$("#usuarios").hide();
						$("#transacciones").hide();
						$("#transbank").hide();
						$("#sac").show();
						$("#dPeronales").hide();
						$("#dDestinatarios").hide();
						$("#fecha").show();
					}else{
									$("#usuarios").hide();
						$("#transacciones").hide();
						$("#transbank").hide();
						$("#sac").hide();
						$("#dPeronales").hide();
						$("#dDestinatarios").hide();
						$("#fecha").hide();
						$("#sac_input").hide();
					}
				}
</script>
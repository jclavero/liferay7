<%@ include file="init.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<div class="content_login">
	<div class="main_login_form congrats">
		<div class="text-center flex-column-center">
			<img src="images/iconos/check-listo.svg" alt="" class="m-bot-20">
			<p class="text-medium">La clave ha sido cambiada existosamente</p>
		</div>		
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		window.setInterval("redireccion()",3000);
	});

	function redireccion(){
		window.location.replace("/c/portal/logout");
	}
</script>

<style>
.congrats{
    display: block;
    margin: 0 auto;
    background: white;
    width: 550px;
    transform: translateY(17%);
    top: 50%;
    }
</style>
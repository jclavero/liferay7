package cl.cch.recuperar.password.models;

public class RecuperarPassPreferences {

	private String email;
	private String passwordUno;
	private String passwordDos;
	
	public RecuperarPassPreferences() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswordUno() {
		return passwordUno;
	}
	public void setPasswordUno(String passwordUno) {
		this.passwordUno = passwordUno;
	}
	public String getPasswordDos() {
		return passwordDos;
	}
	public void setPasswordDos(String passwordDos) {
		this.passwordDos = passwordDos;
	}

	@Override
	public String toString() {
		return "RecuperarPassPreferences [email=" + email + ", passwordUno=" + passwordUno + ", passwordDos="
				+ passwordDos + "]";
	}

	
	
	
	
}

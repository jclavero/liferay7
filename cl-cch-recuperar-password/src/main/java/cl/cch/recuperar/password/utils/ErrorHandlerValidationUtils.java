package cl.cch.recuperar.password.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ErrorHandlerValidationUtils {

	public  String rutFormat(String rut) {
		int cont = 0;
		String format;
		rut = rut.replace(".", "");
		rut = rut.replace("-", "");
		format = "-" + rut.substring(rut.length() - 1);
		for (int i = rut.length() - 2; i >= 0; i--) {
			format = rut.substring(i, i + 1) + format;
			cont++;
			if (cont == 3 && i != 0) {
				format = "" + format;
				cont = 0;
			}
		}
		return format;
	}

	public  boolean rutValidate(String rut) {
		rut=rutFormat(rut);
		boolean validacion = false;
		try {
			rut = rut.toUpperCase();
			rut = rut.replace(".", "");
			rut = rut.replace("-", "");
			int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
			char dv = rut.charAt(rut.length() - 1);
			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
			}
		} catch (Exception e) {
		}
		return validacion;
	}

	public  boolean emailFormat(String email) {

		// Patrón para validar el email
		boolean flag;
		Pattern pattern = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		Matcher mather = pattern.matcher(email);

		if (mather.find() == true) {
			flag = true;
			System.out.println("El email ingresado es valido.");
		} else {
			flag = false;
			System.out.println("El email ingresado es invalido.");
		}

		return flag;

	}

	public  boolean passwordValidate(String pass) {
		Pattern pattern = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z]).{8,40})");
		Matcher mather = pattern.matcher(pass);
		if (mather.find() == true) {
			System.out.println("password es válido.");
			return true;
		} else {
			System.out.println("password es inválido.");
			return false;
		}
	}

	public boolean isString(String input) {
		for (int i = 0; i < input.length(); i++) {
			char caracter = input.toUpperCase().charAt(i);
			int valorASCII = (int) caracter;
			if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
				return false;
		}
		return true;
	}

	@SuppressWarnings("unused")
	public  boolean isNumeric(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
}

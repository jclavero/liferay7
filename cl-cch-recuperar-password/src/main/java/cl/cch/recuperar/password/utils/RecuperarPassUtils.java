package cl.cch.recuperar.password.utils;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;

import cl.cch.recuperar.password.constants.ClCchRecuperarPasswordPortletKeys;
import cl.cch.recuperar.password.models.RecuperarPassPreferences;

public class RecuperarPassUtils {

	private static final Log _log = LogFactoryUtil.getLog(RecuperarPassUtils.class);
		
	public void savePreferences(ActionRequest req, ActionResponse res) throws IOException, PortletException {		
		PortletPreferences portletPrefs = req.getPreferences();
	}

	private void savePrefs(PortletPreferences portletPrefs, String variable, String constante) throws IOException, PortletException{		
		if(Validator.isBlank(variable)){
			portletPrefs.setValue(constante, StringPool.BLANK);
			portletPrefs.store();
			}else{
			portletPrefs.setValue(constante, variable);
			portletPrefs.store();
		}		
	}
	
	public static RecuperarPassPreferences loadPreferences(PortletRequest portletRequest){
		PortletPreferences portletPrefs = portletRequest.getPreferences();
		RecuperarPassPreferences regPrefs = new RecuperarPassPreferences();

		
		return regPrefs;
	}
	
	public static String getKeywordURL(HttpServletRequest httpReq) {

		String keyword = ParamUtil.getString(httpReq, ClCchRecuperarPasswordPortletKeys.TXT_KEYWORD, StringPool.BLANK);
		
		return keyword;
	}
	
	public static void enviarEmail(ResourceRequest resourceRequest,
			String email,String correos,String sucursal,String nuevoEnvio) {
		String contenidoFinal = StringPool.BLANK;

			DDMTemplate _templateRecuperarPass = getTemplatesId("Plantilla-exito-cambio-pass", resourceRequest);
			String _plantillaFormularioContacto = _templateRecuperarPass.getScript();

			_plantillaFormularioContacto = _plantillaFormularioContacto
					.replace("${correos}", correos)
					.replace("${sucursal}", sucursal)
					.replace("${nuevoEnvio}", nuevoEnvio)
					.replace("${correoElectronico}", email);
			contenidoFinal = contenidoFinal.concat(_plantillaFormularioContacto);
			

		try {

			String emailBody = contenidoFinal;
			String emailSubject = "Sucursal Virtual - Recuperación de Contraseña";

			InternetAddress from = new InternetAddress("noresponder@correos.cl", "Correos Chile");

			MailMessage message = new MailMessage();
			message.setFrom(from);
			message.setTo(new InternetAddress(email));

			message.setSubject(emailSubject);
			message.setBody(emailBody);
			message.setHTMLFormat(true);
			MailServiceUtil.sendEmail(message);
			_log.info("Email enviado correctamente a = " + email);
		} catch (Exception e) {
			_log.error(e.getMessage(), e);
		}
	}

	public static DDMTemplate getTemplatesId(String template, PortletRequest resourceRequest) {

		ThemeDisplay td = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<DDMTemplate> lista = null;


		DynamicQuery dq = DDMTemplateLocalServiceUtil.dynamicQuery();
		dq.add(RestrictionsFactoryUtil.eq("groupId", td.getLayout().getGroupId()));
		dq.addOrder(OrderFactoryUtil.asc("name"));
		String templateName = "%" + template + "%";
		Criterion criterion = RestrictionsFactoryUtil.like("name", templateName);
		dq.add(criterion);

		try {
			lista = DDMTemplateLocalServiceUtil.dynamicQuery(dq);

			for (DDMTemplate plantilla : lista) {
				if (template.equalsIgnoreCase(plantilla.getName(resourceRequest.getLocale()))) {
					return plantilla;
				}
			}

		} catch (Exception e) {
			_log.error("NO Existe plantilla con nombre = " + template);
			_log.error("error al obtener la plantilla");
			_log.error(e.getMessage());
			return null;
		}

		_log.info("ALGO OCURRIO Y RETORNA PLANTILLA NULL");
		return null;

	}
	
	
}

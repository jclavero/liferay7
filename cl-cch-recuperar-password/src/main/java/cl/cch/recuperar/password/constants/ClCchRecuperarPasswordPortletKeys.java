package cl.cch.recuperar.password.constants;

/**
 * @author VASS
 */
public class ClCchRecuperarPasswordPortletKeys {

	public static final String CLCCHRECUPERARPASSWORD =
		"cl_cch_recuperar_password_ClCchRecuperarPasswordPortlet";

	public static final String PASS_UNO = "passwordUno";
	public static final String PASS_DOS = "passwordDos";
	public static final String CODE_RESET_PASS = "code";
	public static final String RUT = "rut";
	public static final String RETORNO_JSON = "retorno";
	public static final String CHANGED_PASSWORD_ACTION = "actualizarUsuario";
	public static final String JSP_EXITO = "exito.jsp";
	public static final String MVC_PATH_BASE = "/";
	public static final String TXT_RESULT = "result";
	public static final String RESPONSE_PORLET = "responsePorlet";
	public static final String PORTLET_PREFERENCIAS = "regPrefs";
	
	public static final String SUCCES = "succes";

	public static final String FAIL = "fail";
	
	public static final String STATUS = "status";

	public static final String TXT_KEYWORD = "ticketKey";
	
	public static final String SUCURSAL = "https://webserver-correosdechile-dev.lfr.cloud/web/guest/home";
	
	public static final String NUEVO_ENVIO = "https://webserver-correosdechile-dev.lfr.cloud/group/guest/realizar-un-envio";
	
	public static final String CORREOS = "https://webserver-correosdechile-dev.lfr.cloud/group/guest/home";

	public static final String URL_LOGIN = "https://login.correos.cl/openam/XUI/?realm=/sucursaltest&goto=https://website-dev.correos.cl/group/guest/mi-correos#login/";

}
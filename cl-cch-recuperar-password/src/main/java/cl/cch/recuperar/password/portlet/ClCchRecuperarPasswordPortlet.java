package cl.cch.recuperar.password.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.TicketLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import cl.cch.recuperar.password.constants.ClCchRecuperarPasswordPortletKeys;
import cl.cch.recuperar.password.models.RecuperarPassPreferences;
import cl.cch.recuperar.password.utils.ErrorHandlerValidationUtils;
import cl.cch.recuperar.password.utils.RecuperarPassUtils;
import cl.correos.portal.security.sso.openam.service.OpenAMService;

/**
 * @author VASS
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=recuperar password",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ClCchRecuperarPassword", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ClCchRecuperarPasswordPortletKeys.CLCCHRECUPERARPASSWORD,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class ClCchRecuperarPasswordPortlet extends MVCPortlet {

	@Reference
    private OpenAMService _openAMservice;
	
	private static final Log _log = LogFactoryUtil.getLog(ClCchRecuperarPasswordPortlet.class);

	@Override
	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		RecuperarPassPreferences regPrefs = RecuperarPassUtils.loadPreferences(renderRequest);
		renderRequest.setAttribute(ClCchRecuperarPasswordPortletKeys.PORTLET_PREFERENCIAS, regPrefs);
		super.doEdit(renderRequest, renderResponse);
	}

	public void savePrefs(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException, Exception {
		RecuperarPassUtils prefs = new RecuperarPassUtils();
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		actionRequest.setAttribute("themeDisplay", themeDisplay);
		prefs.savePreferences(actionRequest, actionResponse);
	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		RecuperarPassPreferences regPrefs = RecuperarPassUtils.loadPreferences(renderRequest);

		HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		String code = RecuperarPassUtils.getKeywordURL(httpReq);
		Ticket ticket = null;
		User user = null;
		try {
			ticket = TicketLocalServiceUtil.getTicket(code);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			user = UserLocalServiceUtil.getUser(ticket.getClassPK());
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String nombre = user.getFirstName();
		renderRequest.setAttribute("nombre", nombre);
		renderRequest.setAttribute("urlLogin", ClCchRecuperarPasswordPortletKeys.URL_LOGIN);
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {

		String resourceID = resourceRequest.getResourceID();
		_log.info("********** " + resourceID + " **********");
		ErrorHandlerValidationUtils fieldValidation = new ErrorHandlerValidationUtils();
		JSONArray result = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletRequestDispatcher dispatcher = null;

		try {
			if (ClCchRecuperarPasswordPortletKeys.CHANGED_PASSWORD_ACTION.equalsIgnoreCase(resourceID)) {
				String passwordUno = ParamUtil.getString(resourceRequest, ClCchRecuperarPasswordPortletKeys.PASS_UNO);
				String passwordDos = ParamUtil.getString(resourceRequest, ClCchRecuperarPasswordPortletKeys.PASS_DOS);
				String code = ParamUtil.getString(resourceRequest, ClCchRecuperarPasswordPortletKeys.CODE_RESET_PASS);
				PrincipalThreadLocal.getPassword();
				_log.info("passwordUno: "+passwordUno);
				_log.info("passwordDos: "+passwordDos);
				_log.info("code: "+code);
					
				// validar campos vacios
				if (!passwordUno.isEmpty() && !passwordDos.isEmpty()) {

					// confirmar que claves son iguales
					if (passwordUno.equals(passwordDos)) {

						// validar campos
						if (fieldValidation.passwordValidate(passwordUno)
								&& fieldValidation.passwordValidate(passwordDos)) {

							_log.info("Claves ingresadas son iguales...");

							Ticket ticket = TicketLocalServiceUtil.getTicket(code);
							User user = UserLocalServiceUtil.getUser(ticket.getClassPK());

							_openAMservice.changePassword(user.getEmailAddress(), passwordUno, themeDisplay.getCompanyId());

							TicketLocalServiceUtil.deleteTicket(ticket);
							_log.info("La password fue actualizada.");
							
							try {
							RecuperarPassUtils.enviarEmail(resourceRequest, user.getEmailAddress(), ClCchRecuperarPasswordPortletKeys.CORREOS,ClCchRecuperarPasswordPortletKeys.SUCURSAL,ClCchRecuperarPasswordPortletKeys.NUEVO_ENVIO);
							}catch(Exception ex) {
								_log.error("Error enviando el correo");
								_log.error(ex.getMessage()!=null?ex.getMessage():ex.getClass().getName());
							}
							jsonObject.put(ClCchRecuperarPasswordPortletKeys.STATUS,
									ClCchRecuperarPasswordPortletKeys.SUCCES);
							result.put(jsonObject);

						} else {
							throw new Exception("Clave demasiado simple");
						}

					} else {
						throw new Exception("Claves ingresadas son distintas...");
					}
				} else {
					throw new Exception("Clave vacia");
				}

			}
		} catch (Exception e) {

			_log.error(e.getMessage().toString());
			jsonObject.put(ClCchRecuperarPasswordPortletKeys.STATUS, ClCchRecuperarPasswordPortletKeys.FAIL);
			jsonObject.put("error", e.getMessage());
			result.put(jsonObject);
		}
		resourceRequest.setAttribute(ClCchRecuperarPasswordPortletKeys.TXT_RESULT, result);
		resourceResponse.setContentType(ContentTypes.APPLICATION_JSON);
		resourceResponse.flushBuffer();
		resourceResponse.getWriter().print(result);
	}

	public PortletRequestDispatcher getDispatcher(String resourceID) {
		PortletRequestDispatcher dispatcher = null;
		String pathBase = ClCchRecuperarPasswordPortletKeys.MVC_PATH_BASE;

		if (ClCchRecuperarPasswordPortletKeys.JSP_EXITO.equalsIgnoreCase(resourceID)) {
			_log.info("exito.jsp");
			dispatcher = getPortletContext()
					.getRequestDispatcher(pathBase + ClCchRecuperarPasswordPortletKeys.JSP_EXITO);
		}

		return dispatcher;
	}

}
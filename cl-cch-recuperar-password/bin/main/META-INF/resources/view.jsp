<%@ include file="init.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<portlet:resourceURL var="actualizarUsuario" id="actualizarUsuario"></portlet:resourceURL>

<section class="container-small">

	<div class="alert alert-corp-dismissible bg-red-1 text-center" role="alert" id="<portlet:namespace/>alertaContrasena" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong >Las contrase&ntilde;as deben ser iguales.</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
			<i class="la la-times c-white fs-7"	onclick="ocultarAlertaContrasena();">&nbsp;</i>
		</button>
	</div>
	<div class="alert alert-corp-dismissible bg-red-1 text-center" role="alert" id="<portlet:namespace/>alertaContrasenaIgual" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong >La contrase&ntilde;a no debe ser igual a la antigua.</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
			<i class="la la-times c-white fs-7"	onclick="ocultarAlertaContrasenaIgual();">&nbsp;</i>
		</button>
	</div>
	<div class="alert alert-corp-dismissible bg-red-1 text-center" role="alert" id="<portlet:namespace/>alertaFormError" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong class="ajax-error">Tienes un error en el formulario.</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
			<i class="la la-times c-white fs-7"	onclick="ocultarAlertaFormError();">&nbsp;</i>
		</button>
	</div>

	<div class="alert alert-corp-dismissible bg-green-1 text-center" role="alert" id="<portlet:namespace/>alertaContrasenaExito" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong>Contrase&ntilde;a modificada con exito</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
		<i class="la la-times c-white fs-7"	onclick="ocultarAlertaContrasenaExito();">&nbsp;</i>
		</button>
	</div>

	<div class="max-w-95 mx-auto">
	<h1 class="second-title text-center mb-2 c-grey-1">Hola <span class="text-capitalize">${nombre}</span>, estas a punto de cambiar tu contrase&ntilde;a</h1>
	<p class="basic-text c-grey-1 text-center mb-6">Escribe tu nueva contrase&ntilde;a y confirmala para poder activarla</p>
</div>

	<form id="<portlet:namespace/>formFormulario" method="post">

	<div class="bg-white px-5 py-8 rounded-common shadow-center min-h-desk-15 max-w-95 mx-auto">
	<div class="max-w-60 mx-auto">
		<p class="small-text mt-3 mb-1 c-grey-1">Elige una contrase&ntilde;a debe contener letras y n&uacute;meros *
			<a tabindex="0" class="cursor-pointer example-popover2 d-inline-block clean-link c-blue-4-60p" role="button" data-placement="top" data-trigger="focus" title="Contrase&ntilde;a" data-content="Debe contener como m&iacute;nimo 6 caracteres, 1 letra may&uacute;scula, 1 letra min&uacute;scula, 1 numero">
				<i class="icon-exclamation-sign fs-4 d-inline-block"></i>
			</a>				
		</p>
		<div class="container">
			<div class="row">
						
					<input class="form-field" id="<portlet:namespace/>code" name="<portlet:namespace/>code" type="hidden" />
				
				<div class="col-sm-6">
					<div class="input-group-pw form-group input-group mb-3">
						<label class="form-label" for="<portlet:namespace/>passwordUno">Escriba una contrase&ntilde;a</label>
						<input type="password" class="form-control form-field" aria-label="Contrasena" aria-describedby="button-addon2" id="<portlet:namespace/>passwordUno" name="<portlet:namespace/>passwordUno" onchange="validatePass(this.value);">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="button-addon2" data-show-pw2=""><i class="la la-eye fs-10"></i></button>
						</div>
					</div>
				</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-label" for="<portlet:namespace/>passwordDos">Confirma tu contrase&ntilde;a</label>
								 <input class="form-control form-field"	id="<portlet:namespace/>passwordDos" name="<portlet:namespace/>passwordDos" required=""	type="password" onchange="validatePassIguales(this.value);" />
							</div>
						</div>
					</div>
		</div>		
		<div class="max-w-40 mx-auto">
			<hr class="bb-dashed mt-3 mb-6">
		</div>
		<div class="text-center">
		<a class="btn btn-corp bg-blue-1 c-white w-100 max-w-40"  onclick="enviarFormulario();" >Cambiar contrase&ntilde;a</a>
		</div>
	</div>
</div>

	</form>
</section>
	<script type="text/javascript">

	var formFormulario = $('#<portlet:namespace/>formFormulario');
	var actualizarUsuario = '${actualizarUsuario}';
	var code = $("#<portlet:namespace/>code");
	var passwordUno = $("#<portlet:namespace/>passwordUno");
	var passwordDos = $("#<portlet:namespace/>passwordDos");
	var alertaContrasena = $("#<portlet:namespace/>alertaContrasena");
	var alertaContrasenaExito = $("#<portlet:namespace/>alertaContrasenaExito"); 
	var alertaFormError = $("#<portlet:namespace/>alertaFormError"); 
	var alertaContrasenaIgual = $("#<portlet:namespace/>alertaContrasenaIgual");
	var passValid = false;
	$(document).ready(function() {
		var URLactual = window.location.search;
		    var array = URLactual.split("="); 
		    code.val(array[1]);
	
	});
	
	Liferay.on('allPortletsReady', function() {
		showPass("#button-addon2", "#<portlet:namespace/>passwordUno");
	});

	function validatePass(val){
		var m = val;
		  var expreg = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
		  

		  if(expreg.test(m)){
			  passwordUno.addClass("is-valid");
			  passwordUno.removeClass("is-invalid");
			  passwordUno.val(m);
		  	passValid = true;
		  }else{
			  setTimeout(function(){ 
				  passwordUno.removeClass("is-valid");
				  passwordUno.addClass("is-invalid");
			  }, 100);
			  passValid = false;		 
		  }
	}

	function validatePassIguales(val){
		
		if(passwordUno.val() != val){
			setTimeout(function(){ 
				passwordDos.removeClass("is-valid");
				passwordDos.addClass("is-invalid");
			}, 100);
			alertaContrasena.show();
			alertaContrasenaIgual.hide();
			 passValid = false;
		}else{
			alertaContrasena.hide();
			passwordDos.val(val);
			passValid = true;
		}
	}
	
	function ocultarAlertaContrasena() {
		alertaContrasena.hide();
	}
	
	function ocultarAlertaFormError() {
		alertaFormError.hide();
	}
	
	function ocultarAlertaContrasenaExito(){
		alertaContrasenaExito.hide();
	}
	
	function ocultarAlertaContrasenaIgual(){
		alertaContrasenaIgual.hide();
	}
	
	
	
	function enviarFormulario() {
		if (validarFormulario()) {
			console.log("hago el form");
				formFormulario.submit();
		}else{
			if(passValid){
				alertaFormError.show();
				alertaContrasena.hide();
			}
		}
	}

	function validarFormulario() {

		if (code.val() == "") {
			return false;
		}

		if (passwordUno.val() == "") {
			return false;
		}
		if (passwordDos.val() == "") {
			return false;
		}
		
		if(!passValid){
			return false;
		}

		return true;
	}
	
	formFormulario.submit(function(event) {
		event.preventDefault();
		if (validarFormulario()) {
			$.ajax({
				type : 'post',
				url : actualizarUsuario,
				data : formFormulario.serialize(),
				datatype : 'json',
				cache : false,
				error : function(xhr, status, error) {
					$('html, body').animate({scrollTop:200}, 'slow');
				},
				success : function(data) {
					alertaContrasena.hide();
					alertaFormError.hide();			
					var jsonData = data[0];
					if (jsonData.status != "fail") {
						alert("contraseņa cambiada correctamente")
						alertaContrasenaExito.show();
						 setTimeout("location.href='https://login.correos.cl/openam/XUI/?realm=/sucursaltest&goto=https://website-dev.correos.cl/group/guest/mi-correos#login/'", 2000);
					}else{		
						alertaFormError.show();
						$(".ajax-error").html(data[0].error);
						passwordDos.removeClass("");
					}				
				}
			});
		} else {
			alertaFormulario.show();
			$('html, body').animate({
				scrollTop : 200
			}, 'slow');
		}
	});
	
	
	

	$(function () {
		$('.example-popover2').popover({
			container: 'body',
			template: '<div class="popover popover-corp" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
		})		
	});
	Liferay.on(
		'allPortletsReady',
		function(){
			showPass("[data-show-pw2]","#pw2");
		}
	);
</script>

package cl.cch.modificar.password.constants;

/**
 * @author VASS
 */
public class ClCchModificarPasswordPortletKeys {

	public static final String CLCCHMODIFICARPASSWORD =
		"cl_cch_modificar_password_ClCchModificarPasswordPortlet";
	

	public static final String PASS_UNO = "password";
	public static final String PASS_DOS = "pw_confirm";
	public static final String CURRENT_PASS = "currentPassword";
	public static final String RUT = "rut";
	public static final String RETORNO_JSON = "retorno";
	public static final String CHANGED_PASSWORD_ACTION = "actualizarUsuario";
	public static final String JSP_EXITO = "exito.jsp";
	public static final String MVC_PATH_BASE = "/META-INF/resources/";
	public static final String TXT_RESULT = "result";
	public static final String RESPONSE_PORLET = "responsePorlet";
	public static final String PORTLET_PREFERENCIAS = "regPrefs";
	public static final String SUCCES = "succes";

	public static final String CODE_CURRENT_PASS = "currentPass";

	public static final String FAIL = "fail";
	
	public static final String STATUS = "status";
	
	public static final String SUCURSAL = "https://webserver-correosdechile-dev.lfr.cloud/web/guest/home";
	
	public static final String NUEVO_ENVIO = "https://webserver-correosdechile-dev.lfr.cloud/group/guest/realizar-un-envio";
	
	public static final String CORREOS = "https://webserver-correosdechile-dev.lfr.cloud/group/guest/mi-correos";
	

}
package cl.cch.modificar.password.portlet;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.pwd.PasswordEncryptorUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import cl.cch.modificar.password.constants.ClCchModificarPasswordPortletKeys;
import cl.cch.modificar.password.models.ModificarPassPreferences;
import cl.cch.modificar.password.utils.ErrorHandlerValidationUtils;
import cl.cch.modificar.password.utils.ModificarPassUtils;
import cl.correos.portal.security.sso.openam.service.OpenAMService;


/**
 * @author VASS
 */
@Component(
	immediate = true,
	property = {	
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ClCchModificarPassword",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ClCchModificarPasswordPortletKeys.CLCCHMODIFICARPASSWORD,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ClCchModificarPasswordPortlet extends MVCPortlet {
	
	@Reference
    private OpenAMService _openAMservice;
	
	private static final Log _log = LogFactoryUtil.getLog(ClCchModificarPasswordPortlet.class);

	@Override
	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ModificarPassPreferences regPrefs = ModificarPassUtils.loadPreferences(renderRequest);
		renderRequest.setAttribute(ClCchModificarPasswordPortletKeys.PORTLET_PREFERENCIAS, regPrefs);
		super.doEdit(renderRequest, renderResponse);
	}

	public void savePrefs(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException, Exception {
		ModificarPassUtils prefs = new ModificarPassUtils();
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		actionRequest.setAttribute("themeDisplay", themeDisplay);
		prefs.savePreferences(actionRequest, actionResponse);
	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ModificarPassPreferences regPrefs = ModificarPassUtils.loadPreferences(renderRequest);
		User usuario = null;
		try {
			String idUser = renderRequest.getRemoteUser();
			if(idUser!=null) {
				usuario = UserLocalServiceUtil.getUser(Long.parseLong(idUser));
				renderRequest.setAttribute("nombre", usuario.getFirstName());
			}
			
			 
		} catch (Exception e) {
			_log.error(e.getMessage());
			_log.error(e.getCause());
		}
		
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {

		String resourceID = resourceRequest.getResourceID();
		_log.info("********** " + resourceID + " **********");
		ErrorHandlerValidationUtils fieldValidation = new ErrorHandlerValidationUtils();
		JSONArray result = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletRequestDispatcher dispatcher = null;

		try {
			if (ClCchModificarPasswordPortletKeys.CHANGED_PASSWORD_ACTION.equalsIgnoreCase(resourceID)) {
				String passwordUno = ParamUtil.getString(resourceRequest, ClCchModificarPasswordPortletKeys.PASS_UNO);
				String passwordDos = ParamUtil.getString(resourceRequest, ClCchModificarPasswordPortletKeys.PASS_DOS);
				String currentPassword = ParamUtil.getString(resourceRequest, ClCchModificarPasswordPortletKeys.CURRENT_PASS);

				// validar campos vacios
				if (!passwordUno.isEmpty() && !passwordDos.isEmpty()) {

					if(passwordUno.equals(currentPassword)) {
						
						throw new Exception("Esa clave ya esta en uso, intente otra");
					}
					
					
						// confirmar que claves son iguales
						if (passwordUno.equals(passwordDos)) {


							// validar campos
							if (fieldValidation.passwordValidate(passwordUno)
									&& fieldValidation.passwordValidate(passwordDos)) {

								
							_log.info("Claves ingresadas son iguales...");

							User user = obtenerUsuarioLogeado(resourceRequest);

							user.setPassword(PasswordEncryptorUtil.encrypt(passwordUno));
							user.setPasswordEncrypted(true);
							user.setPasswordModified(false);
							UserLocalServiceUtil.updateUser(user);
							_openAMservice.changePassword(user.getEmailAddress(), passwordUno, themeDisplay.getCompanyId());
							// actualizar clave
							//UserLocalServiceUtil.updatePasswordManually(user.getUserId(), passwordUno, false, false, null);
							
							_log.info("La password fue actualizada.");
							ModificarPassUtils.enviarEmail(resourceRequest, user.getEmailAddress(), ClCchModificarPasswordPortletKeys.CORREOS,ClCchModificarPasswordPortletKeys.SUCURSAL,ClCchModificarPasswordPortletKeys.NUEVO_ENVIO);
							jsonObject.put(ClCchModificarPasswordPortletKeys.STATUS, ClCchModificarPasswordPortletKeys.SUCCES);
							result.put(jsonObject);

						} else {
							throw new Exception("Los parametros son incorrectos");
						}

					} else {
						throw new Exception("Claves ingresadas son distintas...");
						
					}
				} else {
					throw new Exception("Los parametros no deben ir vacios");
				}

			}
		} catch (Exception e) {
			
			_log.error(e.getMessage().toString());
			jsonObject.put(ClCchModificarPasswordPortletKeys.STATUS, ClCchModificarPasswordPortletKeys.FAIL);
			jsonObject.put("error",e.getMessage());
			result.put(jsonObject);
		}
		resourceRequest.setAttribute(ClCchModificarPasswordPortletKeys.TXT_RESULT, result);
		resourceResponse.setContentType(ContentTypes.APPLICATION_JSON);
		resourceResponse.flushBuffer();
		resourceResponse.getWriter().print(result);
	}

	private User obtenerUsuarioLogeado(ResourceRequest resourceRequest) {
		String idUser = resourceRequest.getRemoteUser();
		User usuario = null;
		
		try {
			usuario = UserLocalServiceUtil.getUser(Long.parseLong(idUser));
		} catch (Exception e) {
			_log.error(e.getMessage());
			_log.error(e.getCause());
		}
		
		return usuario;
	}
	
	
	public PortletRequestDispatcher getDispatcher(String resourceID) {
		PortletRequestDispatcher dispatcher = null;
		String pathBase = ClCchModificarPasswordPortletKeys.MVC_PATH_BASE;

		if (ClCchModificarPasswordPortletKeys.JSP_EXITO.equalsIgnoreCase(resourceID)) {
			_log.info("exito.jsp");
			dispatcher = getPortletContext()
					.getRequestDispatcher(pathBase + ClCchModificarPasswordPortletKeys.JSP_EXITO);
		}

		return dispatcher;
	}

	
}
<%@ include file="init.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<portlet:resourceURL var="actualizarUsuario" id="actualizarUsuario"></portlet:resourceURL>

<section class="container-small">
<div class="max-w-95 mx-auto">
	<h1 class="second-title text-center mb-2 c-grey-1"><span class="text-capitalize">${nombre}</span>, estas a punto de cambiar tu contraseņa</h1>
	<p class="basic-text c-grey-1 text-center mb-6">Escribe tu nueva contraseņa y confirmala para poder activarla</p>
</div>

	<form id="<portlet:namespace/>formFormulario" method="post">

	<div class="bg-white px-5 py-8 rounded-common shadow-center min-h-desk-15 max-w-95 mx-auto">
	<div class="max-w-60 mx-auto">
	
	<p class="small-text mt-3 mb-1 c-grey-1">Ingresa tu antigua contraseņa</p>
		<div class="container">
		
		
			<div class="row">
			
				<div class="col-sm-6">
					<div class="input-group-pw form-group input-group mb-3">
						<label class="form-label" for="<portlet:namespace/>currentPassword">Escriba una contrase&ntilde;a</label>
						<input type="password" class="form-control form-field" aria-label="Contrasena" aria-describedby="button-addon1" id="<portlet:namespace/>currentPassword" name="<portlet:namespace/>currentPassword">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="button-addon1" data-show-pw1=""><i class="la la-eye fs-10"></i></button>
						</div>
					</div>
				</div>
	
		<div class="small-text mt-3 c-grey-1">
					Elige una contrase&ntilde;a debe contener letras y n&uacute;meros *
					<a tabindex="0"
						class="cursor-pointer example-popover d-inline-block clean-link c-blue-4-60p"
						role="button" data-placement="top" data-trigger="focus"
						title="Contrase&ntilde;a"
						data-content="Debe contener como m&iacute;nimo 8 caracteres, 1 letra may&uacute;scula, 1 letra min&uacute;scula, 1 numero">
						<i class="icon-exclamation-sign fs-4 d-inline-block"></i>
					</a>
				</div>
		
		
		<div class="col-sm-6">
					<div class="input-group-pw form-group input-group mb-3">
						<label class="form-label" for="<portlet:namespace/>password">Escriba una contrase&ntilde;a</label>
						<input type="password" class="form-control form-field" aria-label="Contrasena" aria-describedby="button-addon2" id="<portlet:namespace/>password" name="<portlet:namespace/>password" onchange="validatePass(this.value);">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="button-addon2" data-show-pw2=""><i class="la la-eye fs-10"></i></button>
						</div>
					</div>
				</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-label" for="<portlet:namespace/>pw_confirm">Confirma
									tu contrase&ntilde;a</label> <input class="form-field"
									id="<portlet:namespace/>pw_confirm"
									name="<portlet:namespace/>pw_confirm" required=""
									type="password" onchange="validatePassIguales(this.value);"/>
							</div>
						</div>
							</div>
					</div>		
		<div class="max-w-40 mx-auto">
			<hr class="bb-dashed mt-3 mb-6">
		</div>
		<div class="text-center">
		<a class="btn btn-corp bg-blue-1 c-white w-100 max-w-40" href="#" onclick="enviarFormulario();" >Cambiar contraseņa</a>
		</div>
	</div>
</div>

	</form>
</section>
	<script type="text/javascript">

	var formFormulario = $('#<portlet:namespace/>formFormulario');
	var actualizarUsuario = '${actualizarUsuario}';
	var currentPassword = $("#<portlet:namespace/>currentPassword");
	var passwordUno = $("#<portlet:namespace/>password");
	var passwordDos = $("#<portlet:namespace/>pw_confirm");
	var alertaFormulario = $('#<portlet:namespace/>alertaFormulario');
	var exitoFormulario = $('#<portlet:namespace/>exitoFormulario'); 

	
	$(document).ready(function() {
		var URLactual = window.location.search;
		console.log(URLactual);
		    var array = URLactual.split("="); 
		    code.val(array[1]);
		   console.log(code);
	});

	function quitarClases() {
		console.log("");
		if (!reclamoEnvio) {
			console.log("entre");
			setTimeout(function() {
				codigoSeguimiento.removeClass("is-valid");
				codigoSeguimiento.removeClass("is-invalid");
			}, 100);
		}
	}

	function ocultarAlertaFormulario() {
		alertaFormulario.hide();
	}
	function ocultarExitoFormulario() {
		exitoFormulario.hide();
	}

	function enviarFormulario() {
		if (validarFormulario()) {
			formFormulario.submit();
		} else {
			alertaFormulario.show();
			$('html, body').animate({
				scrollTop : 200
			}, 'slow');
		}
	}

	function validarFormulario() {

		if (currentPassword == "") {
			console.log("codeinput");
			return false;
		}

		if (passwordUno.val() == "") {
			console.log("invalidPassword");
			return false;
		}
		if (passwordDos.val() == "") {
			console.log("invalidPassword");
			return false;
		}

		return true;
	}
	formFormulario.submit(function(event) {
		event.preventDefault();
		if (validarFormulario()) {
			alertaFormulario.hide();
			$.ajax({
				type : 'post',
				url : actualizarUsuario,
				data : formFormulario.serialize(),
				datatype : 'json',
				cache : false,
				error : function(xhr, status, error) {
					$('html, body').animate({scrollTop:200}, 'slow');
				},
				success : function(data) {
					console.log(data);
					console.log('${error}');
					var jsonData = data[0];

					if (jsonData.status != "fail") {
						alert("contraseņa cambiada correctamente")
						$('html, body').animate({scrollTop:200}, 'slow');
					}else{		
						alert(jsonData.error);
						$('html, body').animate({scrollTop:200}, 'slow');
					}				
				}
			});
		} else {
			alertaFormulario.show();
			$('html, body').animate({
				scrollTop : 200
			}, 'slow');
		}
	});
	function limpiarCampos() {
		currentPassword.val("");
		currentPassword.removeClass("is-valid");
		currentPassword.parent().removeClass("focused");
		passwordUno.val("");
		passwordUno.removeClass("is-valid");
		passwordUno.parent().removeClass("focused");
		passwordDos.val("");
		passwordDos.removeClass("is-valid");
		passwordDos.parent().removeClass("focused");
	}

	$(function () {
		$('.example-popover2').popover({
			container: 'body',
			template: '<div class="popover popover-corp" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
		})		
	});
	
	Liferay.on(
		'allPortletsReady',
		function(){
			showPass("#button-addon2","#<portlet:namespace/>password");
		}
	);
	
	Liferay.on(
			'allPortletsReady',
			function(){
				showPass("#button-addon1","#<portlet:namespace/>currentPassword");
			}
		);
	
	Liferay.on(
			'allPortletsReady',
			function(){
				showPass("[data-show-pw2]","#pw2");
			}
		);
	
	Liferay.on(
			'allPortletsReady',
			function(){
				showPass("[data-show-pw1]","#pw1");
			}
		);
	function validatePass(val){
		var m = val;
		  var expreg = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
		  

		  if(expreg.test(m)){
			  passwordUno.addClass("is-valid");
			  passwordUno.removeClass("is-invalid");
			  passwordUno.val(m);
		  	passValid = true;
		  }else{
			  setTimeout(function(){ 
				  passwordUno.removeClass("is-valid");
				  passwordUno.addClass("is-invalid");
			  }, 100);
			  passValid = false;		 
		  }
	}

	function validatePassIguales(val){
		
		if(passwordUno.val() != val){
			setTimeout(function(){ 
				passwordDos.removeClass("is-valid");
				passwordDos.addClass("is-invalid");
			}, 100);
			alertaContrasena.show();
			alertaContrasenaIgual.hide();
			 passValid = false;
		}else{
			alertaContrasena.hide();
			passwordDos.val(val);
			passValid = true;
		}
	}
</script>

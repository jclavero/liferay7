/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cl.cch.event;

import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.model.ExpandoValue;
import com.liferay.expando.kernel.service.ExpandoValueLocalService;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import cl.cch.mis.direcciones.sb.model.Direccion_personal;
import cl.cch.mis.direcciones.sb.service.Direccion_personalLocalService;

@Component(immediate = true, property = { "key=login.events.post" }, service = LifecycleAction.class)
public class ClCchPostloginLoginPreAction implements LifecycleAction {

	private final Log _log = LogFactoryUtil.getLog(ClCchPostloginLoginPreAction.class);

	@Reference
	private PhoneLocalService _phoneLocalService;
	@Reference
	private ExpandoValueLocalService _expandoValueLocalService;
	@Reference
	private Direccion_personalLocalService _direccionPersonalLocalService;

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {

		_log.debug("login.event.post=" + lifecycleEvent);

		try {

			HttpServletRequest request = lifecycleEvent.getRequest();
			HttpServletResponse response = lifecycleEvent.getResponse();
			User user = PortalUtil.getUser(request);

			List<Phone> phones = _phoneLocalService.getPhones(user.getCompanyId(), Contact.class.getName(),
					user.getContactId());
			ExpandoValue value = _expandoValueLocalService.getValue(user.getCompanyId(), User.class.getName(),
					ExpandoTableConstants.DEFAULT_TABLE_NAME, "Numero_Documento", user.getUserId());

			String rut = value.getData();

			List<Direccion_personal> direccionInicial = _direccionPersonalLocalService
					.getFindByTipo_direccion_andNumero_Documento("Personal", rut);

			Group sitio = GroupLocalServiceUtil.getFriendlyURLGroup(PortalUtil.getDefaultCompanyId(), "/guest");

			String finalEndPoint = (String) sitio.getExpandoBridge().getAttribute("urlPostLoginRedirect", false);
			String endPoint = finalEndPoint;
			boolean flagTelefono = false;
			boolean flagDireccion = false;

			// quitar la negacion de la condicional
			if (phones.isEmpty()) {
				_log.info("[status phone]: vacio");
				String telefono = "?002on0=" + flagTelefono;
				endPoint = endPoint + telefono;
			} else {
				_log.info("[status phone]: tiene");
				flagTelefono = true;
				String telefono = "?002on0=" + flagTelefono;
				endPoint = endPoint + telefono;
			}

			if (direccionInicial.isEmpty()) {
				_log.info("[status direccion]: vacio");
				String direccion = "&002i0n=" + flagDireccion;
				endPoint = endPoint + direccion;
			} else {
				_log.info("[status direccion]: tiene");
				flagDireccion = true;
				String direccion = "&002i0n=" + flagDireccion;
				endPoint = endPoint + direccion;
			}

//			if (flagTelefono && flagDireccion) {
//				endPoint = finalEndPoint;
//			}
			_log.info("[endPoint]: " + endPoint);
			response.sendRedirect(endPoint);

		} catch (PortalException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
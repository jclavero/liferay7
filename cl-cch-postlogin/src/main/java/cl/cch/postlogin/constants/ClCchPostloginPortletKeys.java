package cl.cch.postlogin.constants;

/**
 * @author VASS
 */
public class ClCchPostloginPortletKeys {

	public static final String CLCCHPOSTLOGIN =
		"cl_cch_postlogin_ClCchPostloginPortlet";
	
	public static final String JSP_VIEW = "/view.jsp";

}
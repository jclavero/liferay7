<%@page import="cl.cch.registro.usuario.constants.Constantes"%>
<%@ include file="init.jsp" %>

<%
pageContext.setAttribute("keyCapcha", Constantes.KEY_CAPCHA);
pageContext.setAttribute("urlSwCodPostal", Constantes.URL_WS_COD_POSTAL);
pageContext.setAttribute("urlHome", Constantes.URL_HOME);
%>

<form id="<portlet:namespace/>prefsBuscador" action='<portlet:actionURL name="savePrefs" />' method="post">
	<div>
		<label for="<portlet:namespace/>${keyCapcha}"> key capcha: </label>
		<input type="text" id="<portlet:namespace/>${keyCapcha}" name="<portlet:namespace/>${keyCapcha}" value="${regPrefs.keyCapcha}">
	</div>
	<div>
		<label for="<portlet:namespace/>${urlSwCodPostal}"> Url web service: </label>
		<input type="text" id="<portlet:namespace/>${urlSwCodPostal}" name="<portlet:namespace/>${urlSwCodPostal}" value="${regPrefs.urlSwCodPostal}">
	</div>	
	<div>
		<label for="<portlet:namespace/>${urlHome}"> Url Home Correos: </label>
		<input type="text" id="<portlet:namespace/>${urlHome}" name="<portlet:namespace/>${urlHome}" value="${regPrefs.urlHome}">
	</div>
	<div>
		<input type="submit" value="Guardar">
	</div>
</form>

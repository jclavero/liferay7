<%@ include file="init.jsp"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	<div class="max-w-63 w-100 mx-auto">
	<h1 class="second-title text-center mb-6 c-grey-1">
	Reg&iacute;strate en la Sucursal Virtual <span class="c-red-1">Mi Correos</span> y descubre servicios exclusivos para ti.
	</h1>
</div>
<div class="max-w-63 w-100 mx-auto mb-4">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<ul class="circle-list my-0">
					<li class="ok-green pb-2 c-grey-1">Pre-ingresa env&iacute;os</li>
					<li class="ok-green pb-2 c-grey-1">Paga en l&iacute;nea o en sucursal</li>
					<li class="ok-green pb-2 c-grey-1">Gestiona tus env&iacute;os</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="circle-list my-0">
					<li class="ok-green pb-2 c-grey-1">Guarda tus env&iacute;os favoritos</li>
					<li class="ok-green pb-2 c-grey-1">Gestiona tus importaciones</li>
					<li class="ok-green pb-2 c-grey-1">Guarda tus direcciones y m&aacute;s</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bg-white py-8 px-5 rounded-common shadow-center h-desk-50 max-w-95 mx-auto">
	<div class="max-w-90 w-100 mx-auto mb-6">	
		<div class="wizard">
			<div class="wizard-graph-row wizard-graph-complete">
				<div class="wizard-block wizard-block-advanced"><span class="circle-number">1</span></div>
				<div class="wizard-block wizard-block-advanced"><span class="circle-number">2</span></div>
				<div class="wizard-block wizard-block-start"><span class="circle-number">3</span></div>
			</div>

			<div class="wizard-text-row">
				<div class="wizard-block">Datos<br>
				Personales</div>
				<div class="wizard-block">Terminos y Cond.</div>
				<div class="wizard-block">Env&iacute;o de activaci&oacute;n de <br>cuenta</div>
			</div>
		</div>
	</div>
	<div class="max-w-63 w-100 mx-auto">
		<div class="text-center">
			<div class="w-np-15 h-np-15 full-rounded bg-green-1 d-flex align-items-center justify-content-center mx-auto mb-1">
				<i class="la la-check c-white la la-check fs-20"></i>
			</div>
			<p class="small-text c-grey-2 mb-0">Un &uacute;ltimo paso y completar&aacute;s tu registro</p>
			<p class="c-grey-1 second-title mb-1">Hemos enviado un email de confirmaci&oacute;n</p>
			<p class="small-text c-grey-2"> Revisa tu bandeja de entrada o de Spam para activar tu registro. Tienes 24hrs despu&eacute;s de registrarte para validarlo.</p>
		</div>		
	</div>
	<div class="text-center">
		<button class="btn btn-corp btn-normal max-w-25 w-100 mx-auto" onclick="redirect();">Home</button>
	</div>
	
							
							
</div>
<script>
function redirect(){
	window.location.replace('${urlHome}');
}
</script>
	
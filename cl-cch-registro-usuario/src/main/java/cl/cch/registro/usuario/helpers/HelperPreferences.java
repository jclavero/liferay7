package cl.cch.registro.usuario.helpers;


import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import cl.cch.registro.usuario.constants.Constantes;
import cl.cch.registro.usuario.models.registroUsuarioPreferencias;


public class HelperPreferences {

	public void savePreferences(ActionRequest req, ActionResponse res) throws IOException, PortletException {
		PortletPreferences portletPrefs = req.getPreferences();
		String capcha = ParamUtil.getString(req, Constantes.KEY_CAPCHA);
		savePrefs(portletPrefs, capcha, Constantes.KEY_CAPCHA);
		String urlCodPostal = ParamUtil.getString(req, Constantes.URL_WS_COD_POSTAL);
		savePrefs(portletPrefs, urlCodPostal, Constantes.URL_WS_COD_POSTAL);
		String urlHome = ParamUtil.getString(req, Constantes.URL_HOME);
		savePrefs(portletPrefs, urlHome, Constantes.URL_HOME);
	}

	private void savePrefs(PortletPreferences portletPrefs, String variable, String constante)
			throws IOException, PortletException {
		if (Validator.isBlank(variable)) {
			portletPrefs.setValue(constante, StringPool.BLANK);
			portletPrefs.store();
		} else {
			portletPrefs.setValue(constante, variable);
			portletPrefs.store();
		}
	}

	public static registroUsuarioPreferencias loadPreferences(PortletRequest portletRequest) {
		PortletPreferences portletPrefs = portletRequest.getPreferences();
		registroUsuarioPreferencias prefs = new registroUsuarioPreferencias();
		prefs.setKeyCapcha(portletPrefs.getValue(Constantes.KEY_CAPCHA, StringPool.BLANK));
		prefs.setUrlSwCodPostal(portletPrefs.getValue(Constantes.URL_WS_COD_POSTAL, StringPool.BLANK));
		prefs.setUrlHome(portletPrefs.getValue(Constantes.URL_HOME, StringPool.BLANK));
		return prefs;
	}
	
	
	
	
}

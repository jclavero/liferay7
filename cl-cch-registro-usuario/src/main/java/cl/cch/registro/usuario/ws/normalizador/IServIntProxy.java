package cl.cch.registro.usuario.ws.normalizador;

public class IServIntProxy implements cl.cch.registro.usuario.ws.normalizador.IServInt {
  private String _endpoint = null;
  private cl.cch.registro.usuario.ws.normalizador.IServInt iServInt = null;
  
  public IServIntProxy() {
    _initIServIntProxy();
  }
  
  public IServIntProxy(String endpoint) {
    _endpoint = endpoint;
    _initIServIntProxy();
  }
  
  private void _initIServIntProxy() {
    try {
      iServInt = (new cl.cch.registro.usuario.ws.normalizador.ServIntLocator()).getBasicHttpBinding_IServInt();
      if (iServInt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iServInt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iServInt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iServInt != null)
      ((javax.xml.rpc.Stub)iServInt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.cch.registro.usuario.ws.normalizador.IServInt getIServInt() {
    if (iServInt == null)
      _initIServIntProxy();
    return iServInt;
  }
  
  public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaBasicaVO normalizar(java.lang.String direccion) throws java.rmi.RemoteException{
    if (iServInt == null)
      _initIServIntProxy();
    return iServInt.normalizar(direccion);
  }
  
  public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullVO normalizarFull(java.lang.String direccion) throws java.rmi.RemoteException{
    if (iServInt == null)
      _initIServIntProxy();
    return iServInt.normalizarFull(direccion);
  }
  
  public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullConComunalVO normalizarFullConComunal(java.lang.String direccion) throws java.rmi.RemoteException{
    if (iServInt == null)
      _initIServIntProxy();
    return iServInt.normalizarFullConComunal(direccion);
  }
  
  public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaBasicaVO[] normalizarListMasivo(cl.cch.registro.usuario.ws.normalizador.Address[] direcciones) throws java.rmi.RemoteException{
    if (iServInt == null)
      _initIServIntProxy();
    return iServInt.normalizarListMasivo(direcciones);
  }
  
  
}
package cl.cch.registro.usuario.portlet;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.kernel.Value;
import com.liferay.expando.kernel.exception.NoSuchTableException;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.model.ExpandoValue;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalServiceUtil;
import com.liferay.expando.kernel.service.ExpandoValueLocalService;
import com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ContactLocalServiceUtil;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.TicketLocalServiceUtil;
import com.liferay.portal.kernel.service.UserGroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import cl.cch.codigo.postal.comunal.sb.model.CodigoPostalComunal;
import cl.cch.codigo.postal.comunal.sb.service.CodigoPostalComunalLocalService;
import cl.cch.mis.direcciones.sb.model.Direccion_personal;
import cl.cch.mis.direcciones.sb.model.Lista_Direcciones;
import cl.cch.mis.direcciones.sb.service.Direccion_personalLocalService;
import cl.cch.mis.direcciones.sb.service.Direccion_personalLocalServiceUtil;
import cl.cch.mis.direcciones.sb.service.Lista_DireccionesLocalServiceUtil;
import cl.cch.registro.usuario.constants.ClCchRegistroUsuarioPortletKeys;
import cl.cch.registro.usuario.constants.Constantes;
import cl.cch.registro.usuario.helpers.HelperPreferences;
import cl.cch.registro.usuario.helpers.SoapWsNormalizadorHelpers;
import cl.cch.registro.usuario.models.Comuna;
import cl.cch.registro.usuario.models.Region;
import cl.cch.registro.usuario.models.registroUsuarioPreferencias;
import cl.cch.registro.usuario.utils.ErrorHandlerValidationUtils;
import cl.cch.registro.usuario.utils.RegisterUtils;
import cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullConComunalVO;
import cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullVO;
import cl.correos.portal.security.sso.openam.service.OpenAMService;

/**
 * @author VASS
 */
@Component(immediate = true, property = {
		"com.liferay.portlet.display-category=Portlets Correo",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Registro Usuario",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.portlet-mode=text/html;view,edit",
		"javax.portlet.init-param.edit-template=/edit.jsp",
		"javax.portlet.name=Registro Usuario",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" 
		}, 
		service = Portlet.class)
public class ClCchRegistroUsuarioPortlet extends MVCPortlet {
	@Reference
	private OpenAMService _openAMservice;
	@Reference
	private UserLocalService _userLocalService;
	@Reference
	private ClassNameLocalService _classNameLocalService;
	@Reference
	private ExpandoValueLocalService _expandoValueLocalService;
	@Reference
	private ExpandoColumnLocalService _expandoColumnLocalService;
	@Reference
	private CompanyLocalService _companyLocalService;
	@Reference
	private PhoneLocalService _phoneLocalService;
	@Reference
	private UserGroupLocalService _userGroupLocalService;
	@Reference
	private CounterLocalService _counterLocalService;
	
	@Reference
	private Direccion_personalLocalService _direccionPersonalLocalService;
	
	@Reference
	private CodigoPostalComunalLocalService _codigoPostalComunalLocalService;
	
	
	private final Log _log = LogFactoryUtil.getLog(ClCchRegistroUsuarioPortlet.class);

	@Override
	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		registroUsuarioPreferencias prefs = HelperPreferences.loadPreferences(renderRequest);
		renderRequest.setAttribute(Constantes.PORTLET_PREFERENCIAS, prefs);
		super.doEdit(renderRequest, renderResponse);
	}

	public void savePrefs(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException, Exception {

		HelperPreferences prefs = new HelperPreferences();
		prefs.savePreferences(actionRequest, actionResponse);
	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		registroUsuarioPreferencias prefs = HelperPreferences.loadPreferences(renderRequest);

		JSONArray result = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

		List<Comuna> listaComunas = RegisterUtils.getAllComunasBySucursales();
		Collections.sort(listaComunas);
		jsonObject.put(Constantes.LISTA_COMUNAS, listaComunas);

		List<Region> listaRegiones = RegisterUtils.getAllRegion();
		Collections.sort(listaRegiones);

		jsonObject.put(Constantes.LISTA_REGION, listaRegiones);
		result.put(jsonObject);

		renderRequest.setAttribute(Constantes.TXT_RESULT, result);
		if(Validator.isNotNull(prefs.getKeyCapcha())) {
			renderRequest.setAttribute(Constantes.KEY_CAPCHA, prefs.getKeyCapcha());
		}else {
			renderRequest.setAttribute(Constantes.KEY_CAPCHA, Constantes.KEY_CAPCHA_DEFAULT);
		}
		

		super.doView(renderRequest, renderResponse);
	}

	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {

		registroUsuarioPreferencias prefs = HelperPreferences.loadPreferences(resourceRequest);
		String resourceID = resourceRequest.getResourceID();
		ErrorHandlerValidationUtils fieldValidation = new ErrorHandlerValidationUtils();
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray result = JSONFactoryUtil.createJSONArray();
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		_log.debug("********** " + resourceID + " **********");

		try {

			if (resourceID.equalsIgnoreCase(ClCchRegistroUsuarioPortletKeys.SERVICE_NAME)) {

				long classNameId = _classNameLocalService.getClassNameId(User.class.getName());

				String firstName = ParamUtil.getString(resourceRequest, "firstName").trim();

				String tdoc = ParamUtil.getString(resourceRequest, "tdoc").trim();
				String lastName = ParamUtil.getString(resourceRequest, "lastName").trim();
				String email = ParamUtil.getString(resourceRequest, "email").trim();
				String password = ParamUtil.getString(resourceRequest, "password").trim();
				String pw_confirm = ParamUtil.getString(resourceRequest, "passConfirm").trim();
				String ndoc = StringPool.BLANK;
				String inputTipoDoc = ParamUtil.getString(resourceRequest, "inputTipoDoc");
				String idCodComunal = ParamUtil.getString(resourceRequest, "idCodComunal");
				if(Constantes.RUT.equalsIgnoreCase(inputTipoDoc)) {
					ndoc = ParamUtil.getString(resourceRequest, "ndoc");
					ndoc = ndoc.replace(".", "").trim();
				}else {
					ndoc = ParamUtil.getString(resourceRequest, "pasaporte");
					ndoc = ndoc.replace(".", "").trim();
				}
				
				
				String year = ParamUtil.getString(resourceRequest, "anno").trim();
				String month = ParamUtil.getString(resourceRequest, "mes").trim();
				String day = ParamUtil.getString(resourceRequest, "dia").trim();
				String telephone = ParamUtil.getString(resourceRequest, "telephone").trim();

				// parametros opcionales
				String dir_calle = ParamUtil.getString(resourceRequest, "calle").trim();
				String dir_num = ParamUtil.getString(resourceRequest, "numero").trim();
				String dir_dep = ParamUtil.getString(resourceRequest, "deptoCasa").trim();
				String referencia = ParamUtil.getString(resourceRequest, "referencia").trim();
				String latitud = ParamUtil.getString(resourceRequest, "latitud").trim();
				String longitud = ParamUtil.getString(resourceRequest, "longitud").trim();
				// codigo postal en duro
				String dir_cod_postal = ParamUtil.getString(resourceRequest, "codPostal").trim();
				String comuna = ParamUtil.getString(resourceRequest, "comuna").trim();

				// validar campos vacios
				if (!firstName.isEmpty() && !lastName.isEmpty() && !email.isEmpty() && !password.isEmpty()
						&& !ndoc.isEmpty() && !telephone.isEmpty() && !dir_calle.isEmpty() && !dir_num.isEmpty() &&  !dir_cod_postal.isEmpty() && !comuna.isEmpty()) {

					try {

						CaptchaUtil.isEnabled(resourceRequest);

					} catch (Exception e) {

						if (e instanceof CaptchaTextException || e instanceof CaptchaMaxChallengesException) {

							SessionErrors.add(resourceRequest, e.getClass(), e);

						}

					}

					if (pw_confirm.equals(password)) {
						boolean numeroIdentificador = true;

						// validar campos

						boolean existe = getUserEmail(themeDisplay, email);
						User user = null;
						_log.info("existe" + existe);
						if (!existe) {
							// crear el usuario
							
							if(month.equalsIgnoreCase("0") || year.equalsIgnoreCase("0") || day.equalsIgnoreCase("0")) {
								month = "01";
								year = "1970";
								day ="1";
							}
							
							user = createUser(firstName, lastName, password, ndoc, email, themeDisplay, classNameId,
									telephone, year, month, day, comuna, tdoc, resourceRequest, dir_calle, dir_num,
									dir_dep,  dir_cod_postal, referencia, inputTipoDoc, latitud, longitud,idCodComunal);
							if (user != null) {
								jsonObject.put(ClCchRegistroUsuarioPortletKeys.STATUS,
										ClCchRegistroUsuarioPortletKeys.SUCCES);

								result.put(jsonObject);
								resourceRequest.setAttribute(Constantes.URL_HOME, prefs.getUrlHome());
								PortletRequestDispatcher dispatcher = getDispatcher(Constantes.JSP_EXITO);
								dispatcher.include(resourceRequest, resourceResponse);
							} else {
								_log.error("error al crear usuario");
							}
						} else {
							_log.debug("usuario ya existe");
						}
					} else {
						_log.error("las contraseñas deben coincidir");
					}
				} else {
					_log.error("Los parametros no pueden ser vacios");

				}

			} else if (resourceID.equalsIgnoreCase("obtenerCP")) {
				String calle = ParamUtil.getString(resourceRequest, "calleInput");
				String numero = ParamUtil.getString(resourceRequest, "numInput");
				String comuna = ParamUtil.getString(resourceRequest, "inputComuna");

				String direccion = "1;".concat(calle).concat(StringPool.SPACE).concat(numero).concat(StringPool.SEMICOLON).concat(comuna);
				String url = prefs.getUrlSwCodPostal();
				_log.info("comuna: " + comuna);
				DireccionNormalizadaFullConComunalVO respuestaWS = SoapWsNormalizadorHelpers.getDireccion(url,direccion);

				if (Validator.isNotNull(respuestaWS)) {
					String latitud = respuestaWS.getLatitud();
					String longitud = respuestaWS.getLongitud();
					String codigoPostal = null;
					long idCodComunal = _counterLocalService.increment(CodigoPostalComunal.class.getName());
					CodigoPostalComunal codigoPostalComunal = _codigoPostalComunalLocalService.createCodigoPostalComunal(idCodComunal);
					_codigoPostalComunalLocalService.addCodigoPostalComunal(codigoPostalComunal);
					if (respuestaWS.isAceptado()) {
						codigoPostal = respuestaWS.getCpostal();
						_codigoPostalComunalLocalService.deleteCodigoPostalComunal(idCodComunal);
						jsonObject.put("idCodComunal", 0);

					} else {
						codigoPostal = respuestaWS.getCpostalComunal();

						codigoPostalComunal.setCalle(calle);
						codigoPostalComunal.setCodigo_postal(codigoPostal);
						codigoPostalComunal.setComuna(comuna);
						codigoPostalComunal.setNumero(numero);
						_codigoPostalComunalLocalService.updateCodigoPostalComunal(codigoPostalComunal);
						jsonObject.put("idCodComunal", idCodComunal);
					}

					String calleWs = respuestaWS.getCalle();
					jsonObject.put("latitud", latitud);
					jsonObject.put("calleWs", calleWs);
					jsonObject.put("longitud", longitud);
					jsonObject.put("codigoPostal", codigoPostal);

				} else {
					jsonObject.put("codigoPostal", 0);
				}

				result.put(jsonObject);
				createJson(result, resourceRequest, resourceResponse);

			} else if (resourceID.equalsIgnoreCase("validCorreo")) {
				String correo = ParamUtil.getString(resourceRequest, "correoInput");
				boolean existe = getUserEmail(themeDisplay, correo);
				jsonObject.put("existe", existe);
				result.put(jsonObject);
				createJson(result, resourceRequest, resourceResponse);

			} else if (resourceID.equalsIgnoreCase("actionRegion")) {

				String region = ParamUtil.getString(resourceRequest, "regionInput");
				List<Comuna> listaComunas = RegisterUtils.getAllComunasByRegion(region);

				Collections.sort(listaComunas);
				jsonObject.put(Constantes.LISTA_COMUNAS_REGION, listaComunas);
				result.put(jsonObject);
				createJson(result, resourceRequest, resourceResponse);
			}

		} catch (Exception e) {
			_log.error("unknow :" + e.getMessage());
			jsonObject.put(ClCchRegistroUsuarioPortletKeys.STATUS, ClCchRegistroUsuarioPortletKeys.FAIL);
			jsonObject.put(ClCchRegistroUsuarioPortletKeys.TXT_RESULT, e.getMessage());

			result.put(jsonObject);
		}

	}

	public User createUser(String firstName, String lastName, String password, String rut, String email,
			ThemeDisplay themeDisplay, long classNameId, String telephone, String yearB, String monthB, String dayB,
			String comuna, String tdoc, ResourceRequest resourceRequest, String dir_calle, String dir_num,
			String dir_dep, String dir_cod_postal, String referencia, String inputTipoDoc,
			String latitud, String longitud, String idCodComunal) {
		User userNew = null;
		try {
			Company company = _companyLocalService.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			ServiceContext serviceContext = ServiceContextFactory.getInstance(User.class.getName(), resourceRequest);
			Locale locale = LocaleUtil.getDefault();
			long companyId = company.getCompanyId();

		
			long userGroupIds[] = new long[1];

			List<UserGroup> ug = _userGroupLocalService.getUserGroups(-1, -1);
			for (UserGroup userGroup : ug) {
				userGroupIds[0] = userGroup.getUserGroupId();
			}

			// registrar un usuario
			userNew = _userLocalService.addUser(0, companyId, false, password, password, true, StringPool.BLANK, email,
					0, null, locale, firstName, StringPool.BLANK, lastName, 0, 0, true, 01, 1, 2019,
					StringPool.BLANK, null, null, null, userGroupIds, false, new ServiceContext());
			userNew.setPasswordReset(false);
		
			Date birthday = RegisterUtils.createBirthday(dayB, monthB, yearB);
			
			Contact contact =  ContactLocalServiceUtil.getContact(userNew.getContactId()); 
			contact.setBirthday(birthday);
			ContactLocalServiceUtil.updateContact(contact);
		
			_openAMservice.createUser(email, password, firstName, lastName, rut, themeDisplay.getCompanyId());

				// agragar rut
				addRut(userNew, themeDisplay, classNameId, rut, tdoc);
				// agregar telefono
				addTelephone(userNew, telephone, themeDisplay);
				// agregar direccion
				addAddress(userNew, rut, dir_calle, dir_num, dir_dep, dir_cod_postal, comuna, referencia,
						telephone, latitud, longitud, email, idCodComunal);

				// eliminar pregunta secreta y primer ingreso
				userNew.setPasswordReset(false);
				userNew.setReminderQueryQuestion("@new@");
				userNew.setReminderQueryAnswer("@new@");
				userNew.setAgreedToTermsOfUse(true);
				userNew.setEmailAddressVerified(true);
				userNew.setStatus(WorkflowConstants.STATUS_INACTIVE);
				userNew = _userLocalService.updateUser(userNew);

				long classPK = userNew.getPrimaryKey();
				String[] tipo = new String[] {};
				
				if (inputTipoDoc.equalsIgnoreCase(Constantes.RUT)) {
					 tipo = new String[] {Constantes.RUT};
				} else {
					tipo = new String[] {Constantes.PASAPORTE};
				}
				_expandoValueLocalService.addValue(companyId, User.class.getName(),
						ExpandoTableConstants.DEFAULT_TABLE_NAME, Constantes.TIPO_DOCUMENTO, classPK, tipo);
				
				Date date = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.HOUR, 24);
				Date expirationDate = cal.getTime();
				_log.debug("EXPIRACION :" + expirationDate);
				Ticket ticket = TicketLocalServiceUtil.addTicket(userNew.getCompanyId(), User.class.getName(),userNew.getUserId(), TicketConstants.TYPE_PASSWORD, null, expirationDate, serviceContext);

				String registrationURL = serviceContext.getPortalURL() + Constantes.LINK_BASE+"?ticketKey="+ticket.getKey();
				

				_log.info("LINK VALIDACION :" + registrationURL);

				RegisterUtils.enviarEmail(resourceRequest, email, registrationURL);

				_log.debug("NUEVO USUARIO INGRESADO ID = " + userNew.getUserId());
				_log.debug("NOMBRE USUARIO = " + userNew.getFullName());
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			PrintWriter pw = new PrintWriter(writer);
			e.printStackTrace(pw);
			_log.error(e.getMessage());
			_log.error(writer.toString());
		}
		return userNew;
	}

	public Phone addTelephone(User user, String telephone, ThemeDisplay themeDisplay) throws PortalException {
		List<Phone> phones = _phoneLocalService.getPhones(user.getCompanyId(), Contact.class.getName(),
				user.getContactId());
		_log.error("se agrega phones");
		Phone fono = _phoneLocalService.createPhone(_counterLocalService.increment(Phone.class.getName()));
		try {

			fono.setPhoneId(user.getContactId());
			fono.setPrimaryKey(user.getContactId());
			fono.setUserId(user.getUserId());
			fono.setClassPK(user.getContactId());
			fono.setPrimary(true);
			fono.setClassNameId(_classNameLocalService.getClassNameId(Contact.class.getName()));
			fono.setNumber("+56"+telephone);
			fono.setTypeId(11006);
			_phoneLocalService.addPhone(fono);

		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		_log.info("se agrega phones");
		return fono;

	}

	public void addAddress(User user, String rut, String dir_calle, String dir_num, String dir_dep,
			String dir_cod_postal, String comuna, String referencia, String telefono, String latitud, String longitud, String emailPersonal,String idCodComunal)
			throws PortalException {
		
		List<Direccion_personal> direcciones = _direccionPersonalLocalService.getFindByNumero_Documento(rut);
		Long idCod = Long.parseLong(idCodComunal);
			
		 
		boolean existeDir = false;
		if(direcciones.size() > 0) {
			for (Direccion_personal direccion_personal : direcciones) {
				 if(direccion_personal.getNombre_direccion().equalsIgnoreCase(Constantes.PERSONAL)) {
					 existeDir = true;
					 break;
				 }
			}
		}else {
			existeDir = false;
		}
		 
		 if(existeDir) {
			 _log.info("usaurio ya tiene la lista creada");
		 }else {
			 long idDireccion = _counterLocalService.increment(Direccion_personal.class.getName());
			 Direccion_personal direccion = _direccionPersonalLocalService.createDireccion_personal(idDireccion);
				direccion.setComuna(comuna);
				direccion.setCalle(dir_calle);
				direccion.setNumero(dir_num);
				direccion.setDepto(dir_dep);
				direccion.setRegion(RegisterUtils.getRegionByComuna(comuna));
				direccion.setCodigo_Postal(dir_cod_postal);
				direccion.setReferencia(referencia);
				direccion.setNumero_documento(rut);
				direccion.setTipo_direccion(Constantes.PERSONAL);
				direccion.setNombre_direccion(Constantes.PERSONAL);
				direccion.setLatitud(latitud);
				direccion.setEmailPersonal(emailPersonal);
				direccion.setLongitud(longitud);
				direccion.setTelefono(telefono);
				

				if(!idCodComunal.equalsIgnoreCase("0")) {
					 CodigoPostalComunal codigoPostalComunal = _codigoPostalComunalLocalService.getCodigoPostalComunal(idCod);
					 
					 codigoPostalComunal.setId_direccion(String.valueOf(idDireccion));
					 codigoPostalComunal.setNumero_documento(rut);
					 codigoPostalComunal.setTipo_direccion(Constantes.PERSONAL);
					 codigoPostalComunal.setTipo_codigo("codigo_comunal");
					 _codigoPostalComunalLocalService.updateCodigoPostalComunal(codigoPostalComunal);
					 direccion.setTipoCodigo("codigo_postal");
				}else {
					 direccion.setTipoCodigo("codigo_comunal");
				}
				
				
				
				 _direccionPersonalLocalService.addDireccion_personal(direccion);
				
				_log.info("se agrega lista personal al usuario");
		 }
		 
		
		 List<Lista_Direcciones> direccionesDestinatario = Lista_DireccionesLocalServiceUtil.getFindByNumero_Documento_and_Nombre_Lista(rut, "Mis Destinatarios");
		 
		 if(direccionesDestinatario.size() > 0) {
			 _log.info("usaurio ya tiene la lista creada");
		 }else {
			 long IdListaDes = _counterLocalService.increment(Lista_Direcciones.class.getName());
				Lista_Direcciones listaDir = Lista_DireccionesLocalServiceUtil.createLista_Direcciones(IdListaDes);
				listaDir.setId_Lista(IdListaDes);
				listaDir.setNumero_documento(rut);
				listaDir.setTipo_direccion("Mis Destinatarios");
//				listaDir.setEmailPersonal(emailPersonal);
				_log.info("se agrega direccion");
				Lista_DireccionesLocalServiceUtil.addLista_Direcciones(listaDir);
		 }
		
		 _log.info("se agrego listas de direcciones");
	}

	public void addRut(User user, ThemeDisplay themeDisplay, long classNameId, String rut, String tdoc)
			throws PortalException {
		// agregamos columna rut
		try {

			ExpandoTable expandoTable = checkTable(themeDisplay.getCompanyId());
			_log.debug("expandoTable" + expandoTable.getTableId());

			ExpandoColumn expandoColumnNdoc = _expandoColumnLocalService.getColumn(themeDisplay.getCompanyId(),
					classNameId, expandoTable.getName(), "Numero_Documento");

			ExpandoValue valueResponse = _expandoValueLocalService.getValue(user.getCompanyId(),
					Contact.class.getName(), expandoTable.getName(), expandoColumnNdoc.getName(), user.getContactId());

			if (valueResponse != null) {
				_expandoValueLocalService.updateExpandoValue(valueResponse);
			} else {

				valueResponse = _expandoValueLocalService.addValue(classNameId, expandoTable.getTableId(),
						expandoColumnNdoc.getColumnId(), user.getUserId(), rut);
			}

			ExpandoColumn expandoColumnTdoc = _expandoColumnLocalService.getColumn(themeDisplay.getCompanyId(),
					classNameId, expandoTable.getName(), "tipo_documento");

			ExpandoValue valueResponseTdoc = _expandoValueLocalService.getValue(user.getCompanyId(),
					Contact.class.getName(), expandoTable.getName(), expandoColumnTdoc.getName(), user.getContactId());

			if (valueResponseTdoc != null) {
				_expandoValueLocalService.updateExpandoValue(valueResponseTdoc);
			} else {
				valueResponseTdoc = _expandoValueLocalService.addValue(classNameId, expandoTable.getTableId(),
						expandoColumnTdoc.getColumnId(), user.getUserId(), tdoc);
			}

		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		_log.info("Rut Agregado");
	}

	private ExpandoTable checkTable(long companyId) throws SystemException, PortalException {

		ExpandoTable expandoTable = null;
		long classNameId = _classNameLocalService.getClassNameId(User.class.getName());
		try {
			expandoTable = ExpandoTableLocalServiceUtil.getTable(companyId, classNameId, "CUSTOM_FIELDS");

		} catch (NoSuchTableException nste) {
			// creat tabla
			expandoTable = addTable(companyId, "CUSTOM_FIELDS", classNameId);
			// crear columna numero de documento
			ExpandoColumn columnNdoc = _expandoColumnLocalService.addColumn(expandoTable.getTableId(),
					"Numero_Documento", ExpandoColumnConstants.STRING);
			columnNdoc.setTypeSettings(
					"display-type=input-field" + " hidden=false" + " index-type=2" + " localize-field=false"
							+ " secret=true" + " visible-with-update-permission=true" + " width=0" + " ");
			_expandoColumnLocalService.updateExpandoColumn(columnNdoc);

			// crear columna tipo de documento
			ExpandoColumn columnTdoc = _expandoColumnLocalService.addColumn(expandoTable.getTableId(),
					"tipo_documento", ExpandoColumnConstants.STRING);
			columnTdoc.setTypeSettings(
					"display-type=input-field" + " hidden=false" + " index-type=2" + " localize-field=false"
							+ " secret=true" + " visible-with-update-permission=true" + " width=0" + " ");
			_expandoColumnLocalService.updateExpandoColumn(columnTdoc);

		}
		return expandoTable;

	}

	public ExpandoTable addTable(long companyId, String tableName, long classNameId)
			throws PortalException, SystemException {
		ExpandoTable expandoTable = ExpandoTableLocalServiceUtil.addTable(companyId, classNameId, tableName);
		_log.error("Expando Table Created Successfully.");
		return expandoTable;
	}

	private PortletRequestDispatcher getDispatcher(String valor) {
		PortletRequestDispatcher dispatcher = null;

		if (Constantes.JSP_EXITO.equalsIgnoreCase(valor)) {
			dispatcher = getPortletContext().getRequestDispatcher(Constantes.PATH_BASE + valor);
		}
		if (Constantes.JSP_VIEW.equalsIgnoreCase(valor)) {
			dispatcher = getPortletContext().getRequestDispatcher(Constantes.PATH_BASE + valor);
		}

		return dispatcher;
	}

	public boolean getUserEmail(ThemeDisplay td, String email) {
		boolean existe = false;
		User user = null;
		Long companyId = td.getCompanyId();
		try {
			user = _userLocalService.getUserByEmailAddress(companyId, email);
			if (user != null) {
				existe = true;
			}
		} catch (PortalException e) {
			_log.debug("Usuario no existe");
		}

		return existe;
	}

	public User getUserNdoc(ThemeDisplay themeDisplay, String ndoc) throws PortalException {

		long classNameId = _classNameLocalService.getClassNameId(User.class.getName());

		ExpandoTable expandoTable = ExpandoTableLocalServiceUtil.getTable(themeDisplay.getCompanyId(), classNameId,
				"CUSTOM_FIELDS");

		_log.debug("expandoTable" + expandoTable.getTableId());

		ExpandoColumn expandoColumn = _expandoColumnLocalService.getColumn(themeDisplay.getCompanyId(), classNameId,
				expandoTable.getName(), "Numero_Documento");

		List<ExpandoValue> values = _expandoValueLocalService.getColumnValues(expandoColumn.getColumnId(), -1, -1);

		User user = null;
		for (ExpandoValue value : values) {
			if (value.getData().equals(ndoc)) {
				long userId = value.getClassPK();
				user = _userLocalService.fetchUser(userId);
			}
		}
		return user;
	}

	private void createJson(JSONArray result, ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException {

		resourceResponse.setContentType(ContentTypes.APPLICATION_JSON);
		resourceResponse.flushBuffer();
		resourceResponse.getWriter().print(result);
	}
}
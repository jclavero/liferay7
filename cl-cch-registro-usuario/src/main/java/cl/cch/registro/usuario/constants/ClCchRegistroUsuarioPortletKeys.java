package cl.cch.registro.usuario.constants;

/**
 * @author VASS
 */
public class ClCchRegistroUsuarioPortletKeys {

	public static final String CLCCHREGISTROUSUARIO = "cl_cch_registro_usuario_ClCchRegistroUsuarioPortlet";

	public static final String SERVICE_NAME = "datosUsuario";

	public static final String SUCCES = "succes";

	public static final String FAIL = "fail";

	public static final String TXT_RESULT = "result";
	
	public static final String STATUS = "status";

}
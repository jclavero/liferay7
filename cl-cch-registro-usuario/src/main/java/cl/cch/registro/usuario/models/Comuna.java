package cl.cch.registro.usuario.models;

public class Comuna implements Comparable<Comuna>{

	private String nombre;
	private String comuna;
	private String region;
	private String direccion;
	
	@Override
	public String toString() {
		return "Comuna [nombre=" + nombre + ", comuna=" + comuna + ", region=" + region + ", direccion=" + direccion
				+ "]";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public int compareTo(Comuna comuna) {
		return this.nombre.compareTo(comuna.getNombre());
	}
	
}

package cl.cch.registro.usuario.helpers;


import com.liferay.dynamic.data.mapping.kernel.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;

import cl.cch.registro.usuario.models.DatosUsuario;
import cl.cch.registro.usuario.models.registroUsuarioPreferencias;

public class FormularioHelpers {

	private static final Log _log = LogFactoryUtil.getLog(FormularioHelpers.class);


	public static void enviarEmail(registroUsuarioPreferencias prefs, ResourceRequest resourceRequest,
			DatosUsuario datosForm) {
		String contenidoFinal = StringPool.BLANK;
		String email = prefs.getEmail();
		String mensaje = null;
		DDMTemplate _templateFormularioDatos = getTemplatesId("Plantilla-activacion-usuario", resourceRequest);
		String _plantillaFormularioContacto = _templateFormularioDatos.getScript();
		
		_plantillaFormularioContacto = _plantillaFormularioContacto
				.replace("${email}", datosForm.getEmail())
				.replace("${Mensaje}", mensaje);
		contenidoFinal = contenidoFinal.concat(_plantillaFormularioContacto);

		_log.info("correo deregistro enviado");
	
	try {
		
			InternetAddress to = new InternetAddress(email);

		
		String emailBody = contenidoFinal;
		String emailSubject = "Formulario de Contacto";

		InternetAddress from = new InternetAddress("contacto@correos.cl", "Formulario de Contacto");

		MailMessage message = new MailMessage();
		message.setFrom(from);
		message.setTo(to);

		message.setSubject(emailSubject);
		message.setBody(emailBody);
		message.setHTMLFormat(true);
		MailServiceUtil.sendEmail(message);
		_log.info("Email enviado correctamente a = " + email);
	} catch (Exception e) {
		_log.error(e.getMessage(), e);
	}
	}

	public static DDMTemplate getTemplatesId(String template, PortletRequest resourceRequest) {

		ThemeDisplay td = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<DDMTemplate> lista = null;

		ClassLoader cl = PortalClassLoaderUtil.getClassLoader();

		DynamicQuery dq = DDMTemplateLocalServiceUtil.dynamicQuery();
		dq.add(RestrictionsFactoryUtil.eq("groupId", td.getLayout().getGroupId()));
		dq.addOrder(OrderFactoryUtil.asc("name"));
		String templateName = "%" + template + "%";
		Criterion criterion = RestrictionsFactoryUtil.like("name", templateName);
		dq.add(criterion);

		try {
			lista = DDMTemplateLocalServiceUtil.dynamicQuery(dq);

			for (DDMTemplate plantilla : lista) {
				if (template.equalsIgnoreCase(plantilla.getName(resourceRequest.getLocale()))) {
					return plantilla;
				}
			}

		} catch (Exception e) {
			_log.error("NO Existe plantilla con nombre = " + template);
			_log.error("error al obtener la plantilla");
			_log.error(e.getMessage());
			return null;
		}

		_log.info("ALGO OCURRIO Y RETORNA PLANTILLA NULL");
		return null;

	}

}

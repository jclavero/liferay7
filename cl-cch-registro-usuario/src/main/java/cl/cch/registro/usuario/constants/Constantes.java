package cl.cch.registro.usuario.constants;

public class Constantes {

	public static final String CLCCHFORMULARIOCONTACTO = ClCchRegistroUsuarioPortletKeys.CLCCHREGISTROUSUARIO;
	public static final String CORREO = "correo";
	public static final String KEY_CAPCHA = "keyCapcha";
	public static final String KEY_CAPCHA_DEFAULT = "6LcS2MYUAAAAAHJPv0s3urGg6rJfgJfYphi0MZvX";
	public static final String URL_HOME = "urlHome";
	public static final String OBTENER_DATOS = "datosUsuario";
	public static final String PORTLET_PREFERENCIAS = "regPrefs";
	public static final String URL_WS_COD_POSTAL = "urlSwCodPostal";
	
	public static final String LISTA_COMUNAS = "comuna";
	public static final String LISTA_COMUNAS_REGION = "listComuna";
	public static final String LISTA_REGION = "region";
	public static final String TXT_RESULT = "result";

	
	public static final String PATH_BASE = "/";
	public static final String JSP_VIEW = "view.jsp";
	public static final String LINK_BASE = "/web/guest/validacion";
	public static final String JSP_EXITO = "exito.jsp";
	public static final String GRUPO_CORREOS = "CorreosChile";
	public static final String PERSONAL = "Personal";
	public static final String TIPO_DOCUMENTO = "Tipo_Documento";
	public static final String RUT = "RUT";
	public static final String PASAPORTE = "Pasaporte";
	public static final String TXT_ERROR = "error";
	public static final String CORREO_EXISTE = "correoRegistrado";
	
	//Nombre regiones
	public static final String TARAPACA = "Tarapac&aacute;";
	public static final String ANTOFAGASTA = "Antofagasta";
	public static final String ATACAMA = "Atacama";
	public static final String COQUIMBO = "Coquimbo";
	public static final String VALPARAISO = "Valpara&iacute;so";
	public static final String O_HIGGINS= "O Higgins";
	public static final String MAULE = "Maule";
	public static final String CONCEPCION = "Concepcion";
	public static final String ARAUCANIA = "Araucan&iacute;a";
	public static final String LOS_LAGOS = "Los Lagos";
	public static final String AYSEN = "Ays&eacute;n";
	public static final String MAGALLANES = "Magallanes";
	public static final String METROPOLITANA = "Metropolitana de Santiago";
	public static final String LOS_RIOS = "Los Rios";
	public static final String ARICA_PARINACOTA = "Arica y Parinacota";
	public static final String NUBLE = "&Ntilde;uble";
	
	public static final String REGION_I = "REGION I";
	public static final String REGION_II = "REGION II";
	public static final String REGION_III = "REGION III";
	public static final String REGION_IV = "REGION IV";
	public static final String REGION_V = "REGION V";
	public static final String REGION_VI= "REGION VI";
	public static final String REGION_VII = "REGION VII";
	public static final String REGION_VIII = "REGION VIII";
	public static final String REGION_IX = "REGION IX";
	public static final String REGION_X = "REGION X";
	public static final String REGION_XI = "REGION XI";
	public static final String REGION_XII = "REGION XII";
	public static final String REGION_METROPOLITANA = "REGION METROPOLITANA";
	public static final String REGION_XVI = "REGION XIII";
	public static final String REGION_XIV = "REGION XIV";
	public static final String REGION_XV = "REGION XV";
	public static final String  ERR_DIRECCION = "ERR_DIRECCION";
}
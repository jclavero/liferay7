/**
 * DireccionNormalizadaBasicaVO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.cch.registro.usuario.ws.normalizador;

public class DireccionNormalizadaBasicaVO  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String calle;

    private java.lang.String numero;

    private java.lang.String resto;

    private java.lang.String comuna;

    private java.lang.String cpostal;

    private java.lang.String direccion_original;

    private java.lang.String comuna_original;

    private java.lang.String sector_postal;

    private java.lang.String cuartel_postal;

    private java.lang.String sdp;

    public DireccionNormalizadaBasicaVO() {
    }

    public DireccionNormalizadaBasicaVO(
           java.lang.String id,
           java.lang.String calle,
           java.lang.String numero,
           java.lang.String resto,
           java.lang.String comuna,
           java.lang.String cpostal,
           java.lang.String direccion_original,
           java.lang.String comuna_original,
           java.lang.String sector_postal,
           java.lang.String cuartel_postal,
           java.lang.String sdp) {
           this.id = id;
           this.calle = calle;
           this.numero = numero;
           this.resto = resto;
           this.comuna = comuna;
           this.cpostal = cpostal;
           this.direccion_original = direccion_original;
           this.comuna_original = comuna_original;
           this.sector_postal = sector_postal;
           this.cuartel_postal = cuartel_postal;
           this.sdp = sdp;
    }


    /**
     * Gets the id value for this DireccionNormalizadaBasicaVO.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this DireccionNormalizadaBasicaVO.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the calle value for this DireccionNormalizadaBasicaVO.
     * 
     * @return calle
     */
    public java.lang.String getCalle() {
        return calle;
    }


    /**
     * Sets the calle value for this DireccionNormalizadaBasicaVO.
     * 
     * @param calle
     */
    public void setCalle(java.lang.String calle) {
        this.calle = calle;
    }


    /**
     * Gets the numero value for this DireccionNormalizadaBasicaVO.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this DireccionNormalizadaBasicaVO.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the resto value for this DireccionNormalizadaBasicaVO.
     * 
     * @return resto
     */
    public java.lang.String getResto() {
        return resto;
    }


    /**
     * Sets the resto value for this DireccionNormalizadaBasicaVO.
     * 
     * @param resto
     */
    public void setResto(java.lang.String resto) {
        this.resto = resto;
    }


    /**
     * Gets the comuna value for this DireccionNormalizadaBasicaVO.
     * 
     * @return comuna
     */
    public java.lang.String getComuna() {
        return comuna;
    }


    /**
     * Sets the comuna value for this DireccionNormalizadaBasicaVO.
     * 
     * @param comuna
     */
    public void setComuna(java.lang.String comuna) {
        this.comuna = comuna;
    }


    /**
     * Gets the cpostal value for this DireccionNormalizadaBasicaVO.
     * 
     * @return cpostal
     */
    public java.lang.String getCpostal() {
        return cpostal;
    }


    /**
     * Sets the cpostal value for this DireccionNormalizadaBasicaVO.
     * 
     * @param cpostal
     */
    public void setCpostal(java.lang.String cpostal) {
        this.cpostal = cpostal;
    }


    /**
     * Gets the direccion_original value for this DireccionNormalizadaBasicaVO.
     * 
     * @return direccion_original
     */
    public java.lang.String getDireccion_original() {
        return direccion_original;
    }


    /**
     * Sets the direccion_original value for this DireccionNormalizadaBasicaVO.
     * 
     * @param direccion_original
     */
    public void setDireccion_original(java.lang.String direccion_original) {
        this.direccion_original = direccion_original;
    }


    /**
     * Gets the comuna_original value for this DireccionNormalizadaBasicaVO.
     * 
     * @return comuna_original
     */
    public java.lang.String getComuna_original() {
        return comuna_original;
    }


    /**
     * Sets the comuna_original value for this DireccionNormalizadaBasicaVO.
     * 
     * @param comuna_original
     */
    public void setComuna_original(java.lang.String comuna_original) {
        this.comuna_original = comuna_original;
    }


    /**
     * Gets the sector_postal value for this DireccionNormalizadaBasicaVO.
     * 
     * @return sector_postal
     */
    public java.lang.String getSector_postal() {
        return sector_postal;
    }


    /**
     * Sets the sector_postal value for this DireccionNormalizadaBasicaVO.
     * 
     * @param sector_postal
     */
    public void setSector_postal(java.lang.String sector_postal) {
        this.sector_postal = sector_postal;
    }


    /**
     * Gets the cuartel_postal value for this DireccionNormalizadaBasicaVO.
     * 
     * @return cuartel_postal
     */
    public java.lang.String getCuartel_postal() {
        return cuartel_postal;
    }


    /**
     * Sets the cuartel_postal value for this DireccionNormalizadaBasicaVO.
     * 
     * @param cuartel_postal
     */
    public void setCuartel_postal(java.lang.String cuartel_postal) {
        this.cuartel_postal = cuartel_postal;
    }


    /**
     * Gets the sdp value for this DireccionNormalizadaBasicaVO.
     * 
     * @return sdp
     */
    public java.lang.String getSdp() {
        return sdp;
    }


    /**
     * Sets the sdp value for this DireccionNormalizadaBasicaVO.
     * 
     * @param sdp
     */
    public void setSdp(java.lang.String sdp) {
        this.sdp = sdp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DireccionNormalizadaBasicaVO)) return false;
        DireccionNormalizadaBasicaVO other = (DireccionNormalizadaBasicaVO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.calle==null && other.getCalle()==null) || 
             (this.calle!=null &&
              this.calle.equals(other.getCalle()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.resto==null && other.getResto()==null) || 
             (this.resto!=null &&
              this.resto.equals(other.getResto()))) &&
            ((this.comuna==null && other.getComuna()==null) || 
             (this.comuna!=null &&
              this.comuna.equals(other.getComuna()))) &&
            ((this.cpostal==null && other.getCpostal()==null) || 
             (this.cpostal!=null &&
              this.cpostal.equals(other.getCpostal()))) &&
            ((this.direccion_original==null && other.getDireccion_original()==null) || 
             (this.direccion_original!=null &&
              this.direccion_original.equals(other.getDireccion_original()))) &&
            ((this.comuna_original==null && other.getComuna_original()==null) || 
             (this.comuna_original!=null &&
              this.comuna_original.equals(other.getComuna_original()))) &&
            ((this.sector_postal==null && other.getSector_postal()==null) || 
             (this.sector_postal!=null &&
              this.sector_postal.equals(other.getSector_postal()))) &&
            ((this.cuartel_postal==null && other.getCuartel_postal()==null) || 
             (this.cuartel_postal!=null &&
              this.cuartel_postal.equals(other.getCuartel_postal()))) &&
            ((this.sdp==null && other.getSdp()==null) || 
             (this.sdp!=null &&
              this.sdp.equals(other.getSdp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCalle() != null) {
            _hashCode += getCalle().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getResto() != null) {
            _hashCode += getResto().hashCode();
        }
        if (getComuna() != null) {
            _hashCode += getComuna().hashCode();
        }
        if (getCpostal() != null) {
            _hashCode += getCpostal().hashCode();
        }
        if (getDireccion_original() != null) {
            _hashCode += getDireccion_original().hashCode();
        }
        if (getComuna_original() != null) {
            _hashCode += getComuna_original().hashCode();
        }
        if (getSector_postal() != null) {
            _hashCode += getSector_postal().hashCode();
        }
        if (getCuartel_postal() != null) {
            _hashCode += getCuartel_postal().hashCode();
        }
        if (getSdp() != null) {
            _hashCode += getSdp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DireccionNormalizadaBasicaVO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DireccionNormalizadaBasicaVO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "calle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "resto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comuna");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "comuna"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpostal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "cpostal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direccion_original");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "direccion_original"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comuna_original");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "comuna_original"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sector_postal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sector_postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuartel_postal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "cuartel_postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sdp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sdp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

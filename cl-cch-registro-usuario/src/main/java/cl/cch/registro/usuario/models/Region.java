package cl.cch.registro.usuario.models;

public class Region implements Comparable<Region> {

	private String nombre;
	private String comuna;
	private String region;
	private String direccion;
	private String nombreRegion;
	
	
	
	public String getNombreRegion() {
		return nombreRegion;
	}
	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	@Override
	public String toString() {
		return "Region [nombre=" + nombre + ", comuna=" + comuna + ", region=" + region + ", direccion=" + direccion
				+ ", nombreRegion=" + nombreRegion + "]";
	}
	@Override
	public int compareTo(Region region) {
		return this.nombre.compareTo(region.getNombre());
	}
	
	
}

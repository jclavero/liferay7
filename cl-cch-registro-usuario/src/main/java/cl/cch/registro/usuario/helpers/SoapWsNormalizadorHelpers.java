package cl.cch.registro.usuario.helpers;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;


import cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullConComunalVO;
import cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullVO;
import cl.cch.registro.usuario.ws.normalizador.IServInt;
import cl.cch.registro.usuario.ws.normalizador.ServInt;
import cl.cch.registro.usuario.ws.normalizador.ServIntLocator;





public class SoapWsNormalizadorHelpers {
	private static final Log _log = LogFactoryUtil.getLog(SoapWsNormalizadorHelpers.class);

    private ServIntLocator proxy;
    private String urlServicio;
    private static SoapWsNormalizadorHelpers instance;
    
    private SoapWsNormalizadorHelpers() {
        super();
    }
    
    private SoapWsNormalizadorHelpers(ServIntLocator proxy,String urlServicio) {
        this.proxy = proxy;
        this.urlServicio = urlServicio;
    }
 
    private ServIntLocator getProxy(){
        return this.proxy;
    }
     
    private void setProxy(ServIntLocator proxy){
        this.proxy  = proxy;
    }
 
    private void setUrlServicio(String urlServicio) {
        this.urlServicio = urlServicio;
    }
     
    public String getUrlServicio() {
        return this.urlServicio;
    }
    
	public static DireccionNormalizadaFullConComunalVO getDireccion(String url, String direccion){
		_log.info("entre al normalizador");
		DireccionNormalizadaFullConComunalVO direccionNormalizada = new DireccionNormalizadaFullConComunalVO();
		try {
			FutureTask<DireccionNormalizadaFullConComunalVO> timeoutTask = null;
			ServInt ss = new ServIntLocator();
			IServInt port = ss.getBasicHttpBinding_IServInt(new URL(url));
			
			timeoutTask = new FutureTask<DireccionNormalizadaFullConComunalVO>(new Callable<DireccionNormalizadaFullConComunalVO>() {
				@Override
		        public DireccionNormalizadaFullConComunalVO call() throws Exception {
					DireccionNormalizadaFullConComunalVO direccionNormalizada = port.normalizarFullConComunal(direccion);
					return direccionNormalizada;
		        }
		    });
			
			new Thread(timeoutTask).start();
			direccionNormalizada = timeoutTask.get(5, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			_log.error(e);
		}
		_log.info("salimos del normalizador");
		return direccionNormalizada;
	}  
}

package cl.cch.registro.usuario.models;

public class registroUsuarioPreferencias {

	private String email;
	private String keyCapcha;
	private String urlSwCodPostal;
	private String urlHome;
	
	
	
	public String getUrlHome() {
		return urlHome;
	}
	public void setUrlHome(String urlHome) {
		this.urlHome = urlHome;
	}
	@Override
	public String toString() {
		return "registroUsuarioPreferencias [email=" + email + ", keyCapcha=" + keyCapcha + ", urlSwCodPostal="
				+ urlSwCodPostal + ", urlHome=" + urlHome + "]";
	}
	public String getKeyCapcha() {
		return keyCapcha;
	}
	public void setKeyCapcha(String keyCapcha) {
		this.keyCapcha = keyCapcha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUrlSwCodPostal() {
		return urlSwCodPostal;
	}
	public void setUrlSwCodPostal(String urlSwCodPostal) {
		this.urlSwCodPostal = urlSwCodPostal;
	}
	
	
	
}

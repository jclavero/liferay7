/**
 * IServInt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.cch.registro.usuario.ws.normalizador;

public interface IServInt extends java.rmi.Remote {
    public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaBasicaVO normalizar(java.lang.String direccion) throws java.rmi.RemoteException;
    public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullVO normalizarFull(java.lang.String direccion) throws java.rmi.RemoteException;
    public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaFullConComunalVO normalizarFullConComunal(java.lang.String direccion) throws java.rmi.RemoteException;
    public cl.cch.registro.usuario.ws.normalizador.DireccionNormalizadaBasicaVO[] normalizarListMasivo(cl.cch.registro.usuario.ws.normalizador.Address[] direcciones) throws java.rmi.RemoteException;
}

/**
 * ServIntLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.cch.registro.usuario.ws.normalizador;

public class ServIntLocator extends org.apache.axis.client.Service implements cl.cch.registro.usuario.ws.normalizador.ServInt {

    public ServIntLocator() {
    }


    public ServIntLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServIntLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_IServInt
    private java.lang.String BasicHttpBinding_IServInt_address = "http://10.88.72.81:8080/ServInt.svc";

    public java.lang.String getBasicHttpBinding_IServIntAddress() {
        return BasicHttpBinding_IServInt_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_IServIntWSDDServiceName = "BasicHttpBinding_IServInt";

    public java.lang.String getBasicHttpBinding_IServIntWSDDServiceName() {
        return BasicHttpBinding_IServIntWSDDServiceName;
    }

    public void setBasicHttpBinding_IServIntWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_IServIntWSDDServiceName = name;
    }

    public cl.cch.registro.usuario.ws.normalizador.IServInt getBasicHttpBinding_IServInt() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_IServInt_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_IServInt(endpoint);
    }

    public cl.cch.registro.usuario.ws.normalizador.IServInt getBasicHttpBinding_IServInt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.cch.registro.usuario.ws.normalizador.BasicHttpBinding_IServIntStub _stub = new cl.cch.registro.usuario.ws.normalizador.BasicHttpBinding_IServIntStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_IServIntWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_IServIntEndpointAddress(java.lang.String address) {
        BasicHttpBinding_IServInt_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.cch.registro.usuario.ws.normalizador.IServInt.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.cch.registro.usuario.ws.normalizador.BasicHttpBinding_IServIntStub _stub = new cl.cch.registro.usuario.ws.normalizador.BasicHttpBinding_IServIntStub(new java.net.URL(BasicHttpBinding_IServInt_address), this);
                _stub.setPortName(getBasicHttpBinding_IServIntWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_IServInt".equals(inputPortName)) {
            return getBasicHttpBinding_IServInt();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "ServInt");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpBinding_IServInt"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_IServInt".equals(portName)) {
            setBasicHttpBinding_IServIntEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

package cl.cch.registro.usuario.utils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cl.cch.registro.usuario.portlet.ClCchRegistroUsuarioPortlet;

public class ErrorHandlerValidationUtils {
	private static final Log _log = LogFactoryUtil.getLog(ErrorHandlerValidationUtils.class);
	public String rutFormat(String rut) {
		int cont = 0;
		String format;
		rut = rut.replace(".", "");
		rut = rut.replace("-", "");
		format = "-" + rut.substring(rut.length() - 1);
		for (int i = rut.length() - 2; i >= 0; i--) {
			format = rut.substring(i, i + 1) + format;
			cont++;
			if (cont == 3 && i != 0) {
				format = "" + format;
				cont = 0;
			}
		}
		return format;
	}

	public boolean rutValidate(String rut) {
		rut = rutFormat(rut);
		boolean validacion = false;
		try {
			rut = rut.toUpperCase();
			rut = rut.replace(".", "");
			rut = rut.replace("-", "");
			int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
			char dv = rut.charAt(rut.length() - 1);
			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
			}
		} catch (Exception e) {
		}
		return validacion;
	}

	public boolean emailFormat(String email) {

		// Patr�n para validar el email
		boolean flag;
		Pattern pattern = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		Matcher mather = pattern.matcher(email);

		if (mather.find() == true) {
			flag = true;
			_log.info("El email ingresado es valido.");
		} else {
			flag = false;
			_log.error("El email ingresado es invalido.");
		}

		return flag;

	}

	public boolean passwordValidate(String pass) {
		Pattern pattern = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z]).{8,40})");
		Matcher mather = pattern.matcher(pass);
		if (mather.find() == true) {
			_log.debug("pass Correcta");
			return true;
		} else {
			_log.error("pass incorrecta");
			return false;
		}
	}

	public boolean isString(String input) {
		for (int i = 0; i < input.length(); i++) {
			char caracter = input.toUpperCase().charAt(i);
			int valorASCII = (int) caracter;
			if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
				return false;
		}
		return true;
	}

	@SuppressWarnings("unused")
	public boolean isNumeric(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public  boolean isTelephone(String input) {
		Pattern pattern = Pattern.compile("(^(\\+?56)?(\\s?)(0?9)(\\s?)[9876543]\\d{7}$)");
		Matcher matcher = pattern.matcher(input);
		if (matcher.find() == true) {
			_log.info("El telefono ingresado es valido.");
			return true;
		} else {
			_log.error("El telefono ingresado es invalido.");
			return false;
		}

	}

	public  boolean birthDayValid(String year, String month , String day) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			formatoFecha.setLenient(false);
			formatoFecha.parse(day.concat("/").concat(month).concat("/").concat(year));

		} catch (java.text.ParseException e) {
			_log.error("error en el cumpleaños: " + e.getMessage());
			return false;
		}
		return true;

	}

	public  Date getFecha(String fecha) {

		try {
			Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
			return date1;
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public static String[] getAddressFormat(String address) {
        String strMain = address;   
	        String[] arrSplit = strMain.split(",");   
			return arrSplit;
//			String[] add = getAddressFormat("san ramon, tacna, 8885");
//			for(int i =0; i<add.length; i++) {
//				System.out.println(add[i].trim());
//			}
	}


}

package cl.cch.registro.usuario.utils;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.internet.InternetAddress;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;

import org.joda.time.DateTime;

import cl.cch.registro.usuario.constants.Constantes;
import cl.cch.registro.usuario.models.Comuna;
import cl.cch.registro.usuario.models.Region;
import cl.cch.sucursales.sb.model.Sucursal;
import cl.cch.sucursales.sb.service.SucursalLocalServiceUtil;

public class RegisterUtils {

	private static final Log _log = LogFactoryUtil.getLog(RegisterUtils.class);
	private static List<Sucursal> listaSucursales = SucursalLocalServiceUtil.getSucursals(-1, -1);

	public static void enviarEmail(ResourceRequest resourceRequest, String email, String code) {
		String contenidoFinal = StringPool.BLANK;

		DDMTemplate _templateRecuperarPass = getTemplatesId("Plantilla-activacion-usuario", resourceRequest);
		String _plantillaFormularioContacto = _templateRecuperarPass.getScript();

		_plantillaFormularioContacto = _plantillaFormularioContacto.replace("${correoElectronico}", email)
				.replace("${Mensaje}", code);
		contenidoFinal = contenidoFinal.concat(_plantillaFormularioContacto);

		try {

			String emailBody = contenidoFinal;
			String emailSubject = "Sucursal Virtual - confirmacion registro Sucursal Virtual";

			InternetAddress from = new InternetAddress("noresponder@correos.cl", "CorreosChile");

			MailMessage message = new MailMessage();
			message.setFrom(from);
			message.setTo(new InternetAddress(email));

			message.setSubject(emailSubject);
			message.setBody(emailBody);
			message.setHTMLFormat(true);
			MailServiceUtil.sendEmail(message);
			_log.info("Email enviado correctamente a = " + email);
		} catch (Exception e) {
			_log.error(e.getMessage(), e);
		}
	}

	public static DDMTemplate getTemplatesId(String template, PortletRequest resourceRequest) {

		ThemeDisplay td = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<DDMTemplate> lista = null;

		DynamicQuery dq = DDMTemplateLocalServiceUtil.dynamicQuery();
		dq.add(RestrictionsFactoryUtil.eq("groupId", td.getLayout().getGroupId()));
		dq.addOrder(OrderFactoryUtil.asc("name"));
		String templateName = "%" + template + "%";
		Criterion criterion = RestrictionsFactoryUtil.like("name", templateName);
		dq.add(criterion);

		try {
			lista = DDMTemplateLocalServiceUtil.dynamicQuery(dq);

			for (DDMTemplate plantilla : lista) {
				if (template.equalsIgnoreCase(plantilla.getName(resourceRequest.getLocale()))) {
					return plantilla;
				}
			}

		} catch (Exception e) {
			_log.error("NO Existe plantilla con nombre = " + template);
			_log.error("error al obtener la plantilla");
			_log.error(e.getMessage());
			return null;
		}

		_log.info("ALGO OCURRIO Y RETORNA PLANTILLA NULL");
		return null;

	}

	public static List<Comuna> getAllComunasBySucursales() {
		List<Comuna> listaComunas = new ArrayList<Comuna>();
		Set<String> setArticles = new HashSet<>();
		for (Sucursal sucursal : listaSucursales) {
			if (setArticles.contains(sucursal.getComuna())) {
				continue;
			}
			setArticles.add(sucursal.getComuna());
			listaComunas.add(getInfoComuna(sucursal));
		}
		Collections.sort(listaComunas);
		return listaComunas;
	}
	
	public static String getNameRegion(String region) {
		String nombreRegion = null;
		
		if(Constantes.REGION_I.equalsIgnoreCase(region)){
			nombreRegion = Constantes.TARAPACA;
		}
		
		if(Constantes.REGION_II.equalsIgnoreCase(region)){
			nombreRegion = Constantes.ANTOFAGASTA;
		}
		
		if(Constantes.REGION_III.equalsIgnoreCase(region)){
			nombreRegion = Constantes.ATACAMA;
		}
		
		if(Constantes.REGION_IV.equalsIgnoreCase(region)){
			nombreRegion = Constantes.COQUIMBO;
		}
		
		if(Constantes.REGION_V.equalsIgnoreCase(region)){
			nombreRegion = Constantes.VALPARAISO;
		}
		
		if(Constantes.REGION_VI.equalsIgnoreCase(region)){
			nombreRegion = Constantes.O_HIGGINS;
		}
		
		if(Constantes.REGION_VII.equalsIgnoreCase(region)){
			nombreRegion = Constantes.MAULE;
		}
		
		if(Constantes.REGION_VIII.equalsIgnoreCase(region)){
			nombreRegion = Constantes.CONCEPCION;
		}
		
		if(Constantes.REGION_IX.equalsIgnoreCase(region)){
			nombreRegion = Constantes.ARAUCANIA;
		}
		
		if(Constantes.REGION_X.equalsIgnoreCase(region)){
			nombreRegion = Constantes.LOS_LAGOS;
		}
		
		if(Constantes.REGION_XI.equalsIgnoreCase(region)){
			nombreRegion = Constantes.AYSEN;
		}
		
		if(Constantes.REGION_XII.equalsIgnoreCase(region)){
			nombreRegion = Constantes.MAGALLANES;
		}
		
		if(Constantes.REGION_METROPOLITANA.equalsIgnoreCase(region)){
			nombreRegion = Constantes.METROPOLITANA;
		}
		
		if(Constantes.REGION_XIV.equalsIgnoreCase(region)){
			nombreRegion = Constantes.LOS_RIOS;
		}
		
		if(Constantes.REGION_XV.equalsIgnoreCase(region)){
			nombreRegion = Constantes.ARICA_PARINACOTA;
		}
		
		if(Constantes.REGION_XVI.equalsIgnoreCase(region)){
			nombreRegion = Constantes.NUBLE;
		}
		
		return nombreRegion;
	}

	public static List<Region> getAllRegion() {
		List<Region> listaRegion = new ArrayList<Region>();
		Set<String> setArticles = new HashSet<>();
		for (Sucursal sucursal : listaSucursales) {
			if (setArticles.contains(sucursal.getRegion())) {
				continue;
			}
			
			setArticles.add(sucursal.getRegion());
			listaRegion.add(getInfoRegion(sucursal));
		}
		Collections.sort(listaRegion);
		return listaRegion;
	}

	public static Comuna getInfoComuna(Sucursal sucursal) {
		Comuna comuna = new Comuna();
		if (Validator.isNotNull(sucursal)) {
			comuna.setNombre(sucursal.getNombre());
			comuna.setComuna(sucursal.getComuna());
			comuna.setRegion(sucursal.getRegion());
			comuna.setDireccion(sucursal.getCalle());
		}
		return comuna;
	}

	public static Region getInfoRegion(Sucursal sucursal) {
		Region region = new Region();
		if (Validator.isNotNull(sucursal)) {
			region.setNombre(sucursal.getNombre());
			region.setComuna(sucursal.getComuna());
			region.setRegion(sucursal.getRegion());
			region.setDireccion(sucursal.getCalle());
			region.setNombreRegion(getNameRegion(region.getRegion()));
		}
		return region;
	}
	
	public static String getRegionByComuna(String comuna){
		String region = null;
		
		Set<String> setArticles = new HashSet<>();
		for (Sucursal sucursal : listaSucursales) {
			if (setArticles.contains(sucursal.getComuna())) {
				continue;
			}
			if(comuna.contains("puente")) {
				if(sucursal.getComuna().contains(comuna)) {
					region = getNameRegion(sucursal.getRegion());
					_log.info("region = " + region);
				}
			}
			
			
		}
		return region;
	}
	
	public static List<Comuna> getAllComunasByRegion(String region) {
		List<Comuna> listaComunas = new ArrayList<Comuna>();
		Set<String> setArticles = new HashSet<>();
		for (Sucursal sucursal : listaSucursales) {
			if (setArticles.contains(sucursal.getComuna())) {
				continue;
			}
			if(sucursal.getRegion().equalsIgnoreCase(region)) {
				setArticles.add(sucursal.getComuna());
				listaComunas.add(getInfoComuna(sucursal));
			}
			
		}
		Collections.sort(listaComunas);
		return listaComunas;
	}
	
	
	public static Date createBirthday(String dia, String mes, String anio) {
		
		
		String dia1 = String.valueOf(dia);
		dia1.length();
		if (dia1.length() == 1) {
			dia1 = "0" + dia1;
		}
		String mes1 = String.valueOf(mes);
		mes1.length();
		if (mes1.length() == 1) {
			mes1 = "0" + mes1;
		}
		String agno = String.valueOf(anio);
		
		DateTime jd = new DateTime(Integer.parseInt(agno), Integer.parseInt(mes1), Integer.parseInt(dia1), 0, 0);
		Date fecha = jd.toDate();	
		
		return fecha;
	}
	

}

/**
 * ServInt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.cch.registro.usuario.ws.normalizador;

public interface ServInt extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_IServIntAddress();

    public cl.cch.registro.usuario.ws.normalizador.IServInt getBasicHttpBinding_IServInt() throws javax.xml.rpc.ServiceException;

    public cl.cch.registro.usuario.ws.normalizador.IServInt getBasicHttpBinding_IServInt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

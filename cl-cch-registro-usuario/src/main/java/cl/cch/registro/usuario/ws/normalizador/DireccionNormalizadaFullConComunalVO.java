/**
 * DireccionNormalizadaFullConComunalVO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.cch.registro.usuario.ws.normalizador;

public class DireccionNormalizadaFullConComunalVO  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String calle;

    private java.lang.String numero;

    private java.lang.String resto;

    private java.lang.String comuna;

    private java.lang.String cpostal;

    private java.lang.String cpostalComunal;

    private java.lang.String direccion_original;

    private java.lang.String comuna_original;

    private java.lang.String sector_postal;

    private java.lang.String cuartel_postal;

    private java.lang.String sdp;

    private java.lang.String ciudad;

    private java.lang.String region;

    private java.lang.String coordX;

    private java.lang.String coordY;

    private java.lang.String latitud;

    private java.lang.String longitud;

    private boolean aceptado;

    private java.lang.String estado;

    public DireccionNormalizadaFullConComunalVO() {
    }

    public DireccionNormalizadaFullConComunalVO(
           java.lang.String id,
           java.lang.String calle,
           java.lang.String numero,
           java.lang.String resto,
           java.lang.String comuna,
           java.lang.String cpostal,
           java.lang.String cpostalComunal,
           java.lang.String direccion_original,
           java.lang.String comuna_original,
           java.lang.String sector_postal,
           java.lang.String cuartel_postal,
           java.lang.String sdp,
           java.lang.String ciudad,
           java.lang.String region,
           java.lang.String coordX,
           java.lang.String coordY,
           java.lang.String latitud,
           java.lang.String longitud,
           boolean aceptado,
           java.lang.String estado) {
           this.id = id;
           this.calle = calle;
           this.numero = numero;
           this.resto = resto;
           this.comuna = comuna;
           this.cpostal = cpostal;
           this.cpostalComunal = cpostalComunal;
           this.direccion_original = direccion_original;
           this.comuna_original = comuna_original;
           this.sector_postal = sector_postal;
           this.cuartel_postal = cuartel_postal;
           this.sdp = sdp;
           this.ciudad = ciudad;
           this.region = region;
           this.coordX = coordX;
           this.coordY = coordY;
           this.latitud = latitud;
           this.longitud = longitud;
           this.aceptado = aceptado;
           this.estado = estado;
    }


    /**
     * Gets the id value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the calle value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return calle
     */
    public java.lang.String getCalle() {
        return calle;
    }


    /**
     * Sets the calle value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param calle
     */
    public void setCalle(java.lang.String calle) {
        this.calle = calle;
    }


    /**
     * Gets the numero value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the resto value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return resto
     */
    public java.lang.String getResto() {
        return resto;
    }


    /**
     * Sets the resto value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param resto
     */
    public void setResto(java.lang.String resto) {
        this.resto = resto;
    }


    /**
     * Gets the comuna value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return comuna
     */
    public java.lang.String getComuna() {
        return comuna;
    }


    /**
     * Sets the comuna value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param comuna
     */
    public void setComuna(java.lang.String comuna) {
        this.comuna = comuna;
    }


    /**
     * Gets the cpostal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return cpostal
     */
    public java.lang.String getCpostal() {
        return cpostal;
    }


    /**
     * Sets the cpostal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param cpostal
     */
    public void setCpostal(java.lang.String cpostal) {
        this.cpostal = cpostal;
    }


    /**
     * Gets the cpostalComunal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return cpostalComunal
     */
    public java.lang.String getCpostalComunal() {
        return cpostalComunal;
    }


    /**
     * Sets the cpostalComunal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param cpostalComunal
     */
    public void setCpostalComunal(java.lang.String cpostalComunal) {
        this.cpostalComunal = cpostalComunal;
    }


    /**
     * Gets the direccion_original value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return direccion_original
     */
    public java.lang.String getDireccion_original() {
        return direccion_original;
    }


    /**
     * Sets the direccion_original value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param direccion_original
     */
    public void setDireccion_original(java.lang.String direccion_original) {
        this.direccion_original = direccion_original;
    }


    /**
     * Gets the comuna_original value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return comuna_original
     */
    public java.lang.String getComuna_original() {
        return comuna_original;
    }


    /**
     * Sets the comuna_original value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param comuna_original
     */
    public void setComuna_original(java.lang.String comuna_original) {
        this.comuna_original = comuna_original;
    }


    /**
     * Gets the sector_postal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return sector_postal
     */
    public java.lang.String getSector_postal() {
        return sector_postal;
    }


    /**
     * Sets the sector_postal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param sector_postal
     */
    public void setSector_postal(java.lang.String sector_postal) {
        this.sector_postal = sector_postal;
    }


    /**
     * Gets the cuartel_postal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return cuartel_postal
     */
    public java.lang.String getCuartel_postal() {
        return cuartel_postal;
    }


    /**
     * Sets the cuartel_postal value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param cuartel_postal
     */
    public void setCuartel_postal(java.lang.String cuartel_postal) {
        this.cuartel_postal = cuartel_postal;
    }


    /**
     * Gets the sdp value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return sdp
     */
    public java.lang.String getSdp() {
        return sdp;
    }


    /**
     * Sets the sdp value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param sdp
     */
    public void setSdp(java.lang.String sdp) {
        this.sdp = sdp;
    }


    /**
     * Gets the ciudad value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return ciudad
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }


    /**
     * Sets the ciudad value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param ciudad
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }


    /**
     * Gets the region value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the coordX value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return coordX
     */
    public java.lang.String getCoordX() {
        return coordX;
    }


    /**
     * Sets the coordX value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param coordX
     */
    public void setCoordX(java.lang.String coordX) {
        this.coordX = coordX;
    }


    /**
     * Gets the coordY value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return coordY
     */
    public java.lang.String getCoordY() {
        return coordY;
    }


    /**
     * Sets the coordY value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param coordY
     */
    public void setCoordY(java.lang.String coordY) {
        this.coordY = coordY;
    }


    /**
     * Gets the latitud value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return latitud
     */
    public java.lang.String getLatitud() {
        return latitud;
    }


    /**
     * Sets the latitud value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param latitud
     */
    public void setLatitud(java.lang.String latitud) {
        this.latitud = latitud;
    }


    /**
     * Gets the longitud value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return longitud
     */
    public java.lang.String getLongitud() {
        return longitud;
    }


    /**
     * Sets the longitud value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param longitud
     */
    public void setLongitud(java.lang.String longitud) {
        this.longitud = longitud;
    }


    /**
     * Gets the aceptado value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return aceptado
     */
    public boolean isAceptado() {
        return aceptado;
    }


    /**
     * Sets the aceptado value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param aceptado
     */
    public void setAceptado(boolean aceptado) {
        this.aceptado = aceptado;
    }


    /**
     * Gets the estado value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this DireccionNormalizadaFullConComunalVO.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DireccionNormalizadaFullConComunalVO)) return false;
        DireccionNormalizadaFullConComunalVO other = (DireccionNormalizadaFullConComunalVO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.calle==null && other.getCalle()==null) || 
             (this.calle!=null &&
              this.calle.equals(other.getCalle()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.resto==null && other.getResto()==null) || 
             (this.resto!=null &&
              this.resto.equals(other.getResto()))) &&
            ((this.comuna==null && other.getComuna()==null) || 
             (this.comuna!=null &&
              this.comuna.equals(other.getComuna()))) &&
            ((this.cpostal==null && other.getCpostal()==null) || 
             (this.cpostal!=null &&
              this.cpostal.equals(other.getCpostal()))) &&
            ((this.cpostalComunal==null && other.getCpostalComunal()==null) || 
             (this.cpostalComunal!=null &&
              this.cpostalComunal.equals(other.getCpostalComunal()))) &&
            ((this.direccion_original==null && other.getDireccion_original()==null) || 
             (this.direccion_original!=null &&
              this.direccion_original.equals(other.getDireccion_original()))) &&
            ((this.comuna_original==null && other.getComuna_original()==null) || 
             (this.comuna_original!=null &&
              this.comuna_original.equals(other.getComuna_original()))) &&
            ((this.sector_postal==null && other.getSector_postal()==null) || 
             (this.sector_postal!=null &&
              this.sector_postal.equals(other.getSector_postal()))) &&
            ((this.cuartel_postal==null && other.getCuartel_postal()==null) || 
             (this.cuartel_postal!=null &&
              this.cuartel_postal.equals(other.getCuartel_postal()))) &&
            ((this.sdp==null && other.getSdp()==null) || 
             (this.sdp!=null &&
              this.sdp.equals(other.getSdp()))) &&
            ((this.ciudad==null && other.getCiudad()==null) || 
             (this.ciudad!=null &&
              this.ciudad.equals(other.getCiudad()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.coordX==null && other.getCoordX()==null) || 
             (this.coordX!=null &&
              this.coordX.equals(other.getCoordX()))) &&
            ((this.coordY==null && other.getCoordY()==null) || 
             (this.coordY!=null &&
              this.coordY.equals(other.getCoordY()))) &&
            ((this.latitud==null && other.getLatitud()==null) || 
             (this.latitud!=null &&
              this.latitud.equals(other.getLatitud()))) &&
            ((this.longitud==null && other.getLongitud()==null) || 
             (this.longitud!=null &&
              this.longitud.equals(other.getLongitud()))) &&
            this.aceptado == other.isAceptado() &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCalle() != null) {
            _hashCode += getCalle().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getResto() != null) {
            _hashCode += getResto().hashCode();
        }
        if (getComuna() != null) {
            _hashCode += getComuna().hashCode();
        }
        if (getCpostal() != null) {
            _hashCode += getCpostal().hashCode();
        }
        if (getCpostalComunal() != null) {
            _hashCode += getCpostalComunal().hashCode();
        }
        if (getDireccion_original() != null) {
            _hashCode += getDireccion_original().hashCode();
        }
        if (getComuna_original() != null) {
            _hashCode += getComuna_original().hashCode();
        }
        if (getSector_postal() != null) {
            _hashCode += getSector_postal().hashCode();
        }
        if (getCuartel_postal() != null) {
            _hashCode += getCuartel_postal().hashCode();
        }
        if (getSdp() != null) {
            _hashCode += getSdp().hashCode();
        }
        if (getCiudad() != null) {
            _hashCode += getCiudad().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getCoordX() != null) {
            _hashCode += getCoordX().hashCode();
        }
        if (getCoordY() != null) {
            _hashCode += getCoordY().hashCode();
        }
        if (getLatitud() != null) {
            _hashCode += getLatitud().hashCode();
        }
        if (getLongitud() != null) {
            _hashCode += getLongitud().hashCode();
        }
        _hashCode += (isAceptado() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DireccionNormalizadaFullConComunalVO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DireccionNormalizadaFullConComunalVO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "calle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "resto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comuna");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "comuna"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpostal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "cpostal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpostalComunal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "cpostalComunal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direccion_original");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "direccion_original"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comuna_original");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "comuna_original"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sector_postal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sector_postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuartel_postal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "cuartel_postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sdp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sdp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ciudad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ciudad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coordX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CoordX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coordY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CoordY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitud");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "latitud"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitud");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "longitud"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aceptado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "aceptado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

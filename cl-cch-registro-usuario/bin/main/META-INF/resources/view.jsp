<%@ include file="init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page language="java"  contentType="text/html; charset=UTF-8"%>
<portlet:resourceURL var="datosUsuario" id="datosUsuario"></portlet:resourceURL>
<portlet:resourceURL var="obtenerCP" id="obtenerCP"></portlet:resourceURL>
<portlet:resourceURL var="validCorreo" id="validCorreo"></portlet:resourceURL>
<portlet:resourceURL var="actionRegion" id="actionRegion"></portlet:resourceURL>

<div id="regristro">
	<form id="<portlet:namespace/>formFormulario" method="post">
		<div class="max-w-63 w-100 mx-auto">
			<div id="form_part1">
				<div class="max-w-63 w-100 mx-auto">
					<h1 class="second-title text-center mb-6">
						Reg&iacute;strate en la Sucursal Virtual <span class="c-red-1">Mi Correos</span> y descubre servicios exclusivos para ti.
					</h1>
				</div>
				<div class="max-w-63 w-100 mx-auto mb-4">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<ul class="circle-list my-0">
									<li class="ok-green pb-2 c-grey-1">Pre-ingresa env&iacute;os
									</li>
									<li class="ok-green pb-2 c-grey-1">Paga en l&iacute;nea o en sucursal
									</li>
									<li class="ok-green pb-2 c-grey-1">Gestiona tus env&iacute;os
									</li>
								</ul>
							</div>
							<div class="col-md-6">
								<ul class="circle-list my-0">
									<li class="ok-green pb-2 c-grey-1">Guarda tus env&iacute;os favoritos</li>
									<li class="ok-green pb-2 c-grey-1">Guarda tus direcciones y m&aacute;s</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div
					class="bg-white py-8 px-5 rounded-common shadow-center max-w-95 mx-auto">
					<div class="max-w-90 w-100 mx-auto">
						<div class="wizard">
							<div class="wizard-graph-row">
								<div class="wizard-block wizard-block-advanced">
									<span class="circle-number">1</span>
								</div>
								<div class="wizard-block">
									<span class="circle-number">2</span>
								</div>
								<div class="wizard-block">
									<span class="circle-number">3</span>
								</div>
							</div>
							<div class="wizard-text-row">
								<div class="wizard-block">
									Datos<br> Personales
								</div>
								<div class="wizard-block">T&eacute;rminos y Condiciones</div>
								<div class="wizard-block">
									Registro<br> Completo
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaFormularioError" style="display: none;">
					<p class="mb-0 d-inline-block small-text f-common-regular c-white">
						<strong>Tienes un error en el formulario</strong>
					</p>
					<button type="button" class="close" aria-label="Close">
					<i class="la la-times c-white fs-7" onclick="ocultarAlertaFormularioError();">&nbsp;</i>
					</button>
				</div>
				<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaFechaError" style="display: none;">
					<p class="mb-0 d-inline-block small-text f-common-regular c-white">
						<strong>La fecha de nacimiento no puede ser posterior a la fecha actual</strong>
					</p>
					<button type="button" class="close" aria-label="Close">
					<i class="la la-times c-white fs-7" onclick="ocultarAlertaFechaError();">&nbsp;</i>
					</button>
				</div>
				<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaFormularioErrorCorreo" style="display: none;">
					<p class="mb-0 d-inline-block small-text f-common-regular c-white">
						<strong>El correo ya se encuentra registrado.</strong>
					</p>
					<button type="button" class="close" aria-label="Close">
					<i class="la la-times c-white fs-7" onclick="ocultarAlertaFormularioErrorCorreo();">&nbsp;</i>
					</button>
				</div>
				<hr class="bb-dashed">
				<h2 class="basic-text f-common-bold c-grey-1">Datos personales</h2>
				<p class="small-text mb-0 c-grey-1">Completa con tus datos personales la informaci&oacute;n solicitada.
				</p>
				<p class="small-text c-red-1">Puedes completar solamente los campos obligatorios para crear tu cuenta (*)
				</p>
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-label" for="<portlet:namespace/>firstName">Nombre*:</label>
								<input class="form-field" id="<portlet:namespace/>firstName" name="<portlet:namespace/>firstName" required="" type="text" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-label" for="<portlet:namespace/>lastName">Apellidos*:</label>
								<input class="form-field" id="<portlet:namespace/>lastName" name="<portlet:namespace/>lastName" required="" type="text"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group noFocus focused">
								<label class="form-label" for="<portlet:namespace/>email">Correo electr&oacute;nico*:</label>
								<input class="form-field" id="<portlet:namespace/>email" name="<portlet:namespace/>email" value="" type="mail" onblur="validarEmail(this.value);" required="" />
							</div>
						</div>
					</div>
					<label class="small-text mt-3">Documento de identificación</label>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-select small-text" for="<portlet:namespace/>tdoc" id="<portlet:namespace/>tdoc"	name="<portlet:namespace/>tdoc"	onchange="obtenerTipo(this.value);">
									<option value="RUT">RUT</option>
									<option value="Pasaporte">Pasaporte</option>
								</select>
								<input class="form-field" id="<portlet:namespace/>inputTipoDoc"	name="<portlet:namespace/>inputTipoDoc" type="hidden" value=""
									required="" readonly="readonly">
							</div>
							<span id="errorTipoDoc" class="fs-1 c-red-1 d-none">Debes seleccionar un tipo de documento</span>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-label" for="<portlet:namespace/>ndoc">N&uacute;mero de documento*:</label> 
								<input class="form-field d-none" id="<portlet:namespace/>pasaporte" onchange="validNumDoc(this.value);" name="<portlet:namespace/>pasaporte" required="" type="text" maxlength="20" />	
								<input class="form-field" id="<portlet:namespace/>ndoc" onchange="validNumDoc(this.value);" name="<portlet:namespace/>ndoc" required="" type="text" maxlength="20" />
							</div>
						</div>
					</div>
				</div>
				<label class="small-text mt-3">Fecha de nacimiento</label>
				<div class="content">
					<div class="row">
						<div class="col-sm-4 col-md-12 col-lg-4">
							<div class="form-group">
								<select class="form-select small-text" id="select_day" name="<portlet:namespace/>dia" onchange="validDay(this.value);">
									<option value="">D&iacute;a</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-12 col-lg-4">
							<div class="form-group">
								<select class="form-select small-text" id="select_month" name="<portlet:namespace/>mes" onchange="validMonth(this.value);">
									<option value="0">Mes</option>
									<option value="01">Enero</option>
									<option value="02">Febrero</option>
									<option value="03">Marzo</option>
									<option value="04">Abril</option>
									<option value="05">Mayo</option>
									<option value="06">Junio</option>
									<option value="07">Julio</option>
									<option value="08">Agosto</option>
									<option value="09">Septiembre</option>
									<option value="10">Octubre</option>
									<option value="11">Noviembre</option>
									<option value="12">Diciembre</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-12 col-lg-4">
							<div class="form-group">
								<select class="form-select small-text" id="select_year" name="<portlet:namespace/>anno" onchange="validYear(this.value);">
									<option value="0">A&ntilde;o</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group codigo-tel noFocus focused">
					<label class="form-label" for="<portlet:namespace/>telephone" >Telefono*:</label>
					<input class="form-field" id="<portlet:namespace/>telephone" name="<portlet:namespace/>telephone" type="text" value="" onchange="validatePhone(this.value);" maxlength="9" required="">
					<span class="code-simulate">+56</span>
				</div>
				<div class="small-text mt-3 c-grey-1">
					Elige una contrase&ntilde;a (debe contener letras may&uacute;sculas, min&uacute;sculas y n&uacute;meros)*
					<a tabindex="0"	class="cursor-pointer example-popover d-inline-block clean-link c-blue-4-60p" role="button" data-placement="top" data-trigger="focus" title="Contrase&ntilde;a" data-content="Debe contener como m&iacute;nimo 8 caracteres, 1 letra may&uacute;scula, 1 letra min&uacute;scula, 1 numero">
					<i class="icon-exclamation-sign fs-4 d-inline-block"></i>
					</a>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="input-group-pw form-group input-group mb-3 noFocus focused">
								<label class="form-label " for="<portlet:namespace/>password">Escriba una contrase&ntilde;a</label> 
								<input type="password" class="form-control form-field" aria-label="Contrasena" aria-describedby="button-addon2" id="<portlet:namespace/>password" name="<portlet:namespace/>password" onchange="validatePass(this.value);">
								<div class="input-group-append">
									<button class="btn btn-outline-secondary" type="button" tabindex="-1"	id="button-addon2" data-show-pw="">
									<i class="la la-eye fs-10"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group mb-0 noFocus focused">
								<label class="form-label" for="portlet:namespace/>passConfirm" >Confirma tu contrase&ntilde;a</label>
								<input class="form-field" id="<portlet:namespace/>passConfirm" name="<portlet:namespace/>passConfirm" required="" onchange="validatePassIguales(this.value);" type="password"/>
							</div>
							<span id="errorClaves" class="fs-1 c-red-1 d-none">Las claves deben ser iguales</span>
						</div>
					</div>
				</div>
				<div class="text-center mt-6 mb-4">
					<button a href="#0" onclick="shows_form_part(2)" class="btn btn-corp btn-normal max-w-25 w-100 mx-auto">Siguiente</button>
				</div>
			</div>
			<div id="form_part2">
				<div class="max-w-63 w-100 mx-auto">
					<h1 class="second-title text-center mb-6 c-grey-1">
						Reg&iacute;strate en la Sucursal Virtual <span class="c-red-1">Mi Correos</span> y descubre servicios exclusivos para ti.
					</h1>
				</div>
				<div class="max-w-63 w-100 mx-auto mb-4">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<ul class="circle-list my-0">
									<li class="ok-green pb-2 c-grey-1">Pre-ingresa env&iacute;os
									</li>
									<li class="ok-green pb-2 c-grey-1">Paga en l&iacute;nea o en sucursal
									</li>
									<li class="ok-green pb-2 c-grey-1">Gestiona tus env&iacute;os
									</li>
								</ul>
							</div>
							<div class="col-md-6">
								<ul class="circle-list my-0">
									<li class="ok-green pb-2 c-grey-1">Guarda tus env&iacute;os favoritos</li>
									<li class="ok-green pb-2 c-grey-1">Guarda tus direcciones y m&aacute;s</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div
					class="bg-white px-5 py-8 rounded-common shadow-center max-w-95 mx-auto">
					<div class="max-w-90 w-100 mx-auto">
						<div class="wizard">
							<div class="wizard-graph-row">
								<div class="wizard-block wizard-block-advanced">
									<span class="circle-number">1</span>
								</div>
								<div class="wizard-block wizard-block-start">
									<span class="circle-number">2</span>
								</div>
								<div class="wizard-block">
									<span class="circle-number">3</span>
								</div>
							</div>
							<div class="wizard-text-row">
								<div class="wizard-block">
									Datos<br> Personales
								</div>
								<div class="wizard-block">T&eacute;rminos y Cond.</div>
								<div class="wizard-block">
									Registro<br> Completo
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaFormularioErrorTer" style="display: none;">
					<p class="mb-0 d-inline-block small-text f-common-regular c-white">
						<strong>Tienes un error en el formulario</strong>
					</p>
					<button type="button" class="close" aria-label="Close">
					<i class="la la-times c-white fs-7" onclick="ocultarAlertaFormularioTermError();">&nbsp;</i>
					</button>
				</div>
				<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaCaptcha" style="display: none;">
					<p class="mb-0 d-inline-block small-text f-common-regular c-white">
						<strong>&iquest;Eres un robot? tienes que validar el captcha antes de continuar</strong>
					</p>
					<button type="button" class="close" aria-label="Close">
					<i class="la la-times c-white fs-7" onclick="ocultarAlertaCaptcha();">&nbsp;</i>
					</button>
				</div>
				<div  class="max-w-63 w-100 mx-auto">
					<hr class="bb-dashed">
					<h2 class="basic-text f-common-bold c-grey-1">Informaci&oacute;n sobre tu direcci&oacute;n	</h2>
					<p class="small-text mb-4 c-grey-1">Completa los datos de tu direcci&oacute;n. 
					</p>
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
									<div class="form-group">
										<label class="form-label" for="<portlet:namespace/>comunas">Comuna*:</label>
										 <input class="form-field text-capitalize" id="<portlet:namespace/>comunas" name="<portlet:namespace/>comunas" maxlength="30" value="${comuna}" onchange="obtenerComuna(this.value);" required="" type="text" />
									</div>
									<input type="hidden" id="<portlet:namespace/>comuna" name="<portlet:namespace/>comuna" />
								</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label" for="<portlet:namespace/>calle">Calle*:</label>
									<input class="form-field" name="<portlet:namespace/>calle" id="<portlet:namespace/>calle" onblur="validCalle(this.value);" type="text" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label" for="<portlet:namespace/>numero">Número*:</label>
									<input class="form-field" name="<portlet:namespace/>numero" id="<portlet:namespace/>numero" required=""  onchange="generarCodPostal(this.value);" type="text" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label" for="<portlet:namespace/>deptoCasa">Depto/Casa:</label>
									<input class="form-field" name="<portlet:namespace/>deptoCasa" id="<portlet:namespace/>deptoCasa" required="" onblur="deleteClass();"  type="text" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group focus noFocus">
									<label class="form-label" for="<portlet:namespace/>codPostal">C&oacute;digo postal:</label>
									<input class="form-field" name="<portlet:namespace/>codPostal" id="<portlet:namespace/>codPostal" required="" readonly="readonly" onblur="deleteClass();" type="text" />
								</div>
								<div id="<portlet:namespace/>loading-registro"class="loading-absolute" style="display: none;">
									<div class="loading-animation"></div>
								</div>
								<span id="errorCodPostal" class="fs-1 c-red-1 d-none">Tienes un error en tu direcci&oacute;n</span>
									<input class="form-field" id="<portlet:namespace/>latitud"	name="<portlet:namespace/>latitud" type="hidden" value="">
									<input class="form-field" id="<portlet:namespace/>longitud"	name="<portlet:namespace/>longitud" type="hidden" value="">
									<input class="form-field" id="<portlet:namespace/>idCodComunal"	name="<portlet:namespace/>idCodComunal" type="hidden" value="">
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="form-label" for="referencia">Referencia:</label>
									<input class="form-field" name="<portlet:namespace/>referencia" id="<portlet:namespace/>referencia" required="" onblur="deleteClass();" type="text" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p class="small-text">Para terminar tu registro tienes que aceptar los t&eacute;rminos y condiciones</p>
								<div class="bg-red-3 p-1 rounded-common mb-4 d-none" id="alertAccept">
									<p class="small-text mb-0 c-white text-center">Debes aceptar los t&eacute;rminos y condiciones del servicio para crear tu cuenta
									</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="g-recaptcha" data-sitekey="${keyCapcha}"></div>
							</div>
							<div class="col-md-6">
								<div class="d-flex align-items-center">
									<div>
										<div class="form-group mb-0">
											<input class="form-switch" id="reciveInfo" name="reciveInfo" type="checkbox"
												> <label class="form-switch-label ml-0"
												for="reciveInfo"></label>
										</div>
									</div>
									<div>
										<p class="small-text c-grey-1 mb-0">Acepto recibir comunicación comercial de CorreosChile
										</p>
									</div>
								</div>
								<div class="d-flex align-items-center">
									<div>
										<div class="form-group mb-0">
											<input class="form-switch" id="accept" onchange="validarTerminos();" name="accept" type="checkbox">
											 <label class="form-switch-label ml-0" for="accept"></label>
										</div>
									</div>
									<div>
										<p class="small-text c-grey-1 mb-0"> Acepto 
											<a class="link-corp link-corp-alter" href="https://www.correos.cl/documents/130639/1289464/Terminos_y_Condiciones_entre_la_Empresa_de_Correos_de_Chile_y_los_Clientes_de_Sucursal_Virtual.pdf" target="_blank">
											T&eacute;rminos y condiciones</a>
<!-- 											<a class="link-corp link-corp-alter" onclick="showTerminos();" href="#0">T&eacute;rminos y condiciones</a> -->
										</p>
									</div>
								</div>
							</div>
						</div>
						<div id="termCond" class="row d-none">
							<div class="col-md-12">
								<div class="rounded-common bg-grey-6 p-3 border-basic">
									<div class="overflow-y-auto h-np-30 w-100 scrollbar-corp pr-2">
										<p class="small-text c-grey-1">Condiciones del servicio</p>
										<p class="small-text c-grey-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor velit sapien, lobortis luctus arcu blandit nec. Suspendisse vulputate magna at quam dignissim, ut tristique turpis euismod. Nullam ac mi mollis, efficitur purus a, convallis ex. Etiam id rutrum tellus, ac vestibulum augue.</p>
										<p class="small-text c-grey-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor velit sapien, lobortis luctus arcu blandit nec. Suspendisse vulputate magna at quam dignissim, ut tristique turpis euismod. Nullam ac mi mollis, efficitur purus a, convallis ex. Etiam id rutrum tellus, ac vestibulum augue.</p>
										<p class="small-text c-grey-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor velit sapien, lobortis luctus arcu blandit nec. Suspendisse vulputate magna at quam dignissim, ut tristique turpis euismod. Nullam ac mi mollis, efficitur purus a, convallis ex. Etiam id rutrum tellus, ac vestibulum augue.</p>
										<p class="small-text c-grey-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor velit sapien, lobortis luctus arcu blandit nec. Suspendisse vulputate magna at quam dignissim, ut tristique turpis euismod. Nullam ac mi mollis, efficitur purus a, convallis ex. Etiam id rutrum tellus, ac vestibulum augue.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="text-center mt-6 mb-4">
					<a data-btn-crear-cta="" class="btn btn-corp btn-normal max-w-27 w-100" >Crear cuenta</a>
				</div>
				<div class="text-center mt-6 mb-4">
					<a  onclick="shows_form_part(1)" class="link-corp link-corp-basic" href="#0">Volver</a>
				</div>
			</div>
		</div>
	</form>
	
	<form id="<portlet:namespace/>formCodPostal" method="post">
		<input class="form-field" id="<portlet:namespace/>numInput"	name="<portlet:namespace/>numInput" type="hidden" value="">
		<input class="form-field" id="<portlet:namespace/>calleInput"	name="<portlet:namespace/>calleInput" type="hidden" value="">
		<input class="form-field" id="<portlet:namespace/>inputComuna"	name="<portlet:namespace/>inputComuna" type="hidden" value="">
	</form>
	
	<form id="<portlet:namespace/>formCorreo" method="post">
		<input class="form-field" id="<portlet:namespace/>correoInput"	name="<portlet:namespace/>correoInput" type="hidden" value="">
	</form>
	
	<form id="<portlet:namespace/>formRegion" method="post">
		<input class="form-field" id="<portlet:namespace/>regionInput"	name="<portlet:namespace/>regionInput" type="hidden" value="">
	</form>
	
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>

function validarTerminos() {
	if ($("#accept").prop('checked')) {
		$("#alertAccept").addClass("d-none");
	} else {
		$("#alertAccept").removeClass("d-none");
	}

}

function validCodPostal(value) {
	if (value == "") {
		setTimeout(function () {
			codPostal.addClass("is-invalid");
			codPostal.removeClass("is-valid");
		}, 100);
	}
}

function validCalle(value) {

	if (value == "") {
		setTimeout(function () {
			calle.addClass("is-invalid");
			calle.removeClass("is-valid");
		}, 100);
	}
}

//obtener codigo postal
var numInput = $('#<portlet:namespace/>numInput');
var calleInput = $('#<portlet:namespace/>calleInput');
var inputComuna = $('#<portlet:namespace/>inputComuna');
var formFormularioPostal = $('#<portlet:namespace/>formCodPostal');
var longitud = $('#<portlet:namespace/>longitud');
var latitud = $('#<portlet:namespace/>latitud');
var codPostal = $("#<portlet:namespace/>codPostal");
var loadingregistro = $('#<portlet:namespace/>loading-registro');
var idCodComunal = $('#<portlet:namespace/>idCodComunal'); 

shows_form_part(1)

var formFormulario = $('#<portlet:namespace/>formFormulario');
var datosUsuario = '${datosUsuario}';

var firstName = $("#<portlet:namespace/>firstName");
var lastName = $("#<portlet:namespace/>lastName");
var telephone = $("#<portlet:namespace/>telephone");
var emailInput = $("#<portlet:namespace/>email");
var tipoDoc = $('#<portlet:namespace/>inputTipoDoc');
tipoDoc.val("RUT"); //Valor por defecto de seleccion
var tdoc = $('#<portlet:namespace/>tdoc');
var ndoc = $("#<portlet:namespace/>ndoc");
var pasaporte =  $("#<portlet:namespace/>pasaporte");
var years = $("#select_year");
var month = $("#select_month");
var dia = $("#select_day");
var monthValid = null;
var dayValid = null;
var monthNumber = null;
var dayNumber = null;
var yearNumber = null;
var password = $("#<portlet:namespace/>password");
var passConfirm = $("#<portlet:namespace/>passConfirm");
var errorClaves = $("#errorClaves");
var alertaFormularioError = $("#<portlet:namespace/>alertaFormularioError");
var alertaFechaError = $("#<portlet:namespace/>alertaFechaError");
var pasaporteValid = false;
var codPostalValido = false;
var passValid = false;
//variable direcciones

var regiones = $("#<portlet:namespace/>regiones");
var region = $("#<portlet:namespace/>region");
var comunas = $("#<portlet:namespace/>comunas");
var comuna = $("#<portlet:namespace/>comuna");
var calle = $("#<portlet:namespace/>calle");
var deptoCasa = $("#<portlet:namespace/>deptoCasa");
var numero = $("#<portlet:namespace/>numero");

var referencia = $("#<portlet:namespace/>referencia");
var result = '${result}';
var accept = '${accept}';
var reciveInfo = "${reciveInfo}";
var validTelefono = false;
var validEmail = false;
var alertaFormulario = $('#<portlet:namespace/>alertaFormulario');
var exitoFormulario = $('#<portlet:namespace/>exitoFormulario');
var validApellido = false;
var alertaFormularioErrorTer = $('#<portlet:namespace/>alertaFormularioErrorTer');
var alertaCaptcha = $('#<portlet:namespace/>alertaCaptcha');


var correoInput = $('#<portlet:namespace/>correoInput');
var formCorreo = $('#<portlet:namespace/>formCorreo');
var correoExiste = false;
var alertaFormularioErrorCorreo = $('#<portlet:namespace/>alertaFormularioErrorCorreo');


var regionInput = $('#<portlet:namespace/>regionInput');
var formRegion = $('#<portlet:namespace/>formRegion');
var actionRegion = '${actionRegion}';
var resultComunas = null;

var today = new Date();

function obtenerRegion(val) {
	region.val(val);
	regionInput.val(val);

	if (regionInput.val() != "") {
		formRegion.submit();
	}
}


formRegion.submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'post',
		url: actionRegion,
		data: formRegion.serialize(),
		datatype: 'json',
		cache: false,
		error: function (xhr, status, error) {},
		success: function (data) {
			var jsonData = data[0];
			var jsonDataFormat = jsonData;
			resultComunas = jsonDataFormat;
			listComunas();
		}
	});
});

function ocultarAlertaFormularioErrorCorreo() {
	alertaFormularioErrorCorreo.hide();
}

function ocultarAlertaFechaError() {
	alertaFechaError.hide();
}


function obtenerComuna(val) {

	comunas.val(val);
	comuna.val(val);
	calle.removeAttr("readonly");
	numero.removeAttr("readonly");
	calle.val("");
	calle.removeClass("is-valid");
	numero.val("");
	numero.removeClass("is-valid");
	deptoCasa.val("");
	deptoCasa.removeClass("is-valid");
	referencia.val("");
	referencia.removeClass("is-valid");
	codPostal.val("");
	codPostal.removeClass("is-valid");

}


formCorreo.submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'post',
		url: '${validCorreo}',
		data: formCorreo.serialize(),
		datatype: 'json',
		cache: false,
		error: function (xhr, status, error) {},
		success: function (data) {
			var jsonData = data[0];
			var jsonDataFormat = jsonData;
			correoExiste = jsonDataFormat.existe;
			validUsuario(correoExiste);
		}
	});
});

function validUsuario(val) {
	if (val) {
		validEmail = false;
		alertaFormularioErrorCorreo.show();
		setTimeout(function () {
			emailInput.addClass("is-invalid");
			emailInput.removeClass("is-valid");
		}, 200);
	} else {
		alertaFormularioErrorCorreo.hide();
		setTimeout(function () {
			validEmail = true;
			emailInput.addClass("is-valid");
			emailInput.removeClass("is-invalid");
		}, 200);

	}
}


function codPostalValid() {
	if (codPostal.val() == 0) {
		codPostalValido = false,
			$("#errorCodPostal").removeClass("d-none");
		calle.val("");
		calle.removeClass("is-valid");
		calle.addClass("is-invalid");
		numero.val("");
		numero.removeClass("is-valid");
		numero.addClass("is-invalid");
		codPostal.addClass("is-invalid");

		loadingregistro.hide();
	} else {
		codPostal.addClass("is-valid");
		codPostal.removeClass("is-invalid");
		$("#errorCodPostal").addClass("d-none");
		calle.attr("readonly", "readonly");
		numero.attr("readonly", "readonly");
		loadingregistro.hide();
		codPostalValido = true;
	}
}


function scrollToAnchorForm(anchor) {
	var selector = $(anchor);
	var offset = selector.offset().top;
	$('html, body').animate({
		scrollTop: offset
	}, 'slow');
};

function generarCodPostal(val) {

	if (val == "") {
		numero.removeClass("is-valid");
		numero.addClass("is-invalid");
	} else {
		inputComuna.val(comuna.val());
		calleInput.val(calle.val());
		numInput.val(val);
		formFormularioPostal.submit();
	}

}


function deleteClass() {
	if (referencia.val() == "") {
		setTimeout(function () {
			referencia.removeClass("is-invalid");
			referencia.removeClass("is-valid");
		}, 100);
	}

	if (codPostal.val() == "") {

		setTimeout(function () {
			codPostal.removeClass("is-invalid");
			codPostal.removeClass("is-valid");
		}, 100);
	}

	if (deptoCasa.val() == "") {
		setTimeout(function () {
			deptoCasa.removeClass("is-invalid");
			deptoCasa.removeClass("is-valid");
		}, 100);
	}
}

formFormularioPostal.submit(function (event) {
	event.preventDefault();
	loadingregistro.show();
	$.ajax({
		type: 'post',
		url: '${obtenerCP}',
		data: formFormularioPostal.serialize(),
		datatype: 'json',
		cache: false,
		error: function (xhr, status, error) {},
		success: function (data) {
			var jsonData = data[0];
			var jsonDataFormat = jsonData;
			codPostal.val(jsonDataFormat.codigoPostal);
			latitud.val(jsonDataFormat.latitud);
			idCodComunal.val(jsonDataFormat.idCodComunal);
			longitud.val(jsonDataFormat.longitud);
			calle.val(jsonDataFormat.calleWs);

			codPostalValid();
		}
	});
});


	function obtenerTipo(valor) {
		$("#errorTipoDoc").addClass("d-none");
		tipoDoc.val(valor);
		ndoc.val('');

		if (tipoDoc.val() != "") {
			$("#errorTipoDoc").addClass("d-none");
			if (tipoDoc.val() == "RUT") {
				pasaporte.addClass("d-none");
				pasaporte.val("");
				ndoc.removeClass("d-none");
				setTimeout(function() {
					ndoc.removeClass("is-valid");
					ndoc.removeClass("is-invalid");
				}, 100);
			} else {
				ndoc.addClass("d-none");
				ndoc.val("");
				pasaporte.removeClass("d-none");
				setTimeout(function() {
					pasaporte.removeClass("is-valid");
					pasaporte.removeClass("is-invalid");
				}, 100);
			}
		} else {
			$("#errorTipoDoc").removeClass("d-none");
			setTimeout(function() {
				tipoDoc.removeClass("is-valid");
				tipoDoc.addClass("is-invalid");
				
			}, 100);
		}

	}

	function showTerminos() {
		$("#termCond").removeClass("d-none");
	}

	function validatePhone(phone) {
		telephone.val(phone);

		if (telephone.val() != "") {
			if (telephone.val().length == 9) {
				validTelefono = true;
				telephone.removeClass("is-invalid");
				telephone.addClass("is-valid");
			} else {
				setTimeout(function() {
					validTelefono = false;
					telephone.addClass("is-invalid");
					telephone.removeClass("is-valid");
				}, 100);
			}
		} else {
			validTelefono = false;
			telephone.addClass("is-invalid");
			telephone.removeClass("is-valid");
		}

	}

	Liferay.on('allPortletsReady', function() {
		showPass("#button-addon2", "#<portlet:namespace/>password");
	});

	function ocultarAlertaFormularioError() {
		alertaFormularioError.hide();
	}

	function ocultarAlertaFormularioTermError() {
		alertaFormularioErrorTer.hide();
	}
	
	function ocultarAlertaCaptcha() {
		alertaCaptcha.hide();
	}

	function validNumDoc(num) {
		if (tipoDoc.val() != "") {
			$("#errorTipoDoc").addClass("d-none");
			ndoc.val(num);
			if (tipoDoc.val() == "RUT") {

				ndoc.val(formatIdRut(ndoc.val()));
				if (!validRut(ndoc.val())) {
					setTimeout(function() {
						ndoc.removeClass("is-valid");
						ndoc.addClass("is-invalid");
					}, 100);
				}
			} else {
				pasaporte.val(num);
				if (pasaporte.val().length > 3) {
					setTimeout(function() {
						pasaporteValid = true;
						pasaporte.addClass("is-valid");
						pasaporte.removeClass("is-invalid");
					}, 100);
				} else {
					pasaporteValid = false;
					setTimeout(function() {
						pasaporte.removeClass("is-valid");
						pasaporte.addClass("is-invalid");
					}, 100);
				}
			}
		} else {
			$("#errorTipoDoc").removeClass("d-none");
			setTimeout(function() {
				ndoc.removeClass("is-valid");
				ndoc.addClass("is-invalid");
			}, 100);
		}

	}
	
	function validDay(num) {
		var todayDay = today.getDate();
		var number = parseInt(num);
		dayNumber = number;
		if (number >= todayDay) {
			if (yearNumber != null) {
				if (monthNumber != null) {
					dia.removeClass("is-valid");
					dia.addClass("is-invalid");
					dayValid = false;
				} else {
					dia.removeClass("is-invalid");
					dia.addClass("is-valid");
					dayValid = true;
				}
			} else {
				dia.removeClass("is-invalid");
				dia.addClass("is-valid");
				dayValid = true;
			}
		} else if (monthValid == false) {
			console.log(monthValid);
			dia.removeClass("is-valid");
			dia.addClass("is-invalid");
			dayValid = false;
		} else {
			dia.removeClass("is-invalid");
			dia.addClass("is-valid");
			dayValid = true;
		}
	}
	
	dia.blur(function() {
		setTimeout(function () {
			if (dayValid) {
				dia.removeClass("is-invalid");
				dia.addClass("is-valid");
			} else {
				dia.removeClass("is-valid");
				dia.addClass("is-invalid");
			}
		}, 50);
	});
	
	function validCalle(value) {
		if (value == "") {
			setTimeout(function () {
				calle.addClass("is-invalid");
				calle.removeClass("is-valid");
			}, 100);
		}
	}
	
	function validMonth(num) {
		var todayMonth = today.getMonth() + 1;
		var todayYear = today.getFullYear();
		var number = parseInt(num);
		monthNumber = number;
		if (yearNumber != null) {
			if (number > todayMonth && yearNumber == todayYear) {
				month.removeClass("is-valid");
				month.addClass("is-invalid");
				monthValid = false;
				dia.removeClass("is-valid");
				dia.addClass("is-invalid");
				dayValid = false;
			} else if (number == todayMonth) {
				var todayDay = today.getDate();
				month.removeClass("is-invalid");
				month.addClass("is-valid");
				monthValid = true;
				if (dayNumber >= todayDay) {
					dia.removeClass("is-valid");
					dia.addClass("is-invalid");
					dayValid = false;
				} else {
					dia.removeClass("is-invalid");
					dia.addClass("is-valid");
					dayValid = true;
				}
			} else {
				dia.removeClass("is-invalid");
				dia.addClass("is-valid");
				dayValid = true;
				month.removeClass("is-invalid");
				month.addClass("is-valid");
				monthValid = true;
			}
			if (añoBisiesto(yearNumber) && monthNumber == 2) { //Febrero
				clearDays();
				addDays(30); //Por rango del for loop se agrega uno mas
				recoverDay();
			} else {
				calculateDays();
			}
		} else {
			calculateDays();
			monthValid = true;
		}
	}
	
	month.blur(function() {
		setTimeout(function () {
			if (monthValid) {
				month.removeClass("is-invalid");
				month.addClass("is-valid");
			} else {
				month.removeClass("is-valid");
				month.addClass("is-invalid");
			}
		}, 50);
	});
	
	function validYear(num) {
		var todayYear = today.getFullYear();
		var number = parseInt(num);
		yearNumber = number;
		if (yearNumber < todayYear &&  (monthNumber != null || dayNumber != null)) {
			dia.removeClass("is-invalid");
			dia.addClass("is-valid");
			dayValid = true;
			month.removeClass("is-invalid");
			month.addClass("is-valid");
			monthValid = true;
		} else if (monthNumber != null || dayNumber != null) {
			var todayMonth = today.getMonth() + 1;
			if (monthNumber > todayMonth) {
				month.removeClass("is-valid");
				month.addClass("is-invalid");
				monthValid = false;
				dia.removeClass("is-valid");
				dia.addClass("is-invalid");
				dayValid = false;
			} else if (monthNumber == todayMonth) {
				var todayDay = today.getDate();
				month.removeClass("is-invalid");
				month.addClass("is-valid");
				monthValid = true;
				if (dayNumber >= todayDay) {
					dia.removeClass("is-valid");
					dia.addClass("is-invalid");
					dayValid = false;
				} else {
					dia.removeClass("is-invalid");
					dia.addClass("is-valid");
					dayValid = true;
				}
			} else {
				dia.removeClass("is-invalid");
				dia.addClass("is-valid");
				dayValid = true;
				month.removeClass("is-invalid");
				month.addClass("is-valid");
				monthValid = true;
			}
		}
		if (añoBisiesto(yearNumber) && monthNumber == 2) { //Febrero
			clearDays();
			addDays(30); //Por rango del for loop se agrega uno mas
			recoverDay();
		} else if (monthNumber == 2) {
			clearDays();
			addDays(29);
			recoverDay();
		}
	}
	
	function añoBisiesto(año)
	{
	  return ((año % 4 == 0) && (año % 100 != 0)) || (año % 400 == 0);
	}
	
	function clearDays() {
		var select = document.getElementById("select_day");
		var length = select.options.length;
		for (i = length-1; i >= 0; i--) {
			select.options[i] = null;
		}
	}
	
	function addDays(number) {
		var $select_day = $("#select_day");
		$('<option>').val('0').text('Día').appendTo($select_day);
		for (var i = 1; i < number; i++) {
			var day_number = i;
			$('<option>').val(('0' + day_number).slice(-2)) // set the value
			.text(i) // set the text in in the <option>
			.appendTo($select_day);
		}
	}
	
	function recoverDay() {
		if (dayNumber != null && dayNumber != "") {
			if (dayNumber < 10) {
				document.getElementById('select_day').value = "0" + dayNumber;
			} else {
				document.getElementById('select_day').value = dayNumber;
			}
		}
	}
		
	function validarFormularioDatosPersonales() {

		if (passConfirm.val() == "") {
			return false;
		}

		if (emailInput.val() == "") {
			return false;
		}

		if (ndoc.val() == "") {
			return false;
		}

		if (tdoc.val() == "") {
			return false;
		}

		if (!validEmail) {
			return false;
		}

		if (password.val() == "") {
			return false;
		}
		if (firstName.val() == "") {
			return false;
		}
		if (telephone.val() == "") {
			return false;
		}
		if (lastName.val() == "") {
			return false;
		}

		if (!passValid) {
			return false;
		}
		if (tipoDoc.val() == "RUT") {
			if (!validRut(ndoc.val())) {
				return false;
			}
		} else {
			if (!pasaporteValid) {
				return false;
			}
		}

		if (!validTelefono) {
			return false;
		}
		console.log(dayNumber);
		console.log(monthNumber);
		console.log(yearNumber);
		
		if ((dayNumber == null || dayNumber == "") && (monthNumber != null || yearNumber != null)) {
			if (monthNumber != "" || yearNumber != "") {
				dia.removeClass("is-valid");
				dia.addClass("is-invalid");
				return false;
			}
		}
		
		if ((monthNumber == null || monthNumber == "") && (dayNumber != null || yearNumber != null)) {
			if (dayNumber != "" || yearNumber != "") {
				month.removeClass("is-valid");
				month.addClass("is-invalid");
				return false;
			}
		}
		
		if ((yearNumber == null || yearNumber == "") && (monthNumber != null || dayNumber != null)) {
			if (monthNumber != "" || dayNumber != "") {
				years.removeClass("is-valid");
				years.addClass("is-invalid");
				return false;
			}
		}
		
		return true;
	}
	
	function fechaValida() {
		if (!dayValid && (dayNumber != null || monthNumber != null || yearNumber != null) && (dayNumber != "" || monthNumber != "" || yearNumber != "")) {
			return false;
		}
		
		if (!monthValid && (dayNumber != null || monthNumber != null || yearNumber != null) && (dayNumber != "" || monthNumber != "" || yearNumber != "")) {
			return false;
		}
		return true;
	}

	function enviarFormulario() {
		var $label = $("#accept").next("label");
		if (!$("#accept").is(":checked")) {
			$label.addClass("form-switch-label-tip");
			$("#alertAccept").removeClass("d-none");
			setTimeout(function() {
				$label.removeClass("form-switch-label-tip");
			}, 1005);
		} else {
			if (validarFormularioTerminosYcondiciones()) {
				var response = grecaptcha.getResponse();
				alertaFormularioErrorTer.hide();

				if (response.length == 0) {
					alertaCaptcha.show();
					scrollToAnchorForm(alertaFormularioErrorTer);
				} else {
					alertaCaptcha.hide();
					formFormulario.submit();
				}

			} else {
				alertaFormularioErrorTer.show();
				scrollToAnchorForm(alertaFormularioErrorTer);
			}
		}
	}

	function validarFormularioTerminosYcondiciones() {

		/* if (regiones.val() == "") {
			return false;
		} */

		if (calle.val() == "") {
			return false;
		}

		if (numero.val() == "") {
			return false;
		}

		if (comunas.val() == "") {
			return false;
		}
		if (!codPostalValido) {
			return false;
		}

		return true;
	}

	formFormulario.submit(function(event) {
		event.preventDefault();

		//aqui va la funcion para validar el primer fomulario validacionForm2()	
		if (validarFormularioTerminosYcondiciones()) {
			$.ajax({
				type : 'post',
				url : datosUsuario,
				data : formFormulario.serialize(),
				datatype : 'json',
				cache : false,
				error : function(xhr, status, error) {

				},
				success : function(data) {
					var jsonData = data[0];

					if (jsonData.status != "fail") {
						$("#regristro").replaceWith(data);
					} else {
						var messageData = data[1];
					}

				}
			});
		} else {
			alertaFormularioErrorTer.show();
			scrollToAnchorForm(alertaFormularioErrorTer);
		}
	});

	function validatePass(val) {
		var m = val;
		var expreg = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

		if (expreg.test(m)) {
			password.addClass("is-valid");
			password.removeClass("is-invalid");
			password.val(m);
			passValid = true;
		} else {
			setTimeout(function() {
				password.removeClass("is-valid");
				password.addClass("is-invalid");
			}, 100);
			passValid = false;
		}
	}

	function validatePassIguales(val) {
		if (password.val() != val) {
			setTimeout(function() {
				passConfirm.removeClass("is-valid");
				passConfirm.addClass("is-invalid");
			}, 100);
			errorClaves.removeClass("d-none");
			passValid = false;
		} else {
			errorClaves.addClass("d-none");
			passConfirm.val(val);
			passValid = true;
		}
	}

	function validarEmail(email) {
		emailInput.val(email.toLowerCase());
		if (emailInput.val() != "") {
			if (validateEmail(emailInput.val())) {
				validEmail = true;
				emailInput.removeClass("is-invalid");
				emailInput.addClass("is-valid");
				correoInput.val(emailInput.val());
				formCorreo.submit();
			} else {
				setTimeout(function() {
					validEmail = false;
					emailInput.addClass("is-invalid");
					emailInput.removeClass("is-valid");
				}, 100);

			}
		} else {
			validEmail = false;
			emailInput.addClass("is-invalid");
			emailInput.removeClass("is-valid");
		}
	}

	function validateEmail(email) {
		var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		return re.test(email);
	}

	$(function() {
		$('.example-popover')
				.popover(
						{
							container : 'body',
							template : '<div class="popover popover-corp" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
						})
	});

	var $select_day = $("#select_day");
	for (var i = 1; i < 32; i++) {
		var day_number = i;
		$('<option>').val(('0' + day_number).slice(-2)) // set the value
		.text(i) // set the text in in the <option>
		.appendTo($select_day);
	}
	// :: YEAR
	var $select_year = $("#select_year");

	// Get the current year
	var year = new Date().getFullYear();
	var $select_year = $('#select_year');

	for (var i = 0; i < 99; i++) {
		$('<option>').val(year - i) // set the value
		.text(year - i) // set the text in in the <option>
		.appendTo($select_year);
	}

	function formatIdRut(rut) {

		var actual = rut.replace(/^0+/, "");
		if (actual != '' && actual.length > 1) {
			var sinPuntos = actual.replace(/\./g, "");
			var actualLimpio = sinPuntos.replace(/-/g, "");
			var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
			var rutPuntos = "";
			var i = 0;
			var j = 1;
			for (i = inicio.length - 1; i >= 0; i--) {
				var letra = inicio.charAt(i);
				rutPuntos = letra + rutPuntos;
				if (j % 3 == 0 && j <= inicio.length - 1) {
					rutPuntos = "." + rutPuntos;
				}
				j++;
			}
			var dv = actualLimpio.substring(actualLimpio.length - 1);
			rutPuntos = rutPuntos + "-" + dv;
			rutPuntos = rutPuntos.toUpperCase();
		}
		return rutPuntos;

	}

	function validRut(rut) {
		var validRut = false;
		var dv = rut.split("-")[1];
		var rut = rut.split("-")[0].replace(".", "").replace(".", "");

		var rutReversa = rut.split("").reverse();

		var sum = 0;
		var multiplo = 2;

		$.each(rutReversa, function(index, val) {

			sum += val * multiplo;
			if (multiplo == 7) {
				multiplo = 1;
			}
			multiplo++;
		})

		sum = sum % 11;
		sum = 11 - sum;

		if (sum == 10) {
			sum = "K";
		} else if (sum == 11) {
			sum = "0";
		}

		if (dv == sum) {
			validRut = true;
		}

		return validRut;
	}

	function isBlank(str) {
		return (!str || /^\s*$/.test(str));
	}

	function shows_form_part(n) {
		if (n === 2) {
			//aqui va la funcion para validar el primer fomulario validacionForm1()
			if (validarFormularioDatosPersonales()) {
				if (fechaValida()) {
					var i = 1, p = document.getElementById("form_part" + 1);
					while (p !== null) {
						if (i === n) {
							p.style.display = "";
						} else {
							p.style.display = "none";
						}
						i++;
						p = document.getElementById("form_part" + i);
					}
				} else {
					alertaFechaError.show();
					scrollToAnchorForm(alertaFechaError);
				}
			} else {
				alertaFormularioError.show();
				scrollToAnchorForm(alertaFormularioError);
			}
		} else {
			var i = 1, p = document.getElementById("form_part" + 1);
			while (p !== null) {
				if (i === n) {
					p.style.display = "";
				} else {
					p.style.display = "none";
				}
				i++;
				p = document.getElementById("form_part" + i);

			}
		}
	}

	var jsonParser = null;
	var data = null;
	var data1 = null;

	function listComunas() {
		data2 = resultComunas;

		var options = {
			data : data2.listComuna,
			getValue : "comuna",
			list : {
				maxNumberOfElements : 4,
				showAnimation : {
					type : "fade",
					time : 400,
					callback : function() {
					}
				},
				hideAnimation : {
					type : "slide",
					time : 400,
					callback : function() {
					}
				},
				sort : {
					enabled : true
				},
				match : {
					enabled : true
				},
			},
			template : {
				type : "custom",
				method : function(value, item) {
					return "<span class='fs-1'>Región " + item.region
							+ "</span> <br/>" + value;
				}
			}
		};
		$("#<portlet:namespace/>comunas").easyAutocomplete(options);
	}

	$(document)
			.ready(
					function() {

						$("[data-btn-crear-cta]").click(function(e) {
							e.preventDefault();
							enviarFormulario();
						});

						jsonParser = JSON.parse(result);
						data = jsonParser[0];
						var options = {
							data : data.comuna,
							getValue : "comuna",
							list : {
								maxNumberOfElements : 4,
								showAnimation : {
									type : "fade",
									time : 400,
									callback : function() {
									}
								},
								hideAnimation : {
									type : "slide",
									time : 400,
									callback : function() {
									}
								},
								sort : {
									enabled : true
								},
								match : {
									enabled : true
								},
							},
							template : {
								type : "custom",
								method : function(value, item) {
									return "<span class='fs-1'>Región "
											+ item.region + "</span> <br/>"
											+ value;
								}
							}
						};
						$("#<portlet:namespace/>comunas").easyAutocomplete(
								options);

						$('.form-group input').focus(function() {
							$(this).parents('.form-group').addClass('focused');
						});
						$('form').find('input', 'select')
								.blur(function() {
									$(this).removeClass('is-valid is-invalid')
										.addClass(this.checkValidity() ? 'is-valid': 'is-invalid');

									var referencia = $("#<portlet:namespace/>referencia");
									if (deptoCasa.val() == ""
											|| referencia.val() == "") {
										deptoCasa
												.removeClass('is-invalid');
										referencia
												.removeClass('is-invalid');
									} else {
										deptoCasa.addClass('is-valid');
										referencia.addClass('is-valid');
									}

									if (codPostal.val() == "") {
										codPostal.removeClass('is-invalid');
									}
								});
						
						$('.form-group input').blur(
								function() {
									var inputValue = $(this).val();
									if (inputValue == "") {
										$(this).removeClass('filled');
										$(this).parents('.form-group')
												.removeClass('focused');
									} else {
										$(this).addClass('filled');
										$(this).attr("value", $(this).val());
									}
								});

						$('.form-group input').blur(
								function() {
									var inputValue = $(this).val();
									if (inputValue == "") {
										$(this).removeClass('filled');
										$(this).parents('.form-group')
												.removeClass('focused');
										$(".noFocus").addClass('focused');
									} else {
										$(this).addClass('filled');
										$(this).attr("value", $(this).val());
										$(".noFocus").addClass('focused');
									}
								});
					});

	var regexNum = /[^\d$]/g;
	$($("#<portlet:namespace/>telephone")).keyup(
			function() {
				$($("#<portlet:namespace/>telephone")).val(
						$($("#<portlet:namespace/>telephone")).val().replace(
								regexNum, ""))
			});

	var regex = /[^+A-Za-z\_\-\.\s\xF1\xD1\u00E0-\u00FC]+$/;
	$($("#<portlet:namespace/>firstName")).keyup(
			function() {
				$($("#<portlet:namespace/>firstName")).val(
						$($("#<portlet:namespace/>firstName")).val().replace(
								regex, ""))
			});

	var regex = /[^+A-Za-z\_\-\.\s\xF1\xD1\u00E0-\u00FC]+$/;
	$($("#<portlet:namespace/>lastName")).keyup(
			function() {
				$($("#<portlet:namespace/>lastName")).val(
						$($("#<portlet:namespace/>lastName")).val().replace(
								regex, ""))
			});

	var validaterut = /\b[^0-9kK-]+\b/g;
	$($("#<portlet:namespace/>ndoc")).keyup(
			function() {
				$($("#<portlet:namespace/>ndoc")).val(
						$($("#<portlet:namespace/>ndoc")).val().replace(
								validaterut, ""))
			});

	var validPasaporte = /[^a-zA-Z0-9]/;
	$($("#<portlet:namespace/>pasaporte")).keyup(
			function() {
				$($("#<portlet:namespace/>pasaporte")).val(
						$($("#<portlet:namespace/>pasaporte")).val().replace(
								validPasaporte, ""))
			});
	
	function calculateDays() {
		switch(monthNumber) {
			case 1:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 2:
				clearDays();
				addDays(29);
				recoverDay();
				break;
			case 3:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 4:
				clearDays();
				addDays(31);
				recoverDay();
				break;
			case 5:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 6:
				clearDays();
				addDays(31);
				recoverDay();
				break;
			case 7:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 8:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 9:
				clearDays();
				addDays(31);
				recoverDay();
				break;
			case 10:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			case 11:
				clearDays();
				addDays(31);
				recoverDay();
				break;
			case 12:
				clearDays();
				addDays(32);
				recoverDay();
				break;
			default:
				clearDays();
				addDays(32);
				recoverDay();
				break;
		}
	}
</script>

	
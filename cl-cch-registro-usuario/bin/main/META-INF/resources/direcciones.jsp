<%@ include file="init.jsp"%>


<div id="form_part2">


	<div class="max-w-63 w-100 mx-auto">
		<h1 class="second-title text-center mb-6 c-grey-1">
			Registrate en <span class="c-red-1">CorreosChile</span> y descubre lo
			nuevo que puedes realizar con tu cuenta.
		</h1>
	</div>
	<div class="max-w-63 w-100 mx-auto mb-4">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="circle-list my-0">
						<li class="ok-green pb-2 c-grey-1">Realiza envíos línea</li>
						<li class="ok-green pb-2 c-grey-1">Gestiona tus envíos</li>
						<li class="ok-green pb-2 c-grey-1">Sube documentos de
							internación</li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="circle-list my-0">
						<li class="ok-green pb-2 c-grey-1">Guarda direcciones</li>
						<li class="ok-green pb-2 c-grey-1">Guarda tus envíos
							favoritos</li>
						<li class="ok-green pb-2 c-grey-1">Crea tu perfil</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div
		class="bg-white px-5 py-8 rounded-common shadow-center max-w-95 mx-auto">
		<div class="max-w-90 w-100 mx-auto">
			<div class="wizard">
				<div class="wizard-graph-row">
					<div class="wizard-block wizard-block-advanced">
						<span class="circle-number">1</span>
					</div>
					<div class="wizard-block wizard-block-start">
						<span class="circle-number">2</span>
					</div>
					<div class="wizard-block">
						<span class="circle-number">3</span>
					</div>
				</div>

				<div class="wizard-text-row">
					<div class="wizard-block">
						Datos<br> Personales
					</div>
					<div class="wizard-block">Terminos y Cond.</div>
					<div class="wizard-block">
						Registro<br> Completo
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="max-w-63 w-100 mx-auto">
		<hr class="bb-dashed">
		<h2 class="basic-text f-common-bold c-grey-1">Información sobre
			tu dirección (no obligatorio)</h2>
		<p class="small-text mb-4 c-grey-1">Puedes completar tus datos de
			dirección ahora mismo o bien terminalo mas adelante, estos campos no
			son obligatorios</p>

		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<select class="form-select small-text"
							name="#<portlet:namespace/>dir_region"
							id="#<portlet:namespace/>dir_region">
							<option value="">Región</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<select class="form-select small-text"
							name="#<portlet:namespace/>comuna"
							id="#<portlet:namespace/>comuna">
							<option value="">Comuna</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-label" for="#<portlet:namespace/>dir_calle">Calle</label>
						<input class="form-field" name="#<portlet:namespace/>dir_calle"
							id="#<portlet:namespace/>dir_calle" required="" type="text" />
					</div>
				</div>

				<div class="form-group">
					<select class="form-select small-text"
						name="#<portlet:namespace/>dir_dep"
						id="#<portlet:namespace/>dir_dep">
						<option value="">Dpto/Casa</option>
						<option value="">Dpto</option>
						<option value="">Casa</option>
					</select>
				</div>


				<div class="col-md-6">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="form-label" for="#<portlet:namespace/>dir_num">Número</label>
									<input class="form-field" name="#<portlet:namespace/>dir_num"
										id="#<portlet:namespace/>dir_num" required="" type="text" />
								</div>
							</div>

							<div class="col-sm-6"></div>


						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-label" for="dir_cod_postal">Código
							postal</label> <input class="form-field"
							for="#<portlet:namespace/>dir_cod_postal"
							name="#<portlet:namespace/>dir_cod_postal"
							id="#<portlet:namespace/>dir_cod_postal" required="" type="text" />
					</div>
				</div>
			</div>
		</div>

		<div class="container">

			<div class="row">




				<div class="col-md-6">
					<div class="g-recaptcha"
						data-sitekey="6LdNcMUUAAAAAM0gVK-q5qSVV6Qt2iNsBj0CCyMN"></div>

				</div>

				<div class="col-md-6">

					<hr class="bb-dashed mb-6">
					<p class="small-text">Para terminar tu registro tienes que
						aceptar los terminos y condiciones</p>
					<div class="bg-red-3 p-1 rounded-common mb-4 d-none"
						id="alertAccept">
						<p class="small-text mb-0 c-white text-center">Debes aceptar
							los términos y condiciones del servicio para crear tu cuenta</p>
					</div>
					<div class="d-flex align-items-center">
						<div>
							<div class="form-group mb-0">
								<input class="form-switch" id="reciveInfo" type="checkbox"
									required=""> <label class="form-switch-label ml-0"
									for="reciveInfo"></label>
							</div>
						</div>
						<div>
							<p class="small-text c-grey-1 mb-0">Acepto recibir
								comunicación comercial de CorreosChile</p>
						</div>
					</div>
					<div class="d-flex align-items-center">
						<div>
							<div class="form-group mb-0">
								<input class="form-switch" id="accept" type="checkbox"
									required=""> <label class="form-switch-label ml-0"
									for="accept"></label>
							</div>
						</div>
						<div>
							<p class="small-text c-grey-1 mb-0">
								Acepto <a class="link-corp link-corp-alter" href="#0">Terminos
									y condiciones</a>
							</p>
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>


	<div class="text-center mt-6 mb-4">
		<button a href="#0" onclick="shows_form_part(1)"
			class="btn btn-corp btn-normal max-w-25 w-100 mx-auto">Volver</button>
	</div>

	<div class="text-center mt-6 mb-4">
		<button a href="#0" onclick="enviarFormulario();"
			class="btn btn-corp btn-normal max-w-25 w-100 mx-auto">Crear
			cuenta</button>
	</div>
</div>


<script>
var jsonParser = null;
var data = null;
AUI().ready(
		
		});
</script>


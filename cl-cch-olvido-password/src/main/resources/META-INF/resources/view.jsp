<%@ include file="init.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<portlet:resourceURL var="olvido" id="olvido"></portlet:resourceURL>

<section class="container-small">
	<div class="alert alert-corp-dismissible bg-red-1 text-center" role="alert" id="<portlet:namespace/>alertaFormulario" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong>Tienes un error en el formulario.</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
		<span class="c-white" onclick="ocultarAlertaFormulario();">X�</span>
		</button>
	</div>
	<div class="alert alert-corp-dismissible bg-green-1 text-center" role="alert" id="<portlet:namespace/>exitoFormulario" style="display: none;">
		<p class="mb-0 d-inline-block small-text f-common-regular c-white">
			<strong>Formulario Enviado Correctamente.</strong>
		</p>
		<button type="button" class="close" aria-label="Close">
		<span class="c-white" onclick="ocultarExitoFormulario();">X�</span>
		</button>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form id="<portlet:namespace/>formFormulario" method="post">
				<%-- <div class="row">
					<div class="col-md-4">
						<div class="row">
							
							<div class="form-group mb-4">
								<label class="form-label fs-1" for="<portlet:namespace/>email">Correo
									Electronico</label> <input class="form-field"
									id="<portlet:namespace/>email" name="<portlet:namespace/>email"
									value="" type="mail" onblur="validarEmail(this.value);"
									required="">
							</div>
						
						</div>
						<div class="row">
							<div class="col-md-6">
								<div>
									<a href="#0" onclick="enviarFormulario();" class="btn btn-corp btn-normal">Enviar</a>
								</div>
							</div>
						</div>
					</div>
				</div> --%>
				
				
				<div class="max-w-63 mx-auto">
	<h1 class="second-title text-center mb-3 c-grey-1">Recupera tu contrase�a para iniciar sesi�n</h1>
	<p class="basic-text c-grey-1 text-center mb-6">�Olvidaste tu contrase�a? Ingresa tu direcci�n de correo electr�nico registrada en los campos a continuaci�n, y te ayudaremos a restablecerla.</p>
</div>
<div class="bg-white px-5 py-8 rounded-common shadow-center min-h-desk-15 max-w-95 mx-auto">
	<div class="max-w-50 w-100 mx-auto">
		<p class="basic-text f-common-bold c-grey-1 mb-0">Ingresa tu Correo electr�nico</p>
		<p class="small-text c-red-1 mb-2">Campo obligatorio para recuperar tu contrase�a(*)</p>
		<div class="form-group"><label class="form-label" for="<portlet:namespace/>email">Correo electr�nico*</label> <input class="form-field" id="<portlet:namespace/>email" name="<portlet:namespace/>email" required="" type="email" onblur="validarEmail(this.value);"/></div>
		<div class="alert alert-corp-dismissible bg-red-1 text-center mt-6" role="alert" id="<portlet:namespace/>alertaCaptcha" style="display: none;">
			<p class="mb-0 d-inline-block small-text f-common-regular c-white">
				<strong>Tienes que validar el captcha antes de continuar</strong>
			</p>
			<button type="button" class="close" aria-label="Close">
			<i class="la la-times c-white fs-7" onclick="ocultarAlertaCaptcha();">&nbsp;</i>
			</button>
		</div>
		<hr class="bb-dashed my-6">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-xs-center">
					<div class="g-recaptcha" data-sitekey="${keyCapcha}"></div>
				</div>
			</div>
		</div>
		<br>
		<div class="text-center">
			<a class="btn btn-corp bg-blue-1 c-white w-100 max-w-40" href="#!" onclick="enviarFormulario();">Enviar Email de recuperaci�n</a>
		</div>
	</div>
</div>


			</form>
		</div>
	</div>
</section>
<style type="text/css">
	.image-casilla {
	background-position: right;
	background-repeat: no-repeat;
	background-size: contain;
	}
	.captcha {
	display: inline-table;
	
	}
	
	@media (max-width:767px){
	
		.captcha {
		display: inline-table;
		 margin: 0 15px 15px 0;
		}
	
	}
	.text-xs-center {
		text-align: center;
	}
	
	.g-recaptcha {
		display: inline-block;
	}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">

	var formFormulario = $('#<portlet:namespace/>formFormulario');
	var olvido = '${olvido}';
	var emailInput = $("#<portlet:namespace/>email");
	var alertaFormulario = $('#<portlet:namespace/>alertaFormulario');
	var exitoFormulario = $('#<portlet:namespace/>exitoFormulario');
	var alertaCaptcha = $('#<portlet:namespace/>alertaCaptcha');
	
	function ocultarAlertaFormulario(){
		alertaFormulario.hide();
	}	
	function ocultarExitoFormulario(){
		exitoFormulario.hide();
	}
	
	function ocultarAlertaCaptcha() {
		alertaCaptcha.hide();
	}

	function enviarFormulario() {
		if (validarFormulario()) {
			var response = grecaptcha.getResponse();
			alertaFormulario.hide();
			if (response.length == 0) {
				alertaCaptcha.show();
			} else {
				alertaCaptcha.hide();
				formFormulario.submit();
			}
		} else {
			alertaFormulario.show();
			$('html, body').animate({scrollTop:200}, 'slow');
		}
	}
	
	function validarFormulario() {	
		if (emailInput.val() == "") {
			console.log("email input");
			return false;
		}
		return true;
	}
	
	formFormulario.submit(function(event) {
		event.preventDefault();
		if(validarFormulario()){
			alertaFormulario.hide();
			$.ajax({
				type : 'post',
				url : olvido,
				data : formFormulario.serialize(),
				datatype : 'json',
				cache : false,
				error : function(xhr, status, error) {
					$('html, body').animate({scrollTop:200}, 'slow');
				},
				success : function(data) {
					console.log(data);
					console.log('${error}');
					var jsonData = data[0];

					if (jsonData.status != "fail") {
						alert("correo de recuperacion enviado")
						$('html, body').animate({scrollTop:200}, 'slow');
					}else{
						
						alert(jsonData.error);
						$('html, body').animate({scrollTop:200}, 'slow');
					}
					
				}
			});
		} else {
			alertaFormulario.show();
			$('html, body').animate({scrollTop:200}, 'slow');
		}
	});
	function limpiarCampos(){
		email.val("");
		email.removeClass("is-valid");
		email.parent().removeClass("focused");	
		
	}
		

	function validarEmail(email) {
		emailInput.val(email.toLowerCase());
		if (emailInput.val() != "") {
			if (validateEmail(emailInput.val())) {
				validEmail = true;
				emailInput.removeClass("is-invalid");
				emailInput.addClass("is-valid");
			} else {
				setTimeout(function() {
					validEmail = false;
					emailInput.addClass("is-invalid");
					emailInput.removeClass("is-valid");
				}, 100);

			}
		} else {
			validEmail = false;
			emailInput.addClass("is-invalid");
			emailInput.removeClass("is-valid");
		}
	}

	function validateEmail(emailInput) {
		var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		return re.test(emailInput);
	}
</script>

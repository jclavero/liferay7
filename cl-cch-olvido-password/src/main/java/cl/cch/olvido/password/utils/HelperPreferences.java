package cl.cch.olvido.password.utils;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import cl.cch.olvido.password.constants.Constantes;
import cl.cch.olvido.password.models.OlvidoPreferences;



public class HelperPreferences {

	public void savePreferences(ActionRequest req, ActionResponse res) throws IOException, PortletException {
		PortletPreferences portletPrefs = req.getPreferences();
		String correos = ParamUtil.getString(req, Constantes.EMAIL);
		savePrefs(portletPrefs, correos, Constantes.EMAIL);
	}

	private void savePrefs(PortletPreferences portletPrefs, String variable, String constante)
			throws IOException, PortletException {
		if (Validator.isBlank(variable)) {
			portletPrefs.setValue(constante, StringPool.BLANK);
			portletPrefs.store();
		} else {
			portletPrefs.setValue(constante, variable);
			portletPrefs.store();
		}
	}

	public static OlvidoPreferences loadPreferences(PortletRequest portletRequest) {
		PortletPreferences portletPrefs = portletRequest.getPreferences();
		OlvidoPreferences prefs = new OlvidoPreferences();
		prefs.setEmail(portletPrefs.getValue(Constantes.EMAIL, StringPool.BLANK));
		return prefs;
	}
	
	
	
	
}

package cl.cch.olvido.password.constants;

public class Constantes {

	
	public static final String CLCCHOLVIDO = "cl_cch_olvido_password_ClCchOlvidoPasswordPortlet";
	public static final String EMAIL = "email";
	public static final String RESOURCE = "olvido";
	public static final String PORTLET_PREFERENCIAS = "prefs";
	
	public static final String KEY_CAPCHA = "keyCapcha";
	public static final String KEY_CAPCHA_DEFAULT = "6LcS2MYUAAAAAHJPv0s3urGg6rJfgJfYphi0MZvX";
	
	public static final String REDIRECT_JSP = "redirectUrl";
	public static final String PATH_BASE = "/";
	public static final String JSP_NAME = "recuperarPass.jsp";
	
	public static final String LINK_BASE = "/web/guest/recuperar-pass";
	public static final String SUCCES = "succes";

	public static final String FAIL = "fail";

	public static final String TXT_RESULT = "result";
	
	public static final String STATUS = "status";

	public static final String CORREOS = "https://webserver-correosdechile-dev.lfr.cloud/web/guest/home";
	
}

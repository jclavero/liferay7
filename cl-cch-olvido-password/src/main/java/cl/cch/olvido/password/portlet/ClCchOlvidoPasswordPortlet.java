package cl.cch.olvido.password.portlet;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.TicketLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import cl.cch.olvido.password.constants.ClCchOlvidoPasswordPortletKeys;
import cl.cch.olvido.password.constants.Constantes;
import cl.cch.olvido.password.models.OlvidoPreferences;
import cl.cch.olvido.password.utils.HelperPreferences;
import cl.cch.olvido.password.utils.OlvidoUtils;

/**
 * @author VASS
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=olvido password",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ClCchOlvidoPassword", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ClCchOlvidoPasswordPortletKeys.CLCCHOLVIDOPASSWORD,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class ClCchOlvidoPasswordPortlet extends MVCPortlet {

	private static final Log _log = LogFactoryUtil.getLog(ClCchOlvidoPasswordPortlet.class);

	@Override
	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		OlvidoPreferences prefs = HelperPreferences.loadPreferences(renderRequest);
		renderRequest.setAttribute(Constantes.PORTLET_PREFERENCIAS, prefs);
		super.doEdit(renderRequest, renderResponse);
	}

	public void savePrefs(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException, Exception {

		HelperPreferences prefs = new HelperPreferences();
		prefs.savePreferences(actionRequest, actionResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		
		String resourceID = resourceRequest.getResourceID();
		long classNameId = ClassNameLocalServiceUtil.getClassNameId(User.class.getName());
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray result = JSONFactoryUtil.createJSONArray();

		_log.info("Paso = " + resourceID);

		if (resourceID.equalsIgnoreCase(Constantes.RESOURCE)) {

			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, 6);
			Date expirationDate = cal.getTime();
			System.out.println(expirationDate);
			
			    
			try {
				OlvidoPreferences prefs = HelperPreferences.loadPreferences(resourceRequest);
				ServiceContext serviceContext = ServiceContextFactory.getInstance(User.class.getName(), resourceRequest);		
				String email = ParamUtil.getString(resourceRequest, "email").trim();
				
				if(!email.isEmpty()) {
					User user = UserLocalServiceUtil.fetchUserByEmailAddress(themeDisplay.getCompanyId(), email);				
					if(user!=null) {
						String pass = user.getPassword();
						System.out.println("pass : "+pass);
						
						Ticket ticket = TicketLocalServiceUtil.addTicket(user.getCompanyId(),
								User.class.getName(), user.getUserId(),
								TicketConstants.TYPE_PASSWORD, null, expirationDate,
								serviceContext);
						
		         
						String passwordResetURL = serviceContext.getPortalURL() + Constantes.LINK_BASE +"?ticketKey=" + ticket.getKey();
						
						_log.info("LINK RECUPERACION :"+passwordResetURL);
					
						OlvidoUtils.enviarEmail(resourceRequest, email, passwordResetURL,Constantes.CORREOS);
						jsonObject.put(Constantes.STATUS, Constantes.SUCCES);
						result.put(jsonObject);
					}else {
						throw new Exception("email invalido o erroneo");
					}			
				}else {
					throw new Exception("email no debe ir vacio");
				}
				
				
			} catch (Exception e) {
				_log.error("unknow :" + e.getMessage());
				jsonObject.put(Constantes.STATUS, Constantes.FAIL);
				jsonObject.put("error",e.getMessage());
				result.put(jsonObject);
			}	
			
			resourceRequest.setAttribute(Constantes.TXT_RESULT, result);
			resourceResponse.setContentType(ContentTypes.APPLICATION_JSON);
			resourceResponse.flushBuffer();
			resourceResponse.getWriter().print(result);


		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		OlvidoPreferences prefs = HelperPreferences.loadPreferences(renderRequest);

		if (Validator.isNotNull(prefs.getKeyCapcha())) {
			renderRequest.setAttribute(Constantes.KEY_CAPCHA, prefs.getKeyCapcha());
		} else {
			renderRequest.setAttribute(Constantes.KEY_CAPCHA, Constantes.KEY_CAPCHA_DEFAULT);
		}
		
		super.doView(renderRequest, renderResponse);
	}
}
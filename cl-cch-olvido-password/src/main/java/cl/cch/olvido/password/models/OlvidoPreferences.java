package cl.cch.olvido.password.models;

public class OlvidoPreferences {

	private String email;
	private String keyCapcha;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "OlvidoPreferences [email=" + email + "]";
	}
	
	public String getKeyCapcha() {
		return keyCapcha;
	}
	public void setKeyCapcha(String keyCapcha) {
		this.keyCapcha = keyCapcha;
	}
}
